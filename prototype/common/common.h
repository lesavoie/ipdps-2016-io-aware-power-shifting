/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/

// This file contains declarations that are common between the PMPI wrapper
// and the stand alone runtime.

#ifndef COMMON_H
#define COMMON_H

#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

static const uint64_t START_SIGNAL = 1111;
static const uint64_t STOP_SIGNAL = 2222;
static const uint64_t START_CKPT_SIGNAL = 3333;
static const uint64_t END_CKPT_SIGNAL = 4444;

static double timevalToDouble(struct timeval tv)
{
   static const double USEC_TO_SEC = 1.0 / 1000000.0;
   return (double)tv.tv_sec + ((double)tv.tv_usec * USEC_TO_SEC);
}

static double getTime()
{
   struct timeval time;
   gettimeofday(&time, NULL);
   return timevalToDouble(time);
}

static void printTime(const char * msg)
{
   printf("%f: %s\n", getTime(), msg);
   fflush(stdout);
}

#ifdef __cplusplus
static void printTime(const char * msg, const char * arg)
{
   printf("%f: %s (%s)\n", getTime(), msg, arg);
   fflush(stdout);
}
#endif

// Returns a null terminated pipe name for this node
#define HEADER_LEN (7)  // strlen(header) - see below
static const char * getPipeName()
{
   static const char header[] = "pspipe_";
   
   // This is static to ensure that the pointer is still valid after this function is called
   static char pipeName[HEADER_LEN + HOST_NAME_MAX + 1];
   static int pipeNameGenerated = 0;
   
   if (!pipeNameGenerated)
   {
      char hostName[HOST_NAME_MAX + 1];
      gethostname(hostName, HOST_NAME_MAX);
      hostName[HOST_NAME_MAX] = '\0';  // To be safe, null terminate the string
      
      // Build up the host-specific pipe name
      strncpy(pipeName, header, HEADER_LEN);
      pipeName[HEADER_LEN] = '\0';
      strcat(pipeName, hostName);
      
      pipeNameGenerated = 1;
   }
   
   return pipeName;
}

#endif
