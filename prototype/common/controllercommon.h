/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// This contains declarations that are shared between the controller and the stand alone
// run time.

#ifndef CONTROLLER_COMMON_H
#define CONTROLLER_COMMON_H

#include <stdint.h>
#include <cstdlib>
#include <cstring>
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <algorithm>
#include <vector>

// The port number is arbitrary - it just needs to avoid collisions with used ports
const int CONTROLLER_PORT = 25178;

// Enum of messages that are sent between the runtime and controller
enum MessageType {INIT_MESSAGE, APP_START_MESSAGE, START_CKPT_MESSAGE, END_CKPT_MESSAGE, PAUSE_APP_MESSAGE, CONTINUE_APP_MESSAGE, POWER_LIMIT_MESSAGE, DISCONNECT_MESSAGE};

// Structs of message data that are sent between the runtime and controller

const int MAX_NODE_NAME_LENGTH = 16;
const int MAX_APP_NAME_LENGTH = 32;
struct InitPayload
{
   char node[MAX_NODE_NAME_LENGTH];
   char app[MAX_APP_NAME_LENGTH];
   double assignedLimit;
   double compPower;
   double ckptPower;
};

struct AppStartPayload {};
struct StartCkptPayload {};
struct EndCkptPayload {};
struct PauseAppPayload {};
struct ContinueAppPayload {};

struct PowerLimitPayload
{
   double limit;
};

struct DisconnectPayload {};

// Generic class used to send and receive all messages
template <class Payload>
struct Message
{
   MessageType messageType;
   Payload payload;
   
   // Constructor used when sending messages
   Message(MessageType t, const Payload & p) :
      messageType(t)
   {
      // Messages cannot have pointers in them - only raw data types and arrays
      memcpy(&payload, &p, sizeof(payload));
   }
   
   // Constructor used when receiving messages
   Message() {}
};

// Max size of a message payload in bytes.
const int MAX_PAYLOAD_SIZE = 76;

typedef uint8_t GenericPayload[MAX_PAYLOAD_SIZE];
typedef Message<GenericPayload> GenericMessage;

static std::vector<size_t> messageTypeToSize;

// I couldn't think of a good way to calculate the max payload size automatically, so
// this is my workaround.  If the max payload size is too small, the program will
// error out before starting (assuming this function is up to date).
// I also piggyback other initialization in this function.
static bool initControllerMessaging()
{
   size_t realMax = 0;
   realMax = std::max(realMax, sizeof(Message<InitPayload>));
   realMax = std::max(realMax, sizeof(Message<AppStartPayload>));
   realMax = std::max(realMax, sizeof(Message<StartCkptPayload>));
   realMax = std::max(realMax, sizeof(Message<EndCkptPayload>));
   realMax = std::max(realMax, sizeof(Message<PauseAppPayload>));
   realMax = std::max(realMax, sizeof(Message<ContinueAppPayload>));
   realMax = std::max(realMax, sizeof(Message<PowerLimitPayload>));
   realMax = std::max(realMax, sizeof(Message<DisconnectPayload>));
   if (sizeof(GenericMessage) < realMax)
   {
      printf("GenericMessage size (%d) is smaller than the size of the largest payload (%d).\n"
             "Please update MAX_PAYLOAD_SIZE (currently %d), recompile the controller and the\n"
             "runtime, and try again (recommended MAX_PAYLOAD_SIZE: %d).\n",
             sizeof(GenericMessage), realMax, MAX_PAYLOAD_SIZE, realMax - sizeof(GenericMessage) + MAX_PAYLOAD_SIZE);
      exit(EXIT_FAILURE);
   }
   
   // TODO: having this initialization here doesn't really make sense.  It makes a copy of the vector in every file that includes this header.
   // It's also not encapsulated very well.  I probably need to reorganize the messaging design.
   messageTypeToSize.push_back(sizeof(Message<InitPayload>));
   messageTypeToSize.push_back(sizeof(Message<AppStartPayload>));
   messageTypeToSize.push_back(sizeof(Message<StartCkptPayload>));
   messageTypeToSize.push_back(sizeof(Message<EndCkptPayload>));
   messageTypeToSize.push_back(sizeof(Message<PauseAppPayload>));
   messageTypeToSize.push_back(sizeof(Message<ContinueAppPayload>));
   messageTypeToSize.push_back(sizeof(Message<PowerLimitPayload>));
   messageTypeToSize.push_back(sizeof(Message<DisconnectPayload>));
   
   return true;
}
static bool maxPayloadSizeValid = initControllerMessaging();

// Function to send a generic message
template <class Payload>
static void sendControllerMessage(int fd, MessageType type, const Payload & payload)
{
   Message<Payload> message(type, payload);
   send(fd, &message, sizeof(message), 0);
}

static size_t recvControllerMessage(int fd, GenericMessage & message)
{
   // First get the header
   size_t size = recv(fd, &message.messageType, sizeof(MessageType), 0);
   
   // Receive the rest of the message, using the header to determine how large it should be
   if (size >= 0)
   {
      size_t payloadSize = messageTypeToSize.at(message.messageType) - sizeof(MessageType);
      if (payloadSize > 0)
      {
         // This relies on the fact that there is no padding between the messageType and the payload in a GenericMessage.
         size += recv(fd, &message.payload, payloadSize, 0);
      }
   }

   return size;
}

template<class Payload>
static const Message<Payload> * castMessage(const GenericMessage & message)
{
   return reinterpret_cast<const Message<Payload> *>(&message);
}

#endif
