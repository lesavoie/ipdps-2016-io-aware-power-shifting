/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Abstract class that defines the interface to a power sharing algorithm

#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "clientdb.h"

#include <cmath>
#include <list>
#include <string>

enum AlgorithmActionType
{
   SET_LIMIT_ACTION,
   PAUSE_APP_ACTION,
   CONTINUE_APP_ACTION
};

struct AlgorithmAction
{
   AlgorithmActionType action;
   ClientIterator iter;
   double newPowerLimit;
};

typedef std::list<AlgorithmAction> ActionList;

class Algorithm
{
protected:
   // Determines how big the difference between the power limit and the requested power must be before we
   // consider them to be different.  Used by several sub-classes.
   const static double EPSILON = 1.0;
   static int lt(double power1, double power2)
   {
      return (power1 < (power2 - EPSILON));  
   }
   static bool eq(double power1, double power2)
   {
      return (fabs(power1 - power2) < EPSILON);
   }
public:
   virtual ~Algorithm() {}

   // Returns a description of the algorithm for debugging purposes.
   virtual std::string getDescription() = 0;

   // Calculates new power limits for all nodes.  clients is an input parameter containing
   // the current state of all clients, and actions is an output parameter containing a list
   // of new power limits to apply.
   virtual void update(ClientDatabase & clients, ActionList & actions) = 0;
};

// Factory for returning the required algorithm instance
Algorithm * getAlgorithm(std::string alg);

#endif

