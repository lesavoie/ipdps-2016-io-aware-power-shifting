/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Best case algorithm: gives everyone exactly what they ask for.
// This does not respect a power limit.

#ifndef BESTCASEALGORITHM_H
#define BESTCASEALGORITHM_H

#include "algorithm.h"

#include <string>

class BestCaseAlgorithm : public Algorithm
{
public:
   virtual std::string getDescription() { return "Best case algorithm: always allocates full amount of power requested."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      ClientIterator begin = clients.begin();
      ClientIterator end = clients.end();
      for (ClientIterator iter = begin; iter != end; ++iter)
      {
         double allocated = iter->second.currentPowerLimit;
         double requested = iter->second.powerNeeded;
         
         // If the power limit is different from the requested power (higher or lower), create
         // an action to set it to the requested power.
         if (!eq(allocated, requested))
         {
            AlgorithmAction action = {SET_LIMIT_ACTION, iter, requested};
            actions.push_back(action);
         }
      }
   }
};

#endif

