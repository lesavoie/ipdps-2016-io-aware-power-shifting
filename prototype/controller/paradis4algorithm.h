/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Designed to handle 4 instances of ParaDiS where as much power as possible is shifted to one instance (in my setup, ParaDiS checkpoints just less than 25% of the time, so this should allow
// that application to run close to full speed).
// This is basically a copy of the simple algorithm except that start times are staggered and as much power as possible is directed to one application.

#ifndef PARADIS4ALGORITHM_H
#define PARADIS4ALGORITHM_H

#include "applicationalgorithm.h"
#include "common.h"

#include <algorithm>
#include <string>

#include <stdio.h>

class Paradis4Algorithm : public ApplicationAlgorithm
{
private:
   static const int NUM_APPS = 4;
   static const double TIME_OFFSET = 3.0;
   
   AppId apps[NUM_APPS];
   double startTime;
   int numClients;
   
   int getAppIndex(const std::string & name)
   {
      static const std::string APP_TO_INDEX[NUM_APPS] = {"paradis1", "paradis2", "paradis3", "paradis4"};
      for (int i = 0; i < NUM_APPS; ++i)
      {
         if (APP_TO_INDEX[i] == name) return i;
      }
      printf("Error: couldn't find index for app %s.\n", name.c_str());
      return NUM_APPS;
   }
   
   void setIndex(const Client & client)
   {
      int appIndex = getAppIndex(client.applicationName);
      if (appIndex < NUM_APPS && apps[appIndex] < 0)
      {
         apps[appIndex] = client.application;
         printf("App ID: %d, priority: %d\n", client.application, appIndex);
         ++numClients;
      }
   }
   
   void handlePauses(ClientDatabase & clients, ActionList & actions)
   {
      double currentTime = getTime();
      
      if (startTime < 0.0)
      {
         // Map applications to app IDs.  This ensures that paradis1 is always the 1st app, paradis2 is always the
         // second app, and so on, which ensures that the same nodes get the same priority between runs.  That helps
         // eliminate node differences as a source of run differences.
         for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
         {
            if (iter->second.initialized) setIndex(iter->second);
         }
         // If we've found all the applications, set the start time.
         if (numClients >= NUM_APPS)
         {
            for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
            {
               if (iter->second.application == apps[0] && iter->second.startTime > 0.0)
               {
                  startTime = currentTime;
                  break;
               }
            }
         }
      }
      
      // If any nodes of app 1 get paused, restart them
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == apps[0] && iter->second.paused)
         {
            AlgorithmAction action = {CONTINUE_APP_ACTION, iter, 0.0};
            actions.push_back(action);
         }
      }
      
      // Pause all applications except the main application for the first time interval
      if (currentTime < startTime + TIME_OFFSET)
      {
         for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
         {
            if (iter->second.application != apps[0] && !iter->second.paused)
            {
               AlgorithmAction action = {PAUSE_APP_ACTION, iter, 0.0};
               actions.push_back(action);
            }
         }
      }
      
      // Start the rest of the jobs.
      else if (currentTime < startTime + (TIME_OFFSET * 2))
      {
         // Start the second application
         // We assume that all nodes have been paused by now
         for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
         {
            if (iter->second.application == apps[1] && iter->second.paused)
            {
               AlgorithmAction action = {CONTINUE_APP_ACTION, iter, 0.0};
               actions.push_back(action);
            }
         }
      }
      else if (currentTime < startTime + (TIME_OFFSET * 3))
      {
         // Start the third application
         for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
         {
            if (iter->second.application == apps[2] && iter->second.paused)
            {
               AlgorithmAction action = {CONTINUE_APP_ACTION, iter, 0.0};
               actions.push_back(action);
            }
         }
      }
      else if (currentTime < startTime + (TIME_OFFSET * 4))  // The if statement isn't strictly necessary here, but it ensures that we stop running this code after a reasonable amount of time.
      {
         // Start the fourth application
         for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
         {
            if (iter->second.paused)
            {
               AlgorithmAction action = {CONTINUE_APP_ACTION, iter, 0.0};
               actions.push_back(action);
            }
         }
      }
   }

public:
   Paradis4Algorithm() : startTime(-1.0), numClients(0)
   {
      for (int i = 0; i < NUM_APPS; ++i) apps[i] = -1;
   }
   virtual std::string getDescription() { return "ParaDiS 4 algorithm: 4 copies of ParaDiS such that all donate to one."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      handlePauses(clients, actions);
      
      // If we don't know all the applications yet, stop
      if (startTime < 0.0) return;
   
      // Make a copy of the clients database
      ClientDatabase updatedClients(clients.begin(), clients.end());
      
      // Set all clients back to their allocation or to their requested power if it's less than their allocation
      ClientIterator begin = updatedClients.begin();
      ClientIterator end = updatedClients.end();
      double unusedPower = 0.0;
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         double allocated = iter->second.assignedPowerLimit;
         double requested = iter->second.powerNeeded;
         if (lt(requested, allocated))
         {
            // If a node doesn't need its full allocation, the extra power can be assigned elsewhere
            iter->second.currentPowerLimit = requested;
            unusedPower += allocated - requested;
         }
         else
         {
            // If a node needs its full allocation, it gets it
            iter->second.currentPowerLimit = allocated;
         }
      }
      
      // Reallocate as much power as possible to applications in the reverse order they were started,
      // except app 1, which has priority.
      {
         double current = getCurrentPower(apps[0], updatedClients);
         double requested = getRequestedPower(apps[0], updatedClients);
         if (lt(current, requested) && unusedPower > 0.0)
         {
            double extraPowerNeeded = requested - current;
            double extraPowerAvailable = std::min(extraPowerNeeded, unusedPower);
            setAllocatedPower(apps[0], extraPowerAvailable, updatedClients);
            unusedPower -= extraPowerAvailable;
         }
      }
      {
         double current = getCurrentPower(apps[3], updatedClients);
         double requested = getRequestedPower(apps[3], updatedClients);
         if (lt(current, requested) && unusedPower > 0.0)
         {
            double extraPowerNeeded = requested - current;
            double extraPowerAvailable = std::min(extraPowerNeeded, unusedPower);
            setAllocatedPower(apps[3], extraPowerAvailable, updatedClients);
            unusedPower -= extraPowerAvailable;
         }
      }
      {
         double current = getCurrentPower(apps[2], updatedClients);
         double requested = getRequestedPower(apps[2], updatedClients);
         if (lt(current, requested) && unusedPower > 0.0)
         {
            double extraPowerNeeded = requested - current;
            double extraPowerAvailable = std::min(extraPowerNeeded, unusedPower);
            setAllocatedPower(apps[2], extraPowerAvailable, updatedClients);
            unusedPower -= extraPowerAvailable;
         }
      }
      {
         double current = getCurrentPower(apps[1], updatedClients);
         double requested = getRequestedPower(apps[1], updatedClients);
         if (lt(current, requested) && unusedPower > 0.0)
         {
            double extraPowerNeeded = requested - current;
            double extraPowerAvailable = std::min(extraPowerNeeded, unusedPower);
            setAllocatedPower(apps[1], extraPowerAvailable, updatedClients);
            unusedPower -= extraPowerAvailable;
         }
      }

      if (lt(0.0, unusedPower))
      {
         printf("@@@@>Unused power: %f\n", unusedPower);
      }
      
      // Finally, create actions for any nodes that have new power limits
      for (ClientIterator newIter = begin; newIter != end; ++newIter)
      {
         ClientIterator oldIter = clients.find(newIter->first);  // TODO: error handling
         if (!eq(newIter->second.currentPowerLimit, oldIter->second.currentPowerLimit))
         {
            AlgorithmAction action = {SET_LIMIT_ACTION, oldIter, newIter->second.currentPowerLimit};
            actions.push_back(action);
         }
      }
   }
};

#endif

