/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Simple algorithm: reallocates power on a first come, first served basis with no attempt to schedule or delay
// applications to increase opportunities for power sharing.

#ifndef SIMPLEALGORITHM_H
#define SIMPLEALGORITHM_H

#include "algorithm.h"

#include <algorithm>
#include <string>

#include <stdio.h>

class SimpleAlgorithm : public Algorithm
{
public:
   virtual std::string getDescription() { return "Simple algorithm: reallocates power on first come, first served basis."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      // Make a copy of the clients database
      ClientDatabase updatedClients(clients.begin(), clients.end());
      
      // Set all clients back to their allocation or to their requested power if it's less than their allocation
      ClientIterator begin = updatedClients.begin();
      ClientIterator end = updatedClients.end();
      double unusedPower = 0.0;
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         double allocated = iter->second.assignedPowerLimit;
         double requested = iter->second.powerNeeded;
         if (lt(requested, allocated))
         {
            // If a node doesn't need its full allocation, the extra power can be assigned elsewhere
            iter->second.currentPowerLimit = requested;
            unusedPower += allocated - requested;
         }
         else
         {
            // If a node needs its full allocation, it gets it
            iter->second.currentPowerLimit = allocated;
         }
      }
      
      // Next, reallocate the unused power to other clients
      for (ClientIterator iter = begin; iter != end; ++iter)
      {
         double current = iter->second.currentPowerLimit;
         double requested = iter->second.powerNeeded;
         if (lt(current, requested) && unusedPower > 0.0)
         {
            double extraPowerNeeded = requested - current;
            double extraPowerAvailable = std::min(extraPowerNeeded, unusedPower);
            iter->second.currentPowerLimit = current + extraPowerAvailable;
            unusedPower -= extraPowerAvailable;
         }
      }
      
      if (lt(0.0, unusedPower))
      {
         printf("@@@@>Unused power: %f\n", unusedPower);
      }
      
      // Finally, create actions for any nodes that have new power limits
      for (ClientIterator newIter = begin; newIter != end; ++newIter)
      {
         ClientIterator oldIter = clients.find(newIter->first);  // TODO: error handling
         if (!eq(newIter->second.currentPowerLimit, oldIter->second.currentPowerLimit))
         {
            AlgorithmAction action = {SET_LIMIT_ACTION, oldIter, newIter->second.currentPowerLimit};
            actions.push_back(action);
         }
      }
   }
};

#endif

