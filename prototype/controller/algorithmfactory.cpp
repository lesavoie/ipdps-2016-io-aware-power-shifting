/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Factory method for providing the correct power sharing algorithm implementation.
// This factory must be updated for each new factory added.

#include "algorithm.h"
#include "alt2secalgorithm.h"
#include "bestcasealgorithm.h"
#include "bigsmallalgorithm.h"
#include "nosharingalgorithm.h"
#include "paradis4algorithm.h"
#include "cactus4algorithm.h"
#include "roundrobin10algorithm.h"
#include "simplealgorithm.h"
#include "simpleappalgorithm.h"
#include "stackalgorithm.h"
#include "lammps2algorithm.h"
#include "hardcodealgorithm.h"

#include <stdio.h>
#include <string>

using namespace std;

Algorithm * getAlgorithm(string alg)
{
   if (alg == "simple") return new SimpleAlgorithm();
   else if (alg == "simpleapp") return new SimpleAppAlgorithm();
   else if (alg == "stack") return new StackAlgorithm();
   else if (alg == "bestcase") return new BestCaseAlgorithm();
   else if (alg == "alt2sec") return new Alt2SecAlgorithm();
   else if (alg == "bigsmall") return new BigSmallAlgorithm();
   else if (alg == "roundrobin10") return new RoundRobin10Algorithm();
   else if (alg == "paradis4") return new Paradis4Algorithm();
   else if (alg == "cactus4") return new Cactus4Algorithm();
   else if (alg == "lammps2") return new Lammps2Algorithm();
   else if (alg == "hardcode") return new HardCodeAlgorithm();
   else
   {
      if (alg != "nosharing") printf("Unrecognized alg %s; using default nosharing.\n", alg.c_str());
      return new NoSharingAlgorithm();
   }
}

