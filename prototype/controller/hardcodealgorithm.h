/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Hard code algorithm: sets all power limits to specific limits at specific time intervals.

#ifndef HARDCODEALGORITHM_H
#define HARDCODEALGORITHM_H

#include "algorithm.h"
#include "common.h"

#include <string>
#include <vector>

const static double INTERVAL = 2.0;	// seconds
const static double MAX_POWER = 75;
const static double MIN_POWER = 40;
const static double POWER_DIFF = MAX_POWER - MIN_POWER;

class HardCodeAlgorithm : public Algorithm
{
private:
   bool started;  // Will be set to true when the first application starts
   double lastTransitionTime;	// seconds
   std::vector<double> powerLimits;
   std::vector<double>::iterator nextLimit;
public:
   HardCodeAlgorithm() {
   
      powerLimits.push_back(MIN_POWER);
      powerLimits.push_back(MAX_POWER);
      powerLimits.push_back(MIN_POWER);
      powerLimits.push_back(MIN_POWER + (POWER_DIFF / 2));
      powerLimits.push_back(MIN_POWER);
      powerLimits.push_back(MIN_POWER + (POWER_DIFF / 4));
      powerLimits.push_back(MIN_POWER);
      powerLimits.push_back(MIN_POWER + 5);

      nextLimit = powerLimits.begin();
      lastTransitionTime = 0;
      started = false;
   }
   virtual std::string getDescription() { return "Hard code algorithm: sets all power limits to specific limits at specific times."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      double currentTime = getTime();
      
      // Check to see if the application has started
      if (!started) {
         ClientIterator iter = clients.begin();
         for(; iter != clients.end(); ++iter) {
            if (iter->second.startTime > 0.0) {
               started = true;
               lastTransitionTime = currentTime;
            }
         }
      }
      else {
      
         if (currentTime > (lastTransitionTime + INTERVAL)) {
            lastTransitionTime = currentTime;
            double newLimit = *nextLimit;
            ++nextLimit;
            if (nextLimit == powerLimits.end()) nextLimit = powerLimits.begin();

            // Change the power limit of all nodes to the new limit
            ClientIterator iter = clients.begin();
            for(; iter != clients.end(); ++iter) {
               AlgorithmAction action = {SET_LIMIT_ACTION, iter, newLimit};
               actions.push_back(action);
            }
         }
      
      }
   }
};

#endif

