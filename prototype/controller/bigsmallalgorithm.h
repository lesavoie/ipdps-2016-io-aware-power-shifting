/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Big/small algorithm: handles one big job with lots of nodes and a high limit and lots of small jobs with few nodes and a low limit

#ifndef BIGSMALLALGORITHM_H
#define BIGSMALLALGORITHM_H

#include "common.h"
#include "simplealgorithm.h"

#include <set>
#include <string>

class BigSmallAlgorithm : public SimpleAlgorithm
{
private:
   static const double BIG_LIMIT = 65.0;
   enum State {INIT, PAUSED, DONE};

   State state;
   double bigJobStartTime;
   std::set<int> pausedClients;

   bool isBig(const Client & client)
   {
      return (client.assignedPowerLimit > BIG_LIMIT);
   }
   void pauseIfNew(ClientIterator & iter, ActionList & actions)
   {
      std::set<int>::iterator paused = pausedClients.find(iter->first);
      if (iter->second.initialized && !isBig(iter->second) && paused == pausedClients.end())
      {
         AlgorithmAction action = {PAUSE_APP_ACTION, iter, 0.0};
         actions.push_back(action);
         pausedClients.insert(iter->first);
      }
   }
   void unpause(ClientIterator & iter, ActionList & actions)
   {
      if (iter->second.initialized && !isBig(iter->second))
      {
         AlgorithmAction action = {CONTINUE_APP_ACTION, iter, 0.0};
         actions.push_back(action);
      }
   }
public:
   BigSmallAlgorithm() :
      state(INIT),
      bigJobStartTime(-1.0)
   {}
   virtual std::string getDescription() { return "Big/small algorithm: handles one big job with lots of nodes and a high limit and lots of small jobs with few nodes and a low limit."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      State newState = state;
      double currentTime = getTime();
      ClientIterator begin = clients.begin();
      ClientIterator end = clients.end();
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         switch(state)
         {
            case INIT:
               // See if the big job has started
               if (bigJobStartTime < 0.0 && iter->second.initialized && isBig(iter->second) && iter->second.startTime > 0.0)
               {
                  bigJobStartTime = iter->second.startTime;
               }
               pauseIfNew(iter, actions);
               if (bigJobStartTime >= 0.0 && pausedClients.size() >= 4)
               {
                  newState = PAUSED;
               }
               break;
            case PAUSED:
               if (currentTime >= bigJobStartTime + 2.0)
               {
                  unpause(iter, actions);
                  newState = DONE;
               }
               break;
            case DONE:
               SimpleAlgorithm::update(clients, actions);
               break;
         }
      }
      state = newState;
   }
};

#endif

