/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// TODO: I never finished writing and testing this.  At last check, it didn't work completely.

// Base class for algorithms that set power limits per application instead of per node

#ifndef APPLICATION_ALGORITHM_H
#define APPLICATION_ALGORITHM_H

#include "algorithm.h"
#include "application.h"

#include <set>
#include <vector>

typedef std::vector<AppId> AppList;
typedef AppList::iterator AppIterator;

class ApplicationAlgorithm : public Algorithm 
{
protected:
   // Common functions for dealing with entire applications at once
   
   AppList getApplications(ClientDatabase & clients)
   {
      // Returns a list of applications, sorted according to application name (which allows for
      // determinism across runs).
      
      // Add all the clients to a map (sorts them and removes duplicates).
      std::map<std::string, AppId> apps;
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         apps.insert(make_pair(iter->second.applicationName, iter->second.application));
      }
      
      // Convert the map to a list with just application IDs
      AppList list;
      list.reserve(apps.size()); // preallocate for speed
      for (std::map<std::string, AppId>::iterator iter = apps.begin(); iter != apps.end(); ++iter)
      {
         list.push_back(iter->second);
      }
      return list;
   }
   double getAllocatedPower(AppId app, ClientDatabase & clients)
   {
      double total = 0.0;
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app) total += iter->second.assignedPowerLimit;
      }
      return total;
   }
   double getRequestedPower(AppId app, ClientDatabase & clients)
   {
      double total = 0.0;
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app) total += iter->second.powerNeeded;
      }
      return total;
   }
   double getCurrentPower(AppId app, ClientDatabase & clients)
   {
      double total = 0.0;
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app) total += iter->second.currentPowerLimit;
      }
      return total;
   }
   bool isCheckpointing(AppId app, ClientDatabase & clients)
   {
      // Returns true if any node in this application is checkpointing
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app && iter->second.checkpointing) return true;
      }
      return false;
   }
   bool allCheckpointing(AppId app, ClientDatabase & clients)
   {
      // Returns true if all nodes in this application are checkpointing
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app && !iter->second.checkpointing) return false;
      }
      return true;
   }
   void setAllocatedPower(AppId app, double extraPower, ClientDatabase & clients)
   {
      // Find all the nodes in this application that need power (not checkpointing or paused).
      int nodesThatNeedPower = 0;
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app && !iter->second.paused && !iter->second.checkpointing) ++nodesThatNeedPower;
      }
      
      // Calculate the amount of power available per node and reallocate it to all nodes.  This assumes that all computing
      // nodes need the same amount of power.
      if (nodesThatNeedPower > 0)
      {
         double powerPerNode = extraPower / nodesThatNeedPower;
         for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
         {
            if (iter->second.application == app && !iter->second.paused && !iter->second.checkpointing)
            {
               iter->second.currentPowerLimit = iter->second.currentPowerLimit + powerPerNode;
            }
         }
      }
   }
   double setPowerCheckpointing(AppId app, ClientDatabase & clients)
   {
      // Sets the power on all nodes for this application to checkpointing power and returns the extra power available.
      // This function assumes that the caller has previously verified that all nodes are checkpointing.
      double extraPower = 0.0;
      for (ClientIterator iter = clients.begin(); iter != clients.end(); ++iter)
      {
         if (iter->second.application == app)
         {
            extraPower += iter->second.currentPowerLimit - iter->second.ckptPower;
            iter->second.currentPowerLimit = iter->second.ckptPower;
         }
      }
      return extraPower;
   }
};

#endif

