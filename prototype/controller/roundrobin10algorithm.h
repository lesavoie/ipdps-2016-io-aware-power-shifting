/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Round robin algorithm for 10 nodes: each node lends power to the next node in line

#ifndef ROUNDROBIN10ALGORITHM_H
#define ROUNDROBIN10ALGORITHM_H

#include "common.h"
#include "algorithm.h"

#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

class RoundRobin10Algorithm : public Algorithm
{
private:
   static const unsigned int NUM_NODES = 10;
   enum State {INIT, DONE};

   State state;
   double startTime;
   std::vector<int> nodes;

   std::vector<int>::iterator find(int fd)
   {
      return std::find(nodes.begin(), nodes.end(), fd);
   }
   void pauseIfNew(ClientIterator & iter, ActionList & actions)
   {
      std::vector<int>::iterator found = find(iter->first);
      if (found == nodes.end() && iter->second.initialized)
      {
         AlgorithmAction action = {PAUSE_APP_ACTION, iter, 0.0};
         actions.push_back(action);
         nodes.push_back(iter->first);      
      }
   }
   void unpause(ClientIterator & iter, ActionList & actions)
   {
      AlgorithmAction action = {CONTINUE_APP_ACTION, iter, 0.0};
      actions.push_back(action);
   }
   void donatePower(ClientIterator & iter, int number, ActionList & actions, ClientDatabase & clients)
   {
      double allocated = iter->second.assignedPowerLimit;
      double requested = iter->second.powerNeeded;
      double current = iter->second.currentPowerLimit;
      int nextApp = (number + 3) % NUM_NODES;   // Spread things out a bit by jumping over the 2 adjacent applications
      ClientIterator nextAppIter = clients.find(nodes.at(nextApp));
      if (nextAppIter != clients.end())
      {
         if (lt(requested, current))
         {
            // Lower the power limit and donate it to another application
            double extraPower = current - requested;
            AlgorithmAction action = {SET_LIMIT_ACTION, iter, requested};
            actions.push_back(action);
            AlgorithmAction action2 = {SET_LIMIT_ACTION, nextAppIter, nextAppIter->second.currentPowerLimit + extraPower};
            actions.push_back(action2);
         }
         else if (lt(requested, current) && lt(current, allocated))
         {
            // Raise the power limit and take the power back from the other application
            double newLimit = std::min(requested, allocated);
            double neededPower = newLimit - current;
            AlgorithmAction action = {SET_LIMIT_ACTION, iter, newLimit};
            actions.push_back(action);
            AlgorithmAction action2 = {SET_LIMIT_ACTION, nextAppIter, nextAppIter->second.currentPowerLimit - neededPower};
            actions.push_back(action2);
         }
      }
   }
public:
   RoundRobin10Algorithm() :
      state(INIT),
      startTime(0.0)
   {}
   virtual std::string getDescription() { return "Round robin algorithm for 10 nodes: each node lends power to the next node in line."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      State newState = state;
      double currentTime = getTime();
      ClientIterator begin = clients.begin();
      ClientIterator end = clients.end();
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         switch (state)
         {
            case INIT:
               // If this is a new client, pause it
               pauseIfNew(iter, actions);
               if (nodes.size() >= NUM_NODES)
               {
                  startTime = getTime();
                  newState = DONE;
               }
               break;
            case DONE:
               double currentTime = getTime();
               int number = distance(nodes.begin(), find(iter->first));
               if (currentTime >= startTime + (2 * number) && iter->second.paused) unpause(iter, actions);
               donatePower(iter, number, actions, clients);
               break;
         }
      }
      state = newState;
   }
};

#endif

