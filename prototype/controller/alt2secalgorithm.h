/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Alternating 2 second algorithm: assumes 2 applications, both of which compute for 2 seconds and then checkpoint for 2 seconds.
// Stalls one application for 2 seconds to get them to line up nicely, then allocates power the same as the simple algorithm.

#ifndef ALT2SECALGORITHM_H
#define ALT2SECALGORITHM_H

#include "common.h"
#include "simplealgorithm.h"

#include <stdio.h>
#include <string>
#include <sys/time.h>

class Alt2SecAlgorithm : public SimpleAlgorithm
{
private:
   enum State {NONE_STARTED, FIRST_CONNECTED, SECOND_CONNECTED, DELAYING, DELAY_FINISHED};
   State state;
   int firstFd;
   double startDelay;
   int delayedFd;
public:
   Alt2SecAlgorithm() :
      state(NONE_STARTED),
      firstFd(0),
      startDelay(0.0),
      delayedFd(0)
   {}
   virtual std::string getDescription() { return "Alternating 2 sec algorithm: pauses the second app for 2 seconds, then uses the simple algorithm."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      bool again = false;
      do
      {
         // This algorithm uses a simple state machine to determine what needs to happen next.
         printf("State: %d\n", state);
         switch (state)
         {
            case NONE_STARTED:
               again = false;
               if (!clients.empty())
               {
                  firstFd = clients.begin()->first;
                  state = FIRST_CONNECTED;
                  again = true;
               }
               break;
            case FIRST_CONNECTED:
               again = false;
               if (clients.size() > 1)
               {
                  ClientIterator iter = clients.begin();
                  ++iter;
                  delayedFd = iter->first;
                  AlgorithmAction action = {PAUSE_APP_ACTION, clients.find(delayedFd), 0.0};
                  actions.push_back(action);
                  state = SECOND_CONNECTED;
                  again = true;
               }
               break;
            case SECOND_CONNECTED:
               again = false;
               if (clients.at(firstFd).startTime > 0.0)
               {
                  startDelay = clients.at(firstFd).startTime;
                  state = DELAYING;
                  again = true;
               }
               break;
            case DELAYING:
            {
               again = false;
               timeval currentTimeVal;
               gettimeofday(&currentTimeVal, NULL);
               double currentTime = timevalToDouble(currentTimeVal);
               if (currentTime >= startDelay + 2.0)
               {
                  AlgorithmAction action = {CONTINUE_APP_ACTION, clients.find(delayedFd), 0.0};
                  actions.push_back(action);
                  state = DELAY_FINISHED;
                  again = true;
               }
               break;
            }
            case DELAY_FINISHED:
               SimpleAlgorithm::update(clients, actions);
               again = false;
               break;
         }
      // This allows us to move through multiple states in a single execution.
      } while(again);   
   }
};

#endif

