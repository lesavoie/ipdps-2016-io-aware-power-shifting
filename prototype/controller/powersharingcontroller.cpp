/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Main file for the power sharing controller

#include "algorithm.h"
#include "clientdb.h"
#include "common.h"
#include "controllercommon.h"

#include <stdio.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/timerfd.h>
#include <netdb.h>
#include <unistd.h>

#include <algorithm>
#include <queue>

using namespace std;

// Maximum connections that can be buffered by the server.  This is arbitrary and may need to be changed.
const int MAX_CONNECTIONS = 512;

// Global database mapping file descriptors to data about clients
static ClientDatabase clients;

// List of disconnected clients.  This allows us to keep track of which
// clients disconnected during a particular iteration and delete them at
// the end of the iteration.  If they were deleted during the iteration,
// they might invalidate the iterators being used.
static queue<int> disconnectedClients;

// Time that the first application started executing
double startTime = -1.0;

static int startTimer(double timeoutTime)
{
   static const int RELATIVE_TIMER = 0;
   static const double SEC_TO_NSEC = 1000000000;

   int timerFd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);

   timespec timeout;
   itimerspec timerSpec;

   timeout.tv_sec = 0;
   timeout.tv_nsec = timeoutTime * SEC_TO_NSEC;

   timerSpec.it_value = timeout;
   timerSpec.it_interval = timeout;

   timerfd_settime(timerFd, RELATIVE_TIMER, &timerSpec, NULL);
   
   return timerFd;
}

static int setUpServerSocket()
{
   int serverFd = socket(AF_INET, SOCK_STREAM, 0);
   if (serverFd < 0)
   {
      printf("Error: could not open server socket.\n");
   }
   sockaddr_in serverAddress;
   bzero(reinterpret_cast<char *>(&serverAddress), sizeof(serverAddress));
   serverAddress.sin_family = AF_INET;
   serverAddress.sin_addr.s_addr = INADDR_ANY;
   serverAddress.sin_port = htons(CONTROLLER_PORT);
   if (bind(serverFd, reinterpret_cast<sockaddr *>(&serverAddress), sizeof(serverAddress)) < 0)
   {
      printf("Error: could not bind to server socket.\n");
   }
   listen(serverFd, MAX_CONNECTIONS);
   return serverFd;
}

static void setPowerLimit(const ClientIterator & iter, double limit)
{
   PowerLimitPayload payload = {limit};
   sendControllerMessage(iter->first, POWER_LIMIT_MESSAGE, payload);
   iter->second.currentPowerLimit = limit;
   printf("Setting power limit on %s to %f.\n", iter->second.node.c_str(), limit);
}

static void pauseApplication(const ClientIterator & iter)
{
   PauseAppPayload payload;
   sendControllerMessage(iter->first, PAUSE_APP_MESSAGE, payload);
   iter->second.paused = true;
   printTime("Pausing client.", iter->second.node.c_str());
}

static void continueApplication(const ClientIterator & iter)
{
   ContinueAppPayload payload;
   sendControllerMessage(iter->first, CONTINUE_APP_MESSAGE, payload);
   iter->second.paused = false;
   printTime("Resuming client.", iter->second.node.c_str());
}

// Returns a unique identifier for each application.
static AppId getApplicationId(string appName)
{
   static AppId nextId = static_cast<AppId>(1);
   static map<string, AppId> apps;
   
   map<string, AppId>::iterator iter = apps.find(appName);
   if (iter != apps.end())
   {
      // If we've seen this application before, return its ID.
      return iter->second;
   }
   else
   {
      // If we haven't seen this application before, create a new ID, add it to the map, and return it.
      AppId newId = nextId;
      nextId = static_cast<AppId>(nextId + 1);
      apps[appName] = newId;
      return newId;
   }
}

static void handleMessage(const ClientIterator & iter, const Message<InitPayload> * message)
{
   iter->second.node.assign(message->payload.node, MAX_NODE_NAME_LENGTH - 1);
   iter->second.applicationName = message->payload.app;  // TODO: this could be a problem if the null terminator is lost
   iter->second.application = getApplicationId(iter->second.applicationName);
   iter->second.compPower = message->payload.compPower;
   iter->second.ckptPower = message->payload.ckptPower;
   iter->second.powerNeeded = iter->second.compPower;
   iter->second.assignedPowerLimit = message->payload.assignedLimit;
   printf("Got init message for node %s (FD = %d, app = %s, app ID = %d).\n", iter->second.node.c_str(), iter->first, iter->second.applicationName.c_str(), iter->second.application);
   printf("\tassignedPowerLimit = %f\n", iter->second.assignedPowerLimit);
   printf("\tcompPower = %f\n", iter->second.compPower);
   printf("\tckptPower = %f\n", iter->second.ckptPower);

   // Give it an initial power limit
   setPowerLimit(iter, iter->second.assignedPowerLimit);
   iter->second.initialized = true;
}

static void handleMessage(const ClientIterator & iter, const Message<AppStartPayload> * message)
{
   if (startTime < 0.0) startTime = getTime();
   printTime("Client application started.", iter->second.node.c_str());
   iter->second.startTime = getTime();
}

static void handleMessage(const ClientIterator & iter, const Message<StartCkptPayload> * message)
{
   printTime("Client started checkpoint.", iter->second.node.c_str());
   iter->second.powerNeeded = iter->second.ckptPower;
   iter->second.checkpointing = true;
}

static void handleMessage(const ClientIterator & iter, const Message<EndCkptPayload> * message)
{
   printTime("Client completed checkpoint.", iter->second.node.c_str());
   iter->second.powerNeeded = iter->second.compPower;
   iter->second.checkpointing = false;
}

static void handleMessage(const ClientIterator & iter, const Message<DisconnectPayload> * message)
{
   printTime("Client sent disconnect message.", iter->second.node.c_str());
   
   // The client is not removed from the database immediately to avoid invalidating iterators.
   // Instead, it is added to a list and removed at the end of the iteration.
   disconnectedClients.push(iter->first);
}

static void handleMessage(const ClientIterator & iter, const GenericMessage & message)
{
   switch (message.messageType)
   {
      case INIT_MESSAGE:
         handleMessage(iter, castMessage<InitPayload>(message));
         break;
      case APP_START_MESSAGE:
         handleMessage(iter, castMessage<AppStartPayload>(message));
         break;
      case START_CKPT_MESSAGE:
         handleMessage(iter, castMessage<StartCkptPayload>(message));
         break;
      case END_CKPT_MESSAGE:
         handleMessage(iter, castMessage<EndCkptPayload>(message));
         break;
      case DISCONNECT_MESSAGE:
         handleMessage(iter, castMessage<DisconnectPayload>(message));
         break;
      default:
         printf("Error: received unrecognized message type: %d\n", message.messageType);
         break;
   }
}

static void handleNewClient(int serverFd)
{
   // Open the port
   sockaddr_in clientAddress;
   socklen_t clientLength = sizeof(clientAddress);
   int clientFd = accept(serverFd, reinterpret_cast<sockaddr *>(&clientAddress), &clientLength);
   if (clientFd < 0)
   {
      printf("Error: could not accept connection from client.\n");
   }
   
   if (clients.find(clientFd) != clients.end())
   {
      // This should never happen.  If it does, report it, but pretend nothing happened.
      printf("Error: got duplicate client with FD %d.\n", clientFd);
   }

   // Add it to the list of clients.  These will be filled in properly after the init message
   // is received.
   Client newClient = clients[clientFd];
   newClient.initialized = false;
   newClient.node = "";
   newClient.applicationName = "";
   newClient.application = NO_APPLICATION;
   newClient.startTime = -1.0;
   newClient.assignedPowerLimit = 0.0;
   newClient.compPower = 0.0;
   newClient.ckptPower = 0.0;
   newClient.powerNeeded = 0.0;
   newClient.currentPowerLimit = 0.0;
   newClient.paused = false;
   newClient.checkpointing = false;
}

// Performs a list of actions generated by the algorithm.  This could include setting
// the power limit on some nodes, pausing some nodes, or un-pausing some nodes.
static void applyActions(const ActionList & actions)
{
   ActionList::const_iterator begin = actions.begin();
   ActionList::const_iterator end = actions.end();
   for (ActionList::const_iterator actionIter = begin; actionIter != end; ++actionIter)
   {
      switch(actionIter->action)
      {
         case SET_LIMIT_ACTION:
            setPowerLimit(actionIter->iter, actionIter->newPowerLimit);
            break;
         case PAUSE_APP_ACTION:
            pauseApplication(actionIter->iter);
            break;
         case CONTINUE_APP_ACTION:
            continueApplication(actionIter->iter);
            break;
         // All options should be represented here, so no default case.
      }
   }
}

int main(int argc, char **argv)
{
   printTime("Starting up.");

   // If there is not a command line argument, disable sharing
   string algString = "nosharing";
   if (argc > 1) algString = argv[1];

   Algorithm * algorithm = getAlgorithm(algString);
   
   printf("Using algorithm %s\n", algorithm->getDescription().c_str());
   
   // Set up a timer.  This ensures that the algorithm runs periodically even if there is no activity from the clients.
   int timerFd = startTimer(0.1);   // The timer goes off every 100 ms.  This can be adjusted as needed.

   // Set up the socket
   int serverFd = setUpServerSocket();
   printTime("Socket created.");
   
   double endTime;
   int numClients = 0;

   do
   {
      fd_set readSet;
      
      int maxFd = serverFd;
      FD_ZERO(&readSet);
      FD_SET(serverFd, &readSet);
      
      FD_SET(timerFd, &readSet);
      maxFd = max(timerFd, maxFd);
      
      // Loop through the clients and add each one to the read set
      ClientIterator begin = clients.begin();
      ClientIterator end = clients.end();
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         int fd = iter->first;
         FD_SET(fd, &readSet);
         maxFd = max(fd, maxFd);
      }
      ++maxFd;

      // Wait until there is data available on one of the sockets.  We don't worry about a timeout because we
      // assume that at least one client will connect.     
      select(maxFd, &readSet, NULL, NULL, NULL);
      
      // Check for messages from existing clients.  We reuse the iterators from above because the clients object
      // will not have changed.
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         int clientFd = iter->first;
         if (FD_ISSET(clientFd, &readSet))
         {
            GenericMessage message;
            size_t size = recvControllerMessage(clientFd, message);
            if (size < 0)
            {
               printf("Error: problem receiving message from client.\n");
            }

            // Handle the message
            handleMessage(iter, message);
         }
      }
      
      // Check for new clients
      if (FD_ISSET(serverFd, &readSet))
      {
         printTime("New client connected.");
         handleNewClient(serverFd);
         ++numClients;
      }
      
      // See if the timer went off.  Nothing special happens in this case - the algorithm just gets to run.
      if (FD_ISSET(timerFd, &readSet))
      {
         // Read the timer file descriptor to clear it
         uint64_t data;
         read(timerFd, &data, sizeof(uint64_t));
      }
      
      // Remove any disconnected clients from the database
      while(!disconnectedClients.empty())
      {
         int clientFd = disconnectedClients.front(); 
         disconnectedClients.pop();
         close(clientFd);
         clients.erase(clientFd);
      }
      
      // Send updates to nodes
      ActionList actions;
      algorithm->update(clients, actions);
      applyActions(actions);

   // We can exit the loop once all clients have disconnected (but first we wait for at least one to connect).
   } while(!clients.empty() || numClients <= 0);
   endTime = getTime();
   
   printTime("All clients disconnected.");
   printf("####--------->Total run time: %f<---------####\n", (endTime - startTime));

   close(serverFd);
   close(timerFd);
   delete algorithm;
   
   printTime("Shutting down.");

   return 0;
}

