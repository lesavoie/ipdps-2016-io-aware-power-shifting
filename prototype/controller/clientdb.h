/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Definitions needed for the client database

#ifndef CLIENTDB_H
#define CLIENTDB_H

#include "application.h"

#include <map>
#include <string>
#include <vector>

struct Client
{
   bool initialized;
   std::string node;
   std::string applicationName;
   AppId application;
   double startTime;
   double assignedPowerLimit;
   double compPower;
   double ckptPower;
   double powerNeeded;
   double currentPowerLimit;
   bool paused;
   bool checkpointing;
};

typedef std::map<int, Client> ClientDatabase;
typedef ClientDatabase::iterator ClientIterator;

#endif

