/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Stack algorithm: attempts to share power fairly, but gives clients as much power as they ask for.  The priority of clients changes on a preset interval.

#ifndef STACKALGORITHM_H
#define STACKALGORITHM_H

#include "applicationalgorithm.h"
#include "common.h"

#include <algorithm>
#include <sstream>
#include <string>

#include <stdio.h>

class StackAlgorithm : public ApplicationAlgorithm
{
private:
   static const double ROTATION_TIME = 30.0; // seconds

   AppId favoredApp;
   double favoredAppStartTime;
   
   std::string to_string(int i)
   {
      std::stringstream ss;
      ss << i;
      return ss.str();
   }

public:
   StackAlgorithm() : favoredApp(0), favoredAppStartTime(0.0) {}
   virtual std::string getDescription() { return "Stack algorithm: allocate power fairly but give applications their full requirements if possible."; }
   virtual void update(ClientDatabase & clients, ActionList & actions)
   {
      // Get a list of applications
      AppList apps = getApplications(clients);

      // See if it's time to rotate the favored app.  Rotating at fixed times like this may cause unnecessary
      // power shifting, but it's done so rarely that it shouldn't matter.
      double currentTime = getTime();
      if (currentTime - favoredAppStartTime >= ROTATION_TIME)
      {
         // Note that app IDs start at 1
         favoredApp = favoredApp + 1;
         if (favoredApp >= apps.size()) favoredApp = 0;
         favoredAppStartTime = currentTime;
         printTime("Updating favored app to ", to_string(favoredApp).c_str());
      }
   
      // Make a copy of the clients database
      ClientDatabase updatedClients(clients.begin(), clients.end());
      
      // Set all clients back to their allocation or to their requested power if it's less than their allocation
      // (this step is independent of the application).
      ClientIterator begin = updatedClients.begin();
      ClientIterator end = updatedClients.end();
      for(ClientIterator iter = begin; iter != end; ++iter)
      {
         iter->second.currentPowerLimit = iter->second.assignedPowerLimit;
      }
      
      // Calculate unused power.  We don't move power away from an application unless all of its nodes are checkpointing.
      double unusedPower = 0.0;
      for (AppIterator iter = apps.begin(); iter != apps.end(); ++iter)
      {
         if (allCheckpointing(*iter, updatedClients))
         {
            double appExtraPower = setPowerCheckpointing(*iter, updatedClients);
            unusedPower += appExtraPower;
         }
      }
      
      // Reallocate unused power by application
      for (int i = 0; i < apps.size(); ++i)
      {
         int appIndex = favoredApp + i;
         if (appIndex >= apps.size()) appIndex = 0;
         int app = apps[appIndex];
      
         // Don't allocate extra power to apps that have at least 1 node checkpointing (we assume that all nodes will be checkpointing soon).
         if (!isCheckpointing(app, updatedClients))
         {
            double current = getCurrentPower(app, updatedClients);
            double requested = getRequestedPower(app, updatedClients);
            if (lt(current, requested) && unusedPower > 0.0)
            {
               double extraPowerNeeded = requested - current;
               double extraPowerAvailable = std::min(extraPowerNeeded, unusedPower);
               setAllocatedPower(app, extraPowerAvailable, updatedClients);
               unusedPower -= extraPowerAvailable;
            }
         }
      }
      
      if (lt(0.0, unusedPower))
      {
         printf("@@@@>Unused power: %f\n", unusedPower);
      }
      
      // Finally, create actions for any nodes that have new power limits
      for (ClientIterator newIter = begin; newIter != end; ++newIter)
      {
         ClientIterator oldIter = clients.find(newIter->first);  // TODO: error handling
         if (!eq(newIter->second.currentPowerLimit, oldIter->second.currentPowerLimit))
         {
            AlgorithmAction action = {SET_LIMIT_ACTION, oldIter, newIter->second.currentPowerLimit};
            actions.push_back(action);
         }
      }
   }
};

#endif

