/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// PMPI wrappers that communicate with the power sharing runtime.

#include "common.h"

#include <fcntl.h>
#include <mpi.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

static int pipeFd = 0;
static struct timeval startTime;

static int sendSignal(uint64_t signal)
{
   write(pipeFd, &signal, sizeof(signal));
}

static int fileExists(const char fileName[])
{
   struct stat buffer;
   return stat(fileName, &buffer) == 0;
}

static int initializePowerSharing(int returnValue)
{
   const char * pipeName = getPipeName();
   
   pid_t pid = getpid();

   // Spin while waiting for the named fifo to be created
   printTime("Waiting for pipe to be created");
   while (!fileExists(pipeName))
      ;  // Spin

   // Set the start time.  We want this to happen after waiting for the runtime to start but before sending the start signal so that
   // we catch any time that the application is paused (the runtime can't pause us until we send it our pid in the start message).   
   gettimeofday(&startTime, NULL);
   
   // Open the named pipe
   printTime("Opening pipe");
   pipeFd = open(pipeName, O_WRONLY);
   
   // Signal to the runtime that we're starting
   printTime("Sending start signal to runtime");
   sendSignal(START_SIGNAL);
   sendSignal(pid);
   printTime("Done with initialization");
   
   return returnValue;
}

// MPI_Init/MPI_Init_thread - signals the power sharing runtime
int MPI_Init(int * argc, char *** argv)
{
   int returnValue;
   returnValue = PMPI_Init(argc, argv);
   printTime("MPI_Init");
   initializePowerSharing(returnValue);   // TODO: should this come before or after PMPI_Init?
   return returnValue;
}

int MPI_Init_thread(int * argc, char *** argv, int required, int * provided)
{
   int returnValue;
   returnValue = PMPI_Init_thread(argc, argv, required, provided);
   printf("MPI_Init_thread");
   initializePowerSharing(returnValue);
   return returnValue;
}

// MPI_Finalize - signals the power sharing runtime
int MPI_Finalize()
{
   struct timeval endTime;
   double startTimeD;
   double endTimeD;
   
   gettimeofday(&endTime, NULL);
   startTimeD = timevalToDouble(startTime);
   endTimeD = timevalToDouble(endTime);

   printTime("MPI_Finalize");
   printf("Total execution time: %f sec\n", (endTimeD - startTimeD));

   // Signal to the runtime that the application is ending
   sendSignal(STOP_SIGNAL);
   
   // Clean up
   close(pipeFd);
   
   return PMPI_Finalize();
}

int MPI_Pcontrol(const int level, ...)
{
   // Applications using this library should call MPI_Pcontrol at the start and end
   // of every checkpoint.  Pass a 0 at the start of the checkpoint and a 1 at the end.
   if (level == 0)
   {
      printTime("Started checkpoint");
      sendSignal(START_CKPT_SIGNAL);
   }
   else
   {
      printTime("Ended checkpoint");
      sendSignal(END_CKPT_SIGNAL);
   }
}
