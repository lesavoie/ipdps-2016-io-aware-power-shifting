/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Power sharing client.  This file creates a thread that will handle power allocation messages from the controller
// and sample power on this machine periodically.
//
// Known limitations:
// - Error handling is limited.
// - This assumes one rank per node.  More than one rank per node will fail.

#include "common.h"
#include "controllerinterface.h"
#include "powerutils.h"

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/eventfd.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <unistd.h>

#include <sstream>
#include <string>

using namespace std;

// Amount of time between power measurements
static const int MEASUREMENT_DELTA_NS = 5000000;    // 5 ms

// Timer constants
static const int RELATIVE_TIMER = 0;
static const timespec NULL_TIMER = {0, 0};

// Name for the named pipe
static const char * pipeName;

// Sets up a periodic timer that wakes up the thread whenever it needs to record power measurements.
static int initializeMeasurementTimer()
{
   int measurementTimerFd;
   measurementTimerFd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
   return measurementTimerFd;
}

static void startMeasurementTimer(int measurementTimerFd)
{
   timespec timeout;
   itimerspec timerSpec;

   timeout.tv_sec = 0;
   timeout.tv_nsec = MEASUREMENT_DELTA_NS;

   timerSpec.it_value = timeout;
   timerSpec.it_interval = timeout;

   timerfd_settime(measurementTimerFd, RELATIVE_TIMER, &timerSpec, NULL);
}

static int makeNamedPipe()
{
   mkfifo(pipeName, S_IRUSR | S_IWUSR);
   return open(pipeName, O_RDONLY | O_NONBLOCK);
}

static string getAppName(const string & outputFileName)
{
   // This is a cheap way to get the application name by parsing the output file name.  Doing it "right" would require having it passed in on the command line.
   size_t nameEnd = outputFileName.rfind('_');
   if (nameEnd == string::npos)
   {
      // It wasn't found, so just use the whole thing (this should never happen)
      return outputFileName;
   }
   else
   {
      return outputFileName.substr(0, nameEnd);
   }
}

static double stringToDouble(const char * str)
{
   double val;
   stringstream ss(str);
   ss >> val;
   return val;
}

int main(int argc, char * argv[])
{
   const double DEFAULT_COMPUTATION_POWER = 80.0;
   const double DEFAULT_CHECKPOINT_POWER = 40.0;
   const double DEFAULT_POWER_LIMIT = (DEFAULT_COMPUTATION_POWER + DEFAULT_CHECKPOINT_POWER) / 2.0;

   fd_set readSet;
   int measurementTimerFd;
   int controllerFd;
   int pipeFd;
   int maxFd = 0;
   uint64_t data;
   string outputFileName;
   string controllerName;
   string appName;
   bool timeToStop = false;
   double powerLimit = DEFAULT_POWER_LIMIT;
   double compPower = DEFAULT_COMPUTATION_POWER;
   double ckptPower = DEFAULT_CHECKPOINT_POWER;
   bool noController = false;

   printTime("Runtime started");

   // Extract command line parameters
   if (argc < 3)
   {
      printf("Error: must provide output file name and controller name as command line arguments\n");
      return 0;
   }
   outputFileName = argv[1];
   controllerName = argv[2];
   
   if (argc >= 4) powerLimit = stringToDouble(argv[3]);
   if (argc >= 5) compPower = stringToDouble(argv[4]);
   if (argc >= 6) ckptPower = stringToDouble(argv[5]);
   
   // If there's no controller, we just set the application to the given limit and let it run.
   if (controllerName == "nocontroller") noController = true;
   
   // For now, cheat and get the application name from the output file name
   appName = getAppName(outputFileName);
   
   // Initialization
   pipeName = getPipeName();
   remove(pipeName);    // Delete the pipe in case it exists
   initializePowerMeasurement(outputFileName);
   ControllerInterface controller(controllerName);
   
   if (!noController)
   {
      controller.connect(powerLimit, compPower, ckptPower, appName);   // TODO: error handling
      controllerFd = controller.getFileDescriptor();
   }
   else
   {
      setPowerLimit(powerLimit);
   }
   
   // This starts the timer that determines when power measurements will be taken.  Accordingly, it should
   // happen after all other initialization;
   measurementTimerFd = initializeMeasurementTimer();
   
   // Find the largest file descriptor we care about
   maxFd = max(maxFd, measurementTimerFd);
   if (!noController) maxFd = max(maxFd, controllerFd);

   // Create a named pipe.  This is also the indication to the client program that initialization is complete.
   printf("Initialization complete\n");
   pipeFd = makeNamedPipe();

   maxFd = max(maxFd, pipeFd);
   ++maxFd;

   while (!timeToStop)
   {
      // Set up the file descriptor sets for the select call
      FD_ZERO(&readSet);
      FD_SET(measurementTimerFd, &readSet);
      if (!noController) FD_SET(controllerFd, &readSet);
      FD_SET(pipeFd, &readSet);

      // Wait until we get a signal to wake up.  We don't specify a timeout since we are guaranteed to at least
      // get a signal that it's time to exit.
      // TODO: error handling
      select(maxFd, &readSet, NULL, NULL, NULL);

      // Check for signals from the MPI program
      if (FD_ISSET(pipeFd, &readSet))
      {
         // Read from the file descriptor
         ssize_t bytes = read(pipeFd, &data, sizeof(uint64_t));
         
         if (bytes > 0)
         {
            switch(data)
            {
               case START_SIGNAL:
                  printTime("Got start signal");
                  // The pid comes right after the start signal
                  read(pipeFd, &data, sizeof(data));
                  startMeasurementTimer(measurementTimerFd);
                  if (!noController)
                  {
                     controller.setPid(static_cast<pid_t>(data));
                     controller.handleStart();
                  }
                  break;
               case STOP_SIGNAL:
                  // It's time to exit
                  printTime("Got stop signal; exiting.");
                  timeToStop = true;
                  break;
               case START_CKPT_SIGNAL:
                  printTime("Application started checkpointing.");
                  if (!noController) controller.startCheckpoint();
                  break;
               case END_CKPT_SIGNAL:
                  printTime("Application finished checkpointing.");
                  if (!noController) controller.endCheckpoint();
                  break;
               default:
                  printf("Got unexpected signal from application: %ld\n", data);
                  break;
            }
         }
      }

      // See if the measurement timer went off
      if (FD_ISSET(measurementTimerFd, &readSet))
      {
         // Read the timer file descriptor to clear it and then record a power measurement.
         read(measurementTimerFd, &data, sizeof(uint64_t));

         // Record a power measurement
         recordPowerMeasurement();
      }
      
      // Check for a message from the controller
      if (!noController && FD_ISSET(controllerFd, &readSet))
      {
         controller.handleMessage();
      }
   }

   // Clean up
   if (!noController) controller.disconnect();
   close(measurementTimerFd);
   close(pipeFd);
   completePowerMeasurement();
   
   // Attempt to delete the named pipe.  If it doesn't work, don't worry about it, just give up.
   remove(pipeName);

   printTime("Client thread stopping");
   return 0;
}

