/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Power utility functions.
// Much of the code for these functions was taken from rapl_clamp.c and related files in:
// https://github.com/pyrovski/powertools

#include "common.h"

extern "C"
{
#include "msr_clocks.h"
#include "msr_common.h"
#include "msr_core.h"
#include "msr_rapl.h"
}

#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

#include <string>
#include <sstream>

using namespace std;

// Required for libmsr/librapl
extern "C"
{
mcsup_nodeconfig_t mc_config;
int mc_config_initialized = 0;
}

static rapl_state_s raplState;
static int numSockets;
static int numCores;
static FILE * powerMeasurementFile;
static FILE * freqFile;
static FILE * powerLimitFile;
static double startTime;

static void initializePowerMeasurementFile(string outputFileName)
{
   int socket;
   
   powerMeasurementFile = fopen(outputFileName.c_str(), "w");
   
   // Write the header
   fprintf(powerMeasurementFile, "timestamp\t");
   for(socket = 0; socket < numSockets; socket++)
   {
      fprintf(powerMeasurementFile, "pkg_%1$d_J\tpp0_%1$d_J\t"
#ifdef ARCH_062A
            "pp1_%1$d_J\t",
#endif
#ifdef ARCH_062D
            "dram_%1$d_J\t",
#endif
            socket);
   }
   fprintf(powerMeasurementFile, "\n");
}

static void initializeFreqFile(string outputFileName)
{
   // TODO: for now, we assume the power measurement file ends in .dat.  This should be changed to be more general, though.
   string freqFileName = outputFileName;
   size_t ext = freqFileName.find(".dat");
   if (ext == string::npos)
   {
      ext = freqFileName.size() - 1;
   }
   freqFileName.replace(ext, 4, "_freq.dat");
   
   freqFile = fopen(freqFileName.c_str(), "w");
   fprintf(freqFile, "timestamp");
   for (int core = 0; core < numCores; ++core)
   {
      fprintf(freqFile, "\tdelta_aperf%1$d\tdelta_mperf%1$d\tdelta_tsc%1$d", core);
   }
   fprintf(freqFile, "\n");
}

static void initializePowerLimitFile(string outputFileName)
{
   // TODO: for now, we assume the power measurement file ends in .dat.  This should be changed to be more general, though.
   string limFileName = outputFileName;
   size_t ext = limFileName.find(".dat");
   if (ext == string::npos)
   {
      ext = limFileName.size() - 1;
   }
   limFileName.replace(ext, 4, "_lim.dat");
   
   powerLimitFile = fopen(limFileName.c_str(), "w");
   fprintf(powerLimitFile, "timestamp\tpower_lim\n");
}

void initializePowerMeasurement(string outputFileName)
{
   FILE * infoFile;
   struct timeval currentTime;
   
   // Set up the rapl library
   char filename[] = "rapl_info.out";
   infoFile = fopen(filename, "w");
   rapl_init(&raplState, infoFile, 1);
   
   // Find out how many sockets we have
   if (mc_config_initialized == 0)
   {
      parse_proc_cpuinfo(&mc_config, &mc_config_initialized);
   }
   numSockets = mc_config.sockets;
   numCores = mc_config.cores;
   
   // Set up the output files
   initializePowerMeasurementFile(outputFileName);
   initializeFreqFile(outputFileName);
   initializePowerLimitFile(outputFileName);
   
   // Set the start time.  This makes the output easier to read by making it relative to a start time
   // rather than absolute.
   gettimeofday(&currentTime, NULL);
   startTime = timevalToDouble(currentTime);
}

static void recordFrequency(double time)
{
   const off_t MSR_TSC = 0x10;
   
   static uint64_t * prevAperf = new uint64_t[numCores];
   static uint64_t * prevMperf = new uint64_t[numCores];
   static uint64_t * prevTsc = new uint64_t[numCores];
   static bool firstTime = true;
   
   uint64_t aperf;
   uint64_t mperf;
   uint64_t tsc;
   if (!firstTime) fprintf(freqFile, "%f", time);
   for (int core = 0; core < numCores; ++core)
   {
      read_aperf_mperf(core, &aperf, &mperf);
      read_msr(core, MSR_TSC, &tsc);
      if (!firstTime)
      {
         uint64_t d_aperf = aperf - prevAperf[core];
         uint64_t d_mperf = mperf - prevMperf[core];
         uint64_t d_tsc = tsc - prevTsc[core];
         fprintf(freqFile, "\t%d\t%d\t%d", d_aperf, d_mperf, d_tsc);
      }
      prevAperf[core] = aperf;
      prevMperf[core] = mperf;
      prevTsc[core] = tsc;
   }
   if (!firstTime) fprintf(freqFile, "\n");
   firstTime = false;
}

void recordPowerMeasurement()
{
   int socket;
   struct timeval currentTime;
   double time;

   // Get the current time
   gettimeofday(&currentTime, NULL);

   // Read the power data from the MSRs
   for(socket = 0; socket < numSockets; socket++)
   {
      get_all_energy_status(socket, &raplState);
   }
   
   // Calculate the elapsed time since the start of the run
   time = timevalToDouble(currentTime) - startTime;

   // Write that data out to the file
   fprintf(powerMeasurementFile, "%lf\t", time);
   for(socket = 0; socket < numSockets; socket++)
   {
      fprintf(powerMeasurementFile, "%15.10lf\t%15.10lf\t%15.10lf\t",
            raplState.energy_status[socket][PKG_DOMAIN],
            raplState.energy_status[socket][PP0_DOMAIN],
#ifdef ARCH_062A
            raplState.energy_status[socket][PP1_DOMAIN]
#endif
#ifdef ARCH_062D
            raplState.energy_status[socket][DRAM_DOMAIN]
#endif
      );
   }
   fprintf(powerMeasurementFile, "\n");
   
   // Record the frequency for all the cores
   recordFrequency(time);
}

void completePowerMeasurement()
{
   // Close the power measurement files
   fclose(powerMeasurementFile);
   fclose(freqFile);
   fclose(powerLimitFile);

   // This closes the file descriptor passed to rapl_init (among other things)
   rapl_finalize(&raplState, 1);
}

void setPowerLimit(double watts)
{
   static const int windowSize = 0;
   int i;
   double time = getTime() - startTime;
   
   stringstream message;
   message << "Setting power limit to " << watts << ", time: " << time;
   printTime(message.str().c_str());
   
   // TODO: this currently only sets the power limit for the package.  I might need to set it separately for the cores and DRAM.
   for(i = 0; i < numSockets; i++)
   {
      power_limit_s power_limit;

      power_limit.lock = 0,
      power_limit.clamp_1 = 1,
      power_limit.enable_1 = 1,
      power_limit.power_limit_1 = UNIT_DESCALE(watts, raplState.power_unit[i].power),
      power_limit.time_multiplier_1 = (windowSize >> 5) & 0b11,
      power_limit.time_window_1 = windowSize & 0b11111,
      power_limit.clamp_2 = 1,
      power_limit.enable_2 = 1,
      power_limit.power_limit_2 = UNIT_DESCALE(watts, raplState.power_unit[i].power),
      power_limit.time_multiplier_2 = (windowSize >> 5) & 0b11,
      power_limit.time_window_2 = windowSize & 0b11111,

      set_power_limit(i, PKG_DOMAIN, &power_limit);
   }

   // Write the power limit to the file
   struct timeval currentTime;

   // Get the current time
   gettimeofday(&currentTime, NULL);

   // Calculate the elapsed time since the start of the run
   time = timevalToDouble(currentTime) - startTime;

   // Write that data out to the file
   fprintf(powerLimitFile, "%lf\t%f\n", time, watts);
}

// For testing
double getStartTime()
{
   return startTime;
}
