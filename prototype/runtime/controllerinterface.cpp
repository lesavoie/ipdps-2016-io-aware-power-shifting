/*
    Copyright 2017 Lee Savoie

    This file is part of PowerShifter.

    PowerShifter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PowerShifter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
*/
// Defines the interface to the controller node.

#include "common.h"
#include "controllerinterface.h"
#include "controllercommon.h"
#include "powerutils.h"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <netdb.h>
#include <unistd.h>

#include <string>

using namespace std;

ControllerInterface::ControllerInterface(const string & controllerName) :
   controllerName_(controllerName),
   fd_(0),
   pid_(-1),
   paused_(false)
{
   // Do nothing.
}

void ControllerInterface::connect(double powerLimit, double compPower, double ckptPower, const string & appName)
{
   // TODO: probably should have a return value for error handling
   fd_ = socket(AF_INET, SOCK_STREAM, 0);
   if (fd_ < 0)
   {
      printf("Error: could not open socket.\n");
   }
   hostent * server = gethostbyname(controllerName_.c_str());
   if (server == NULL)
   {
      printf("Error: could not get host.\n");
   }
   sockaddr_in serverAddress;
   bzero((char *)&serverAddress, sizeof(serverAddress));
   serverAddress.sin_family = AF_INET;
   bcopy((char *)server->h_addr, 
         (char *)&serverAddress.sin_addr.s_addr,
         server->h_length);
   serverAddress.sin_port = htons(CONTROLLER_PORT);
   if (::connect(fd_, reinterpret_cast<sockaddr *>(&serverAddress), sizeof(serverAddress)) < 0)
   {
      printf("Error: could not connect to server.\n");
   }
   
   sendInitMessage(powerLimit, compPower, ckptPower, appName);
}

void ControllerInterface::sendInitMessage(double powerLimit, double compPower, double ckptPower, const string & appName)
{
   InitPayload payload;
   gethostname(payload.node, MAX_NODE_NAME_LENGTH);
   payload.node[MAX_NODE_NAME_LENGTH - 1] = '\0';
   strncpy(payload.app, appName.c_str(), MAX_APP_NAME_LENGTH);
   payload.app[MAX_APP_NAME_LENGTH - 1] = '\0';
   payload.assignedLimit = powerLimit;
   payload.compPower = compPower;
   payload.ckptPower = ckptPower;
   sendControllerMessage(fd_, INIT_MESSAGE, payload);
}

void ControllerInterface::disconnect()
{
   // Send a message to the controller indicating that we are done
   DisconnectPayload payload;
   sendControllerMessage(fd_, DISCONNECT_MESSAGE, payload);
   
   // Close the socket
   close(fd_);
   fd_ = 0;
}

void ControllerInterface::startCheckpoint()
{
   StartCkptPayload payload;
   sendControllerMessage(fd_, START_CKPT_MESSAGE, payload);
}

void ControllerInterface::endCheckpoint()
{
   EndCkptPayload payload;
   sendControllerMessage(fd_, END_CKPT_MESSAGE, payload);
}

void ControllerInterface::pauseApplication()
{
   paused_ = true;
   if (pid_ >= 0) kill(pid_, SIGSTOP);
   printTime("Pausing application.");
}

void ControllerInterface::continueApplication()
{
   paused_ = false;
   if (pid_ >= 0) kill(pid_, SIGCONT);
   printTime("Continuing application.");
}

int ControllerInterface::getFileDescriptor()
{
   return fd_;
}

void ControllerInterface::setPid(pid_t pid)
{
   pid_ = pid;
}

void ControllerInterface::handleStart()
{
   AppStartPayload payload;
   sendControllerMessage(fd_, APP_START_MESSAGE, payload);
   // We resend the stop signal on application startup in case it missed the first one.
   if (paused_) pauseApplication();
}

void ControllerInterface::handleMessage()
{
   GenericMessage message;
   size_t size = recvControllerMessage(fd_, message);
   if (size < 0)
   {
      printf("Error: problem receiving message from controller.\n");
   }
   handleMessage(message);
}

void ControllerInterface::handleMessage(const GenericMessage & message)
{
   switch(message.messageType)
   {
      case POWER_LIMIT_MESSAGE:
         handleMessage(castMessage<PowerLimitPayload>(message));
         break;
      case PAUSE_APP_MESSAGE:
         handleMessage(castMessage<PauseAppPayload>(message));
         break;
      case CONTINUE_APP_MESSAGE:
         handleMessage(castMessage<ContinueAppPayload>(message));
         break;
      default:
         printf("Got unexpected message %d from controller.\n", message.messageType);
         break;
   }
}

void ControllerInterface::handleMessage(const Message<PowerLimitPayload> * message)
{
   // Impose some sanity checks
   double newLimit = message->payload.limit;
   if (newLimit >= 20.0)
   {
      printTime("Updating power limit.");
      setPowerLimit(newLimit);
   }
   else
   {
      printf("Got unexpected power limit from controller: %f\n", newLimit);
   }
}

void ControllerInterface::handleMessage(const Message<PauseAppPayload> * message)
{
   pauseApplication();
}

void ControllerInterface::handleMessage(const Message<ContinueAppPayload> * message)
{
   continueApplication();
}

