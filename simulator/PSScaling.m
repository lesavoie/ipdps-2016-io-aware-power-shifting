% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
classdef PSScaling
    properties (Constant)
        % Defines the different sets of scaling characteristics we can use (most
        % are derived by running the application on real hardware).  The values of
        % these constants is not important; they just need to be different.
        PARADIS_SCALING = 1;
        LAMMPS_SCALING = 2;
        CACTUS_SCALING = 3;
        CUSTOM_SCALING = 4;
        NO_SCALING = 5;
        
        PARADIS_POWER = [40; 50; 60; 70; 80; 90; 91];
        PARADIS_SPEED = 117.26736 ./ [240.272882; 209.341929; 165.724184; 152.342891; 124.366892; 117.791105; 117.26736];
        
        LAMMPS_POWER = [40; 50; 60; 70; 80; 88];
        LAMMPS_SPEED = 679.0018 ./ [858.4011; 766.5445; 712.0962; 697.3447; 686.6095; 679.0018];
        
        CACTUS_POWER = [50; 60; 70; 80; 90; 100; 102];
        CACTUS_SPEED = 167.2678 ./ [280.0086; 210.5007; 194.8711; 178.649; 170.2256; 169.4657; 167.2678];

        % Custom scaling is based on ParaDiS scaling but fitted to a curve
        % and smoothed.  The speed numbers are calculated from
        % y = -6.237e-4*x.^3 + 0.175*x.^2 - 17.318*x + 715.047
        % where x is the power measurements.  This formula was produced
        % using the curve fitting function in Google Sheets.
        CUSTOM_POWER = [40; 50; 60; 70; 80; 90; 91];
        CUSTOM_SPEED = 118.2818 ./ [262.4102; 208.6845; 171.2478; 146.3579; 130.2726; 119.2497; 118.2818];
    end
    methods
        function progressFactor = getComputingProgressFactor(obj, scaling, phasePower, checkpointPower, scalingFactor, usage)
            [MEAS_SPEED, MEAS_POWER] = obj.getScalingMeasurements(scaling, phasePower, checkpointPower, scalingFactor);

            SIM_POWER_LIMITS = [checkpointPower; phasePower];
            MEAS_POWER_LIMITS = [min(MEAS_POWER); max(MEAS_POWER)];

            % Use linear interpolation to map from simulated power to measured
            % power.
            measUsed = obj.linterp(SIM_POWER_LIMITS, MEAS_POWER_LIMITS, usage);

            % Use linear iterpolation to match the result of the previous calculation
            % with a progress factor.
            progressFactor = obj.linterp(MEAS_POWER, MEAS_SPEED, measUsed);
            
            if progressFactor > 1.0
                % If this happens, it's a coding error.
                error([getTask() '****Error: progress factor greater than 1****']);
            end
        end
    end
    methods (Access = private)
        function [MEAS_SPEED, MEAS_POWER] = getScalingMeasurements(obj, scaling, phasePower, checkpointPower, scalingFactor)
            % Most of these use measured power data from actual tests.  The power
            % numbers given are actual power usage, not just power limits (in some
            % cases, the power used was below the limit).  This function first maps the
            % simulated power to the same range as the measured power, and then maps
            % that to a progress factor.  Both steps use linear interpolation.
            if scaling == obj.CUSTOM_SCALING
                % Use ParaDiS as a sample application and then adjust its power and
                % speed to match the new application.

                % To calculate new power, use y = mx + b where the coordinates are (old min
                % power, new min power) and (old max power, new max power).
                m = (phasePower - checkpointPower) / (obj.CUSTOM_POWER(end) - obj.CUSTOM_POWER(1));
                b = phasePower - m * obj.CUSTOM_POWER(end);
                MEAS_POWER = m * obj.CUSTOM_POWER + b;

                % For speed, scale by the scaling factor and then add a constant to
                % get the max speed back to 1.
                MEAS_SPEED = obj.CUSTOM_SPEED * scalingFactor;
                diff = 1 - MEAS_SPEED(end);
                MEAS_SPEED = MEAS_SPEED + diff;
            elseif scaling == obj.PARADIS_SCALING
                MEAS_POWER = obj.PARADIS_POWER;
                MEAS_SPEED = obj.PARADIS_SPEED;
            elseif scaling == obj.LAMMPS_SCALING
                MEAS_POWER = obj.LAMMPS_POWER;
                MEAS_SPEED = obj.LAMMPS_SPEED;
            elseif scaling == obj.CACTUS_SCALING
                MEAS_POWER = obj.CACTUS_POWER;
                MEAS_SPEED = obj.CACTUS_SPEED;
            elseif scaling == obj.NO_SCALING
                % Non-scaling model for testing
                MEAS_POWER = [50, 100];
                MEAS_SPEED = [1, 1];
            else
                error('Invalid scaling model');
            end
        end

        function y = linterp(obj, xs, ys, x)
            % Simple linear interpolation.  I wrote this function because the iterp1q
            % calls were the longest part of the code, and this ended up being an order
            % of magnitude faster.
            if x < xs(1)
                y = ys(1);
            elseif x >= xs(end)
                y = ys(end);
            else
                % Find the right interval
                num = length(xs) - 1;
                for i = 1:num
                    if x >= xs(i) && x < xs(i + 1)
                        y = ys(i) + (ys(i + 1) - ys(i)) * (x - xs(i)) / (xs(i + 1) - xs(i));
                        break;
                    end
                end
            end
        end
    end
end