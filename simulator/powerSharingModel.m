% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.

% Disable some common warnings.
%#ok<*AGROW>
%#ok<*NBRAK>

function powerSharingModel(run)

% Record all output to a file
diary('output.txt');

global MTTF;
global FAILURE_ALLOWED;
global INCREMENT;
global DO_PLOTS;
global SHARE_ONLY_DURING_CHECKPOINTS;
global MACHINE_SIZE;
global DEFAULT_POWER_LIMIT;

%------------------------------------------------------------------------%
% Set global "constants".  These can be used to configure the run.

% Mean time to failure per node.
MTTF = 20000 * 64;  % Seconds

% Determines whether to randomly insert application failures
FAILURE_ALLOWED = true;

% Defines how often the simulation updates its calculations.  Times used in
% the simulation should be large relative to this increment to minimize
% errors in calculations.
INCREMENT = 0.1;      % Seconds

% Number of times to run each scenario.  This should only be set to 1
% unless there is non-determinism, which currently only happens when there
% are failures and restarts.
MONTE_CARLO_RUNS = 5;

% Determines whether charts of power usage are plotted or not
DO_PLOTS = false;

% If this is true, apps will only donate power during checkpoints (not
% other low power phases).
SHARE_ONLY_DURING_CHECKPOINTS = false;

% Number of random apps to generate for scheduling runs
NUM_SCHEDULING_APPS = 50;

% Number of random apps to generate for power limit sweep runs.
NUM_POWER_LIMIT_APPS = 50;

% Number of nodes in the simulated machine
MACHINE_SIZE = 512;

% Seed for random number generation.  If this is less than zero, a random
% seed will be chosen.  Set it to a non-negative number for repeatable
% runs.
RAND_SEED = 877056319;

% Default power limit per node in watts.
DEFAULT_POWER_LIMIT = 60;

%------------------------------------------------------------------------%

if nargin < 1
%------------------------------------------------------------------------%
% Use this to select which run to set up.  If you want to do a new run, add
% a new variable to PSRun and a new if statement to set it up below.
run = PSRun.POWER_LIMIT_SWEEP_10_CONTROL_RUN;
NUM_POWER_LIMIT_APPS = 3;
MONTE_CARLO_RUNS = 1;
%------------------------------------------------------------------------%
end

% Get a seed for the random number generator (to change the seed, change
% the RAND_SEED constant above).
if RAND_SEED < 0
    s = RandStream('mt19937ar','Seed','shuffle');
    seed = s.Seed;
else
    seed = RAND_SEED;
end

paramSweep = false;
numSweepApps = 0;
ckptData = NO_CKPT_DATA;
ckptPercent = -1.0;

% This value is not used as part of this file, but it is saved in the
% results and used in the offline analysis.
paramSweepConfigs = -1; %#ok<NASGU>

% Test runs
if run == PSRun.TEST_RUN
    % No need to save the output when running unit tests.
    diary off;
    runTests;
    return;
elseif run == PSRun.TEMP_RUN
    % This run is meant to be changed as needed for testing/debugging
    % purposes.
    MONTE_CARLO_RUNS = 1;
    MACHINE_SIZE = 2;
    app(1) = initialize('App 1', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(2) = initialize('App 2', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    config(1) = setConfig('Control', CONTROL_SHARING, 0.1, 0, DEFAULT_POWER_LIMIT);

% Validation runs
elseif run == PSRun.LAMMPS_PARADIS_CACTUS_RUN
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    [app, config] = lammpsParadisCactusRun;
elseif run == PSRun.LAMMPS_CACTUS_RUN
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    [app, config] = lammpsCactusRun;
elseif run == PSRun.PARADIS4_RUN
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    [app, config] = paradis4Run;
elseif run == PSRun.CACTUS4_RUN
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    [app, config] = cactus4Run;
elseif run == PSRun.LAMMPS_BEST_CASE_RUN
    % This run is mentioned in the validation section (because it had the
    % biggest difference between real-world and simulation) but not
    % included in the chart.
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    [app, config] = lammpsBestCaseRun;
elseif run == PSRun.LAMMPS_BEST_CASE_RUN_FIXED
    % This is the same as the run above except reworked based on the high
    % power run.  Both cases include two runs - one at 60W and one at 80W.
    % If I base the model parameters on the 60W run (which is LAMMPS_BEST_CASE_RUN),
    % that run is very close but the 80W run is off.  This bases the model
    % parameters on the 80W run, so that run is close and the 60W run is
    % off.
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    [app, config] = lammpsBestCaseRunFixed;
    
% Capacity machine runs
elseif (run >= PSRun.CAPACITY_10_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_50_END) || ...
       (run >= PSRun.CAPACITY_EDISON_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_INTREPID_END)
    
    if run < PSRun.CAPACITY_50_END
        metric = AVERAGE_IMPROVEMENT;
    elseif run < PSRun.CAPACITY_MAX_50_END
        metric = MAX_IMPROVEMENT;
        
    % Runs based on I/O data from real machines
    elseif run < PSRun.CAPACITY_EDISON_END
        metric = AVERAGE_IMPROVEMENT;
        ckptData = EDISON_CKPT_DATA;
    elseif run < PSRun.CAPACITY_MIRA_END
        metric = AVERAGE_IMPROVEMENT;
        ckptData = MIRA_CKPT_DATA;
    elseif run < PSRun.CAPACITY_INTREPID_END
        metric = AVERAGE_IMPROVEMENT;
        ckptData = INTREPID_CKPT_DATA;
    elseif run < PSRun.CAPACITY_MAX_EDISON_END
        metric = MAX_IMPROVEMENT;
        ckptData = EDISON_CKPT_DATA;
    elseif run < PSRun.CAPACITY_MAX_MIRA_END
        metric = MAX_IMPROVEMENT;
        ckptData = MIRA_CKPT_DATA;
    elseif run < PSRun.CAPACITY_MAX_INTREPID_END
        metric = MAX_IMPROVEMENT;
        ckptData = INTREPID_CKPT_DATA;
    else
        error('Invalid improvement metric in capacity sweep (should be impossible)');
    end
    
    sharing = getSharing(run);
    
    if (run >= PSRun.CAPACITY_10_NO_SHARING_RUN && run < PSRun.CAPACITY_10_END) || ...
       (run >= PSRun.CAPACITY_MAX_10_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_10_END)
        ckptPercent = 0.10;
    elseif (run >= PSRun.CAPACITY_20_NO_SHARING_RUN && run < PSRun.CAPACITY_20_END) || ...
           (run >= PSRun.CAPACITY_MAX_20_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_20_END)
        ckptPercent = 0.20;
    elseif (run >= PSRun.CAPACITY_30_NO_SHARING_RUN && run < PSRun.CAPACITY_30_END) || ...
           (run >= PSRun.CAPACITY_MAX_30_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_30_END)
        ckptPercent = 0.30;
    elseif (run >= PSRun.CAPACITY_40_NO_SHARING_RUN && run < PSRun.CAPACITY_40_END) || ...
           (run >= PSRun.CAPACITY_MAX_40_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_40_END)
        ckptPercent = 0.40;
    elseif (run >= PSRun.CAPACITY_50_NO_SHARING_RUN && run < PSRun.CAPACITY_50_END) || ...
           (run >= PSRun.CAPACITY_MAX_50_NO_SHARING_RUN && run < PSRun.CAPACITY_MAX_50_END)
        ckptPercent = 0.50;
    elseif ckptData > NO_CKPT_DATA
        % Checkpoint percentages will be defined by individual apps.
        ckptPercent = -1.0;
    else
        error('Could not figure out checkpoint percent (should be impossible)');
    end
    
    [app, config] = capacityRun(ckptPercent, sharing, metric);
    
% Power limit sweep runs
elseif run >= PSRun.POWER_LIMIT_SWEEP_10_NO_SHARING_RUN && run < PSRun.POWER_LIMIT_SWEEP_50_END
    FAILURE_ALLOWED = false;
   
    sharing = getSharing(run);
    
    if run >= PSRun.POWER_LIMIT_SWEEP_10_NO_SHARING_RUN && run < PSRun.POWER_LIMIT_SWEEP_10_END
        ckptPercent = 0.10;
    elseif run >= PSRun.POWER_LIMIT_SWEEP_30_NO_SHARING_RUN && run < PSRun.POWER_LIMIT_SWEEP_30_END
        ckptPercent = 0.30;
    elseif run >= PSRun.POWER_LIMIT_SWEEP_50_NO_SHARING_RUN && run < PSRun.POWER_LIMIT_SWEEP_50_END
        ckptPercent = 0.50;
    else
        error('Could not figure out checkpoint percent (should be impossible)');
    end

    [app, config] = powerLimRun(ckptPercent, sharing);
    paramSweep = true;
    paramSweepConfigs = 1; %#ok<NASGU>
    
    numSweepApps = NUM_POWER_LIMIT_APPS;

% Sweeps with perfectly aligned I/O phases
elseif run >= PSRun.ALIGNED_SWEEP_10_NO_SHARING_RUN && run < PSRun.ALIGNED_SWEEP_50_END
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;
    
    sharing = getSharing(run);
    
    if run >= PSRun.ALIGNED_SWEEP_10_NO_SHARING_RUN && run < PSRun.ALIGNED_SWEEP_10_END
        ckptPercent = 0.10;
    elseif run >= PSRun.ALIGNED_SWEEP_30_NO_SHARING_RUN && run < PSRun.ALIGNED_SWEEP_30_END
        ckptPercent = 0.30;
    elseif run >= PSRun.ALIGNED_SWEEP_50_NO_SHARING_RUN && run < PSRun.ALIGNED_SWEEP_50_END
        ckptPercent = 0.50;
    else
        error('Could not figure out checkpoint percent (should be impossible)');
    end
    
    app(1) = initialize('App 1', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(2) = initialize('App 2', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(3) = initialize('App 3', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(4) = initialize('App 4', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(5) = initialize('App 5', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(6) = initialize('App 6', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    paramSweep = true;
    paramSweepConfigs = 1;
    numSweepApps = length(app);
    name = [nameFromSharingAlg(sharing) ': ' num2str(ckptPercent * 100) '%'];
    c = 1;
    for numApps = 2:1:numSweepApps
        config(c) = setConfig([name ', ' num2str(numApps) ' apps'], sharing, ckptPercent, 0, DEFAULT_POWER_LIMIT, -1, [], numApps);
        c = c + paramSweepConfigs;
    end

% Sweeps with partially algined I/O phases
elseif run >= PSRun.PARTIALLY_ALIGNED_SWEEP_10_NO_SHARING_RUN && run < PSRun.PARTIALLY_ALIGNED_SWEEP_50_END
    FAILURE_ALLOWED = false;
    MONTE_CARLO_RUNS = 1;

    sharing = getSharing(run);
    
    if run >= PSRun.PARTIALLY_ALIGNED_SWEEP_10_NO_SHARING_RUN && run < PSRun.PARTIALLY_ALIGNED_SWEEP_10_END
        ckptPercent = 0.10;
    elseif run >= PSRun.PARTIALLY_ALIGNED_SWEEP_30_NO_SHARING_RUN && run < PSRun.PARTIALLY_ALIGNED_SWEEP_30_END
        ckptPercent = 0.30;
    elseif run >= PSRun.PARTIALLY_ALIGNED_SWEEP_50_NO_SHARING_RUN && run < PSRun.PARTIALLY_ALIGNED_SWEEP_50_END
        ckptPercent = 0.50;
    else
        error('Could not figure out checkpoint percent (should be impossible)');
    end
    
    app(1) = initialize('App 1', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(2) = initialize('App 2', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(3) = initialize('App 3', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(4) = initialize('App 4', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(5) = initialize('App 5', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    app(6) = initialize('App 6', [80], [1], 40, 68, 10, 1050, CHECKPOINT_BY_ITERATIONS, PSScaling.CUSTOM_SCALING, false, 0, 1, 1);
    offset = 5;
    paramSweep = true;
    paramSweepConfigs = 1;
    numSweepApps = length(app);
    name = [nameFromSharingAlg(sharing) ': ' num2str(ckptPercent * 100) '%'];
    c = 1;
    for numApps = 2:1:numSweepApps
        config(c) = setConfig([name ', ' num2str(numApps) ' apps'], sharing, ckptPercent, offset, DEFAULT_POWER_LIMIT, -1, [], numApps);
        c = c + paramSweepConfigs;
    end
    
else
    error('Invalid run specified.');
end

%------------------------------------------------------------------------%
% In most cases, you will not have to modify anything below this line.
%------------------------------------------------------------------------%

NUM_APPS = length(app);
NUM_CONFIGS = length(config);

if NUM_APPS == 0
    if paramSweep
        schedulingRun = false;
        NUM_APPS = numSweepApps;
    else
        schedulingRun = true;
        NUM_APPS = NUM_SCHEDULING_APPS;
    end
else
    schedulingRun = false;
end

% Do some sanity checks on the apps and configs
if ~schedulingRun && ~paramSweep
    errorCheck(app, NUM_APPS, config, NUM_CONFIGS);
end

results = zeros(NUM_CONFIGS, NUM_APPS);
speedups = zeros(NUM_CONFIGS, MONTE_CARLO_RUNS);

disp('--------------------------------------------------------------------');

startTime = clock;
disp(['Start time: ' datestr(startTime)]);

for run = 1:MONTE_CARLO_RUNS

    % This was the easiest method I could find to force the seed to be
    % different for each run.
    % Documentation and testing both indicate that each worker has an
    % independent random number stream, so starting them each at the
    % same seed will guarantee identical results regardless of
    % interleaving.
    % See http://www.mathworks.com/help/distcomp/control-random-number-streams.html
    runSeed = seed + run;

    if schedulingRun || (paramSweep && isempty(app))
        % We assume all configs have the same size machine
        myapp = getTraceApps(NUM_APPS, runSeed, paramSweep, ckptData);
    else
        myapp = app;
    end

    localResults = zeros(NUM_CONFIGS, NUM_APPS);

    for c = 1:NUM_CONFIGS
        localResults(c,:) = runSimulation(myapp, config(c), runSeed);
        disp(['Total run time after run ' num2str(run) ', config ' num2str(c) ': ' num2str(etime(clock, startTime)) ' seconds']);
    end

    results = results + localResults;

    if schedulingRun || paramSweep
        % Print out the applications
        disp('--------------------------------------------------------------------');
        disp('Applications:');
        printApplications(myapp, NUM_APPS);
        
        % Print the results
        printResults(myapp, NUM_APPS, config, NUM_CONFIGS, localResults, 1);
        disp('--------------------------------------------------------------------');

        totals = sum(localResults, 2);
        localSpeedups = getSpeedups(1, NUM_CONFIGS, totals);
        speedups(:,run) = localSpeedups;
    end
    
    disp(['Time after run ' num2str(run) ': ' datestr(clock)]);
    disp(['Total run time after run ' num2str(run) ': ' num2str(etime(clock, startTime)) ' seconds']);

end

% This variable was originally intended to allow the user to cancel partway
% through a run.  That was never implemented, so the runs completed are
% always the same as the runs requested.
runsCompleted = MONTE_CARLO_RUNS;

% Print out the environment
disp('--------------------------------------------------------------------');
disp('Environment:');
disp(['MTTF (per node): ' num2str(MTTF)]);
disp(['Starting seed: ' num2str(seed)]);
disp(['Number of runs: ' num2str(MONTE_CARLO_RUNS)]);
disp(['Machine size: ' num2str(MACHINE_SIZE)]);
disp(['Time increment for calculations: ' num2str(INCREMENT)]);
disp(['Sharing only during checkpoints: ' num2str(SHARE_ONLY_DURING_CHECKPOINTS)]);
disp(['Failures allowed: ' num2str(FAILURE_ALLOWED)]);
disp(['Checkpoint data: ' num2str(ckptData)]);
disp(['Checkpoint percent: ' num2str(ckptPercent)]);

% Print out the configurations
disp('--------------------------------------------------------------------');
disp('Configurations:');
printConfigs(config, NUM_CONFIGS);

if ~schedulingRun && ~paramSweep

    % Print out the applications
    disp('--------------------------------------------------------------------');
    disp('Applications:');
    printApplications(app, NUM_APPS);

    % Print the final results
    printResults(app, NUM_APPS, config, NUM_CONFIGS, results, runsCompleted);

end

disp('--------------------------------------------------------------------');

% Store the results
if schedulingRun || paramSweep
    save results.mat
end

disp('--------------------------------------------------------------------');

endTime = clock;
disp(['End time: ' datestr(endTime)]);
disp(['Total run time: ' num2str(etime(endTime, startTime)) ' seconds']);

diary off;

% Give the user an audible indication that the simulation has finished.
beep;

end

%------------------------------------------------------------------------%
% Set up functions: these functions set up specific scenarios.

function [app, config] = paradis4Run()
% This setup is designed to test the paradis4 algorithm - 4 instances of
% ParaDiS running together with the power concentrated on 1 of them
CHECKPOINT_POWER = 40;
CHECKPOINT_INTERVAL = 1;  % iterations (really 100 iterations but I made the iterations 100 times longer)
CHECKPOINT_COST = 3.82;
POWER_LIMIT = 60;
CHECKPOINT_METHOD = CHECKPOINT_BY_ITERATIONS;
PHASE_POWER = [91];
PHASE_TIME = [6.4];
CHECKPOINT_AT_END = true;
WORK_TO_DO = 50;

% function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, checkpointMethod, scaling)
app(1) = initialize('PARADIS1', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, PSScaling.PARADIS_SCALING, CHECKPOINT_AT_END);
app(2) = initialize('PARADIS2', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, PSScaling.PARADIS_SCALING, CHECKPOINT_AT_END);
app(3) = initialize('PARADIS3', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, PSScaling.PARADIS_SCALING, CHECKPOINT_AT_END);
app(4) = initialize('PARADIS4', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, PSScaling.PARADIS_SCALING, CHECKPOINT_AT_END);
% function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset, powerLimit)
config(1) = setConfig('no power sharing', NO_SHARING, 0, 0, POWER_LIMIT);
config(2) = setConfig('power sharing', APP4_SHARING, 0, 3, POWER_LIMIT);
end

function [app, config] = lammpsBestCaseRun()
% This setup is designed to test the LAMMPS best case runs
% function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, workToDo, checkpointMethod, scaling, checkpointAtEnd)
app(1) = initialize('LAMMPS', [88], [87.4], 40, 1, 10.9, 8, CHECKPOINT_BY_ITERATIONS, PSScaling.LAMMPS_SCALING, true);   % Uses 60W run to calculate iteration time
% function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset, powerLimit)
config(1) = setConfig('no sharing', NO_SHARING, 0, 0, 60);
config(2) = setConfig('best case', NO_SHARING, 0, 0, 80);
end

function [app, config] = lammpsBestCaseRunFixed()
% This setup is designed to test the LAMMPS best case runs
% function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, workToDo, checkpointMethod, scaling, checkpointAtEnd)
app(1) = initialize('LAMMPS', [88], [84.2], 40, 1, 10.9, 8, CHECKPOINT_BY_ITERATIONS, PSScaling.LAMMPS_SCALING, true);   % Uses 80W run to calculate iteration time
% function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset, powerLimit)
config(1) = setConfig('no sharing', NO_SHARING, 0, 0, 60);
config(2) = setConfig('best case', NO_SHARING, 0, 0, 80);
end

function [app, config] = lammpsParadisCactusRun()
% Simulates a run with LAMMPS, ParaDiS, and Cactus all running together.
% function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, workToDo, checkpointMethod, scaling, checkpointAtEnd)
app(1) = initialize('LAMMPS', [88], [85.8], 40, 1, 11, 15, CHECKPOINT_BY_ITERATIONS, PSScaling.LAMMPS_SCALING, true);
app(2) = initialize('ParaDiS', [91], [6.8], 40, 1, 3.3, 100, CHECKPOINT_BY_ITERATIONS, PSScaling.PARADIS_SCALING, true);
% The checkpoint interval for this app was carefully chosen to cause 5
% checkpoints without sharing and 4 checkpoints with, because that's what
% happened in the real run.  However, the checkpoints actually happen at
% different times, but the scaling should come out about the same.
app(3) = initialize('Cactus', [102], [10.6], 50, 305, 55, 100, CHECKPOINT_BY_TIME, PSScaling.CACTUS_SCALING, true);
% function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset, powerLimit)
config(1) = setConfig('no sharing', NO_SHARING, 0, 0, 65);
config(2) = setConfig('sharing', STACK_SHARING, 0, 0, 65);
end

function [app, config] = lammpsCactusRun()
% Simulates a run with LAMMPS and Cactus running together.
% function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, workToDo, checkpointMethod, scaling, checkpointAtEnd)
app(1) = initialize('LAMMPS', [88], [85.7], 40, 1, 11.2, 15, CHECKPOINT_BY_ITERATIONS, PSScaling.LAMMPS_SCALING, true);
app(2) = initialize('Cactus', [102], [10.5], 50, 280, 55.4, 100, CHECKPOINT_BY_TIME, PSScaling.CACTUS_SCALING, true);
% function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset, powerLimit)
config(1) = setConfig('no sharing', NO_SHARING, 0, 0, 65);
config(2) = setConfig('sharing', STACK_SHARING, 0, 0, 65);
end

function [app, config] = cactus4Run()
% This setup is designed to test the paradis4 algorithm - 4 instances of
% ParaDiS running together with the power concentrated on 1 of them
CHECKPOINT_POWER = 50;
CHECKPOINT_INTERVAL = 310;
CHECKPOINT_COST = 55.6;
CHECKPOINT_METHOD = CHECKPOINT_BY_TIME;
PHASE_POWER = [102];
PHASE_TIME = [1.05];
CHECKPOINT_AT_END = true;
WORK_TO_DO = 1000;
POWER_LIMIT = 65;
SCALING = PSScaling.CACTUS_SCALING;

% function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, checkpointMethod, scaling)
app(1) = initialize('Cactus 1', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, SCALING, CHECKPOINT_AT_END);
app(2) = initialize('Cactus 2', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, SCALING, CHECKPOINT_AT_END);
app(3) = initialize('Cactus 3', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, SCALING, CHECKPOINT_AT_END);
app(4) = initialize('Cactus 4', PHASE_POWER, PHASE_TIME, CHECKPOINT_POWER, CHECKPOINT_INTERVAL, CHECKPOINT_COST, WORK_TO_DO, CHECKPOINT_METHOD, SCALING, CHECKPOINT_AT_END);
% function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset, powerLimit)
config(1) = setConfig('no power sharing', NO_SHARING, 0, 0, POWER_LIMIT);
config(2) = setConfig('power sharing', APP4_SHARING, 0, 55, POWER_LIMIT);
end

function sharing = getSharing(run)
algNo = mod(run, 10);
if algNo == 0
    sharing = NO_SHARING;
elseif algNo == 1
    sharing = SPREAD_SHARING;
elseif algNo == 2
    sharing = STAGGER_SHARING;
elseif algNo == 3
    sharing = CONTROL_SHARING;
elseif algNo == 4
    sharing = CPU_STACK_SHARING;
elseif algNo == 5
    sharing = STAGGER_STACK_SHARING;
elseif algNo == 6
    sharing = CONTROL_STACK_SHARING;
else
    error('Invalid sharing method (should be impossible)');
end
end

function [app, config] = capacityRun(ckptPercent, sharingAlg, resultWanted)
global FAILURE_ALLOWED;

if resultWanted == MAX_IMPROVEMENT
    FAILURE_ALLOWED = false;
end

[app, config] = traceRun(ckptPercent, sharingAlg);
end

function [app, config] = traceRun(ckptPercent, sharing)
global DEFAULT_POWER_LIMIT;

app = [];

name = nameFromSharingAlg(sharing);
% setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset,  powerLimit, scalingFactor, nodeCounts, numApps)
config(1) = setConfig(name, sharing, ckptPercent, 0, DEFAULT_POWER_LIMIT, -1, [], -1);
end

function [app, config] = powerLimRun(ckptPercent, sharing)
app = [];

name = [nameFromSharingAlg(sharing) ': ' num2str(ckptPercent * 100) '%'];
c = 1;
for power = 0:2.5:40
    % setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset,  powerLimit, scalingFactor, nodeCounts, numApps)
    config(c) = setConfig([name ', ' num2str(40 + power) 'W'], sharing, ckptPercent, 0, 40 + power, -1, [], -1);
    c = c + 1;
end

end

%------------------------------------------------------------------------%

function homeDir = getHomeDir
if ispc
    homeDir = [getenv('USERPROFILE') '\'];
else
    homeDir = [getenv('HOME') '/'];
end
end

function trace = readFromTrace(numApps)
global MACHINE_SIZE;

% Reads data about apps from a trace.  The remaining data will be randomly
% generated.
homeDir = getHomeDir;
filename = [homeDir 'RICC-2010-2.swf'];
trace = [];

fid = fopen(filename, 'rt');

% Skip the comments at the start of the file
while true
    line = textscan(fid, '%s', 1, 'delimiter', '\n');
    if feof(fid) || line{1}{1}(1) ~= ';'
        break;
    end
end
% Note that this skips the first line that wasn't a comment, but we were
% going to skip that anyway.

% Set the maximum job size
maxJobSize = floor(MACHINE_SIZE / 4);

% Skip the first several jobs.
% Could randomize this number to get something different.
jobsToSkip = 447729;
textscan(fid, '%s', jobsToSkip, 'delimiter', '\n');

% Get the data about the next few jobs for use in the simulator
data = textscan(fid, '%f %f %f %f %f %f %f %f %f %f %d %f %f %f %f %f %f %f', numApps, 'delimiter', ' ', 'MultipleDelimsAsOne', 1);
fclose(fid);

% Want:
% number of nodes
% arrival time
% run time
valid = 0;
i = 1;
while valid < numApps
    arrivalTime = data{2}(i);
    runTime = data{4}(i);
    nodes = data{5}(i);
    %Sanity check
    if arrivalTime > 0 && runTime > 0 && nodes > 0
        trace(i).arrivalTime = data{2}(i);
        trace(i).runTime = data{4}(i);
        trace(i).nodes = min(data{5}(i), maxJobSize);   % Limit the nodes per job to a fraction of the total number of nodes (to ensure at least a minimum number of jobs can run at the same time)
        valid = valid + 1;
    end
    i = i + 1;
end

end

function app = getTraceApps(numApps, seed, sweep, ckptData)
% Generates a random set of applications for a scheduling run.  The set of
% applications generated can be controlled by setting the seed value at the
% top.

% Use the seed to produce a deterministic set of applications
s = RandStream('twister', 'Seed', seed);
RandStream.setGlobalStream(s);

% Read applications from the trace
trace = readFromTrace(numApps);
traceStartTime = trace(1).arrivalTime;

for i = 1:numApps
    name = ['App ' num2str(i, '%04d')];
    
    % Randomly generated
    if sweep
        compPower = [80];
    else
        compPower = [uniform(80, 100)];
    end
    compTime = [uniform(.1, 2)]; % Per iteration
    if sweep
        ckptPower = 40;
    else
        ckptPower = uniform(35, 45);
    end
    ckptTime = uniform(30, 5*60);
    checkpointMethod = getCheckpointMethod;
    ckptPercent = getCkptPercent(ckptData); % Percent of time spent checkpointing - this will be overridden for most tests
    disp([name ': ' num2str(ckptPercent)]);
    scalingModel = PSScaling.CUSTOM_SCALING;
    scalingFactor = uniform(0.2, 1.1);
    checkpointAtEnd = getCheckpointAtEnd;
    ckptInterval = getCkptInterval(compTime, ckptTime, compPower, ckptPower, scalingModel, scalingFactor, ckptPercent, checkpointMethod);
    
    % From trace
    workToDo = round(trace(i).runTime * (1 - ckptPercent) / compTime);
    nodes = trace(i).nodes;
    startTime = trace(i).arrivalTime - traceStartTime;
    
    app(i) = initialize(name, compPower, compTime, ckptPower, ckptInterval, ckptTime, workToDo, checkpointMethod, scalingModel, checkpointAtEnd, startTime, nodes, scalingFactor);
end

end

function ckptPercent = getCkptPercent(ckptData)

persistent MACHINE_CHECKPOINT_PERCENT;

if ckptData > NO_CKPT_DATA
    if isempty(MACHINE_CHECKPOINT_PERCENT)
        % This is a cell array.  Each cell holds the I/O percent data for one
        % machine.
        MACHINE_CHECKPOINT_PERCENT = cell(3, 1);
    end
    if isempty(MACHINE_CHECKPOINT_PERCENT{ckptData})
        % Load the I/O percent data for the machine we're using if it hasn't
        % been loaded yet.
        MACHINE_CHECKPOINT_PERCENT{ckptData} = loadCheckpointFile(ckptData);
    end

    % Pull a checkpoint percent randomly from the machine logs
    num = length(MACHINE_CHECKPOINT_PERCENT{ckptData});
    index = uniformInt(1, num);
    ckptPercent = MACHINE_CHECKPOINT_PERCENT{ckptData}(index);
else
    % Generate the checkpoint percent randomly
    ckptPercent = uniform(.05, .2);
end
end

function ckptPercent = loadCheckpointFile(ckptData)
if ckptData == EDISON_CKPT_DATA
    machine = 'edison';
elseif ckptData == MIRA_CKPT_DATA
    machine = 'mira';
elseif ckptData == INTREPID_CKPT_DATA
    machine = 'intrepid';
else
    error('Invalid machine in loadCheckpointFile (should be impossible)');
end

filename = [getHomeDir machine '_iopercentage.csv'];
data = csvread(filename, 1, 0); % The 1 skips the first row, which is the heading
filtered = data(data > 2.0 & data < 90.0);  % Filter out anything below 2% or above 90%
ckptPercent = filtered / 100;
end

function result = uniform(min, max)
    % Produces uniform random numbers on the given interval.
    result = (rand() * (max - min)) + min;
end

function result = uniformInt(min, max)
    % Produces uniform random integers on the given interval.
    result = randi([min, max]);
end

function result = expArrivalTime(meanArrivalTime)
    % This function is -ln(rand) / lambda, where lambda = 1/(meanArrivalTime)
    % (a poisson process).  Note that log() is a natural log in Matlab.
    result = -log(rand()) * meanArrivalTime;
end

function result = getTrueFalse
    result = (uniformInt(1, 2) == 1);
end

function checkpointMethod = getCheckpointMethod
    if uniform(0, 1) <= 0.80
        checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    else
        checkpointMethod = CHECKPOINT_BY_TIME;
    end
end

function interval = getCkptInterval(compTime, ckptTime, compPower, ckptPower, scalingModel, scalingFactor, ckptPercent, checkpointMethod)
global DEFAULT_POWER_LIMIT;

if checkpointMethod == CHECKPOINT_BY_ITERATIONS
    scalingObj = PSScaling;
    slowdownFactor = scalingObj.getComputingProgressFactor(scalingModel, compPower, ckptPower, scalingFactor, DEFAULT_POWER_LIMIT);

    % The checkpoint interval is set so that checkpoint time is a given percent of total
    % cycle time when running under the power limit.
    interval = round(ckptTime * (1 - ckptPercent) / (ckptPercent * compTime / slowdownFactor));
else
    % Select the checkpoint interval (in seconds) so that it matches
    % the percent of time we want to checkpoint.
    interval = ckptTime / ckptPercent - ckptTime;
end
end

function checkpointAtEnd = getCheckpointAtEnd
    checkpointAtEnd = getTrueFalse;
end

%------------------------------------------------------------------------%

function errorCheck(apps, NUM_APPS, configs, NUM_CONFIGS)

startAtZero = false;
for a = 1:NUM_APPS
    if apps(a).startTime == 0.0
        startAtZero = true;
    end
    for c = 1:NUM_CONFIGS
        powerLimit = configs(c).powerLimit * apps(a).nodes;
        if powerLimit < apps(a).checkpointPower
            % Eventually, I could support slowing down checkpointing if not enough
            % power is available, but checkpointing uses so close to the minimum
            % power that that is probably not useful.
            error('Power limit must be greater than checkpoint power.');
        end
    end
end

for c = 1:NUM_CONFIGS
    if configs(c).sharingMethod == BUDDY_SHARING
        % The method of keeping track of how much time is left in a
        % checkpoint has changed, and buddy has not been updated for
        % that change.  At this point, it is unlikely to be updated
        % since we're not using the buddy algorithm.
        error('Buddy sharing is not currently supported.');
    end
end
    
if ~startAtZero && NUM_APPS > 0
    error('At least one application should start at time 0.');
end

end

%------------------------------------------------------------------------%

function printApplications(app, NUM_APPS)

global MTTF;

for i = 1:NUM_APPS

    power = '[';
    time = '[';
    for phase = 1:length(app(i).phasePower)
        power = [power num2str(app(i).phasePower(phase)) ','];
        time = [time num2str(app(i).phaseTime(phase)) ','];
    end
    power(end) = ']';
    time(end) = ']';
    
    if app(i).checkpointMethod == CHECKPOINT_BY_TIME
        method = 'checkpoint by time';
    else
        method = 'checkpoint by iterations';
    end
    
    if app(i).providedCheckpointInterval == OPTIMAL_CHECKPOINT_INTERVAL
        interval = ['optimal (' optimalCheckpointInterval(MTTF, app.nodes, app.checkpointTime) ')'];
    else
        interval = num2str(app(i).providedCheckpointInterval);
    end
    
    if app(i).checkpointAtEnd
        lastCheckpoint = 'extra checkpoint at end';
    else
        lastCheckpoint = 'no extra checkpoint at end';
    end
    
    if app(i).scaling == PSScaling.PARADIS_SCALING
        scaling = 'ParaDiS';
    elseif app(i).scaling == PSScaling.LAMMPS_SCALING
        scaling = 'LAMMPS';
    elseif app(i).scaling == PSScaling.CACTUS_SCALING
        scaling = 'Cactus';
    elseif app(i).scaling == PSScaling.CUSTOM_SCALING
        scaling = ['custom (scale factor ' num2str(app(i).providedScalingFactor) ')'];
    elseif app(i).scaling == PSScaling.NO_SCALING
        scaling = 'No';
    else
        scaling = 'Error: not all scaling methods handled in printApplications';
    end
    
    disp([app(i).name ':' ...
         ' nodes: ' num2str(app(i).nodes) ...
         ', phase power: ' power ...
         ', phase time: ' time ...
         ', checkpoint power: ' num2str(app(i).checkpointPower) ...
         ', checkpoint cost: ' num2str(app(i).checkpointTime) ...
         ', checkpoint interval: ' interval ...
         ', iterations to do: ' num2str(app(i).workToDo) ...
         ', ' method ...
         ', ' scaling ' scaling' ...
         ', ' lastCheckpoint ...
        ]);
end

end

%------------------------------------------------------------------------%

function printConfigs(config, NUM_CONFIGS)

for i = 1:NUM_CONFIGS
    try
        sharing = nameFromSharingAlg(config(i).sharingMethod);
    catch
        sharing = 'ERROR: need to update printConfigs with new sharing methods';
    end
    
    disp([config(i).name ': ' sharing ', checkpoint mod: ' num2str(config(i).checkpoint_mod), ', start time offset: ' num2str(config(i).startTimeOffset), ', power limit (per node): ' num2str(config(i).powerLimit) ...
         ', scaling factor: ' num2str(config(i).scalingFactor) ', node counts: ' num2str(config(i).nodeCounts) ', num apps: ' num2str(config(i).numApps)]);
end

end

%------------------------------------------------------------------------%

function speedups = getSpeedups(NUM_APPS, NUM_CONFIGS, averages)

% Speedup is calculated for each app in each config.  By convention, the
% first configuration is the default.
speedups = zeros(NUM_CONFIGS, NUM_APPS);
for a = 1:NUM_APPS
    default = averages(1,a);
    speedups(:,a) = ((default./averages(:,a)) - 1) * 100;
end

end

%------------------------------------------------------------------------%

function printResults(app, NUM_APPS, config, NUM_CONFIGS, results, runs)

averages = results / runs;
speedups = getSpeedups(NUM_APPS, NUM_CONFIGS, averages);

for i = 1:NUM_APPS
    % By convention, the first configuration is the default.
    printAppResults(app(i).name, config, NUM_CONFIGS, averages(:,i), speedups(:,i));
end

% Print the combined results for all applications
totals = sum(averages, 2);
mins = min(speedups, [], 2);
maxs = max(speedups, [], 2);
meanPercent = mean(speedups, 2);
speedups = getSpeedups(1, NUM_CONFIGS, totals);
printAppResults('Totals for all apps', config, NUM_CONFIGS, totals, speedups, mins, maxs, meanPercent);

end
    
%------------------------------------------------------------------------%

function printAppResults(appName, config, NUM_CONFIGS, averages, speedups, mins, maxs, meanPercent)

printMinMax = nargin > 5;

disp('--------------------------------------------------------------------');
message = [appName ' (execution time, average speedup'];
if printMinMax
    message = [message ', min speedup, max speedup, avg of percent speedups'];
end
message = [message ')'];
disp(message);
for c = 1:NUM_CONFIGS
    message = ['(' num2str(averages(c)) ', ' num2str(speedups(c))];
    if printMinMax
        message = [message ', ' num2str(mins(c)) ', ' num2str(maxs(c)) ', ' num2str(meanPercent(c))];
    end
    message = [message ') - ' config(c).name];
    disp(message);
end

end

%------------------------------------------------------------------------%

function config = setConfig(name, sharingMethod, checkpoint_mod, startTimeOffset,  powerLimit, scalingFactor, nodeCounts, numApps)

if nargin < 6
    scalingFactor = -1;
end
if nargin < 7
    nodeCounts = [];
end
if nargin < 8
    numApps = -1;
end
if fllteq(checkpoint_mod, 0.0)
    checkpoint_mod = -1.0;
end

config.name = name;
config.sharingMethod = sharingMethod;
config.checkpoint_mod = checkpoint_mod;
config.startTimeOffset = startTimeOffset;
config.powerLimit = powerLimit;
config.scalingFactor = scalingFactor;
config.nodeCounts = nodeCounts;
config.numApps = numApps;

end

%------------------------------------------------------------------------%

function results = runSimulation(app, config, seed)

global INCREMENT;
global DO_PLOTS;
global FAILURE_ALLOWED;
global MACHINE_SIZE;

% Size to make the arrays at initialization.  It doesn't really matter, but
% setting this accurately could result in better performance.
if DO_PLOTS
    DEFAULT_ARRAY_SIZE = 200000;
else
    % This value only matters when plotting results.
    DEFAULT_ARRAY_SIZE = 1;
end

NUM_APPS = length(app);
if config.numApps > 0
    NUM_APPS = min(NUM_APPS, config.numApps);
end

disp(['Starting simulation with seed ' num2str(seed) ' and configuration ' config.name]);

% Ensure that we run with a given set of failures.  We must also specify
% the algorithm to ensure that we get the same answers when we run single
% threaded or parallel.
s = RandStream('twister', 'Seed', seed);
RandStream.setGlobalStream(s);

% Clear out persistent variables
getFavoredApp([], 0, 0, true);

% It is assumed that the start time offset will be zero for scheduling
% runs, but it doesn't have to be.
for i = 1:NUM_APPS
    startTime = app(i).providedStartTime + ((i - 1) * config.startTimeOffset);
    if length(config.nodeCounts) >= i
        nodes = config.nodeCounts(i);
    else
        nodes = -1;
    end
    app(i) = reinit(app(i), config.checkpoint_mod, startTime, DEFAULT_ARRAY_SIZE, config.scalingFactor, nodes);
end

IDLE_NODE_POWER = 25;
INC = INCREMENT;
    
currentSimTime = 0.0;
outIndex = 1;
nextToStart = 1;
freeNodes = MACHINE_SIZE;

staggerSharing = config.sharingMethod == STAGGER_SHARING || config.sharingMethod == STAGGER_STACK_SHARING;

if staggerSharing
    staggerAlg = StaggerAlgorithm(NUM_APPS, CHECKPOINT_BY_ITERATIONS);
end

if DO_PLOTS
    simTime = zeros(DEFAULT_ARRAY_SIZE, 1);
    powerBankUnused = zeros(DEFAULT_ARRAY_SIZE, 1);
    numAppsCheckpointing = zeros(DEFAULT_ARRAY_SIZE, 1);
    numNodesCheckpointing = zeros(DEFAULT_ARRAY_SIZE, 1);
    totalPowerPlot = zeros(DEFAULT_ARRAY_SIZE, 1);
    unusedPowerPlot = zeros(DEFAULT_ARRAY_SIZE, 1);
    totalNodes = zeros(DEFAULT_ARRAY_SIZE, 1);
end

finishedApps = 0;

while finishedApps < NUM_APPS
    
  % Initialize data that is reset on every pass
  [app.currentAvailablePower] = deal(0);
  [app.currentUsage] = deal(0);
  [app.borrowedPower] = deal(0);
  [app.powerNeeded] = deal(0);
  [app.extraPower] = deal(0);
  [app.shareablePower] = deal(0);
  
  % Start applications that should begin running at the start of this time
  % step.  Apps always start in number order, so we only need to check a
  % few each time.
  start = nextToStart;
  for appIndex = start:NUM_APPS
      if ~app(appIndex).started && fllteq(app(appIndex).startTime, currentSimTime) && ...
         app(appIndex).nodes <= freeNodes  % Don't start an app unless nodes are available.
          app(appIndex).started = true;
          app(appIndex).powerLimit = config.powerLimit * app(appIndex).nodes;
          app(appIndex).actualStartTime = currentSimTime;
          
          % An application can always go back to the start if it fails.
          app(appIndex) = startCheckpoint(app(appIndex), currentSimTime);
          app(appIndex) = finishCheckpoint(app(appIndex), currentSimTime);
          app(appIndex).numCheckpoints = 0;

          freeNodes = freeNodes - app(appIndex).nodes;
          disp(['Starting application ' app(appIndex).name ' at ' num2str(currentSimTime)]);
          disp(['Arrival time was ' num2str(app(appIndex).startTime) ', wait time was ' num2str(currentSimTime - app(appIndex).startTime)]);
          nextToStart = nextToStart + 1;
      else
          % If the above if statement isn't true, we don't have any more
          % apps to start this iteration.
          break;
      end
  end
  
  % Unpause applications that should no longer be paused at the start of
  % this time step.
  for appIndex = 1:NUM_APPS
      if app(appIndex).paused && fllteq(app(appIndex).endPauseTime, currentSimTime)
        disp(['Un-pausing app ' app(appIndex).name ' at ' num2str(currentSimTime)]);
        app(appIndex).paused = false;
      end
  end
  
  % Schedule applications.  This may involve pausing some applications.
  % This step only applies for the more sophisticated algorithms.
  if staggerSharing
      app = staggerAlg.doScheduling(app, currentSimTime);
  end
  
  % Calculate power needs and find out which applications have unused
  % power.
  for appIndex = 1:NUM_APPS
    app(appIndex) = calculatePowerNeeds(app(appIndex), IDLE_NODE_POWER);
  end

  % Share power between applications.
  idleNodePower = 0;
  app = sharePower(app, NUM_APPS, config.sharingMethod, currentSimTime, idleNodePower);
  
  % Calculate the overall power limit (not used in the calculations - just
  % for output).  Note that this is calculated here instead of further down
  % with the rest of the total power calculations because an app might
  % finish and have its power limit set to zero during the simulation run.
  totalPower = sum(cat(1, app.powerLimit));
  
  % Advance the simulation one timestep.
  currentSimTime = currentSimTime + INC;

  % Update each of the apps.
  for appIndex = 1:NUM_APPS
    if app(appIndex).started && ~app(appIndex).finished
        app(appIndex) = performSimulationStep(app(appIndex), outIndex);
        app(appIndex) = updatePhase(app(appIndex), currentSimTime);

        % See if there is any more work to do for this application.
        if app(appIndex).finished
            finishedApps = finishedApps + 1;
            freeNodes = freeNodes + app(appIndex).nodes;
        end
    elseif DO_PLOTS
        app(appIndex).powerAvailable(outIndex) = 0;
        app(appIndex).powerUsage(outIndex) = 0;
    end
    
    % Determine if it's time for this application to fail.  This must be
    % called even if the app is not running to ensure that the rand
    % functions get called in the same order every time so the same set of
    % failures happens every time.
    if FAILURE_ALLOWED && fllteq(app(appIndex).nextFailureTime, currentSimTime)
        app(appIndex) = failAndRestart(app(appIndex), currentSimTime);
    end
  end

  shareablePower = sum(cat(1, app.shareablePower));
  app = handleCheckpointRequests(app, NUM_APPS, config.sharingMethod, currentSimTime, shareablePower, totalPower);
  
  % Update output variables.
  if DO_PLOTS
      usedPower = sum(cat(1, app.currentUsage));
      unusedPower = totalPower - usedPower;
      borrowedPower = sum(cat(1, app.borrowedPower));
      unusedShareablePower = shareablePower - borrowedPower;

      simTime(outIndex) = currentSimTime;
      powerBankUnused(outIndex) = unusedShareablePower;
      totalPowerPlot(outIndex) = totalPower;
      unusedPowerPlot(outIndex) = unusedPower;
      
      numAppsCheckpointing(outIndex) = 0;
      numNodesCheckpointing(outIndex) = 0;
      totalNodes(outIndex) = 0;
      for i = 1:NUM_APPS
          if app(i).checkpointing
              numAppsCheckpointing(outIndex) = numAppsCheckpointing(outIndex) + 1;
              numNodesCheckpointing(outIndex) = numNodesCheckpointing(outIndex) + app(i).nodes;
          end
          if app(i).started && ~app(i).finished
              totalNodes(outIndex) = totalNodes(outIndex) + app(i).nodes;
          end
      end
      
      outIndex = outIndex + 1;
  end

end

% Account for the fact that control sometimes skips checkpoints
if config.sharingMethod == CONTROL_SHARING || config.sharingMethod == CONTROL_STACK_SHARING
    for i = 1:NUM_APPS
        if app(i).checkpointMethod == CHECKPOINT_BY_ITERATIONS
            % If the app checkpoints by iterations
            expectedCheckpoints = floor(app(i).workToDo / app(i).checkpointInterval);
            if mod(app(i).workToDo, app(i).checkpointInterval) == 0
                expectedCheckpoints = expectedCheckpoints - 1;
            end
        else
            % If the app checkpoints by time
            computationTime = app(i).completionTime - app(i).totalCheckpointTime;
            expectedCheckpoints = floor(computationTime / app(i).checkpointInterval);
        end
        missedCheckpoints = expectedCheckpoints - app(i).numCheckpoints;
        if missedCheckpoints > 0
            disp([app(i).name ' missed ' num2str(missedCheckpoints) ' checkpoints (total time: ' num2str(missedCheckpoints * app(i).checkpointTime) '). These checkpoints will not be added back in.']);
        end
    end
end

% For just the APP4_SHARING algorithm, we count the time offset against the
% completion time.
if config.sharingMethod == APP4_SHARING
    for i = 1:NUM_APPS
        offset = (i - 1) * config.startTimeOffset;
        app(i).completionTime = app(i).completionTime + offset;
    end
end

% Output the results
results = zeros(length(app), 1);

disp('--------------------------------------------------------------------');

for appIndex = 1:NUM_APPS
    if DO_PLOTS
      lastValidIndex = outIndex - 1;

      figure;
      plot(simTime(1:lastValidIndex), app(appIndex).powerAvailable(1:lastValidIndex), '-bx');
      hold on;
      plot(simTime(1:lastValidIndex), app(appIndex).powerUsage(1:lastValidIndex), '--ro');
      hold off;
      title([app(appIndex).name ': blue = available power, red = power used']);
    end

    disp([app(appIndex).name ' running time: ' num2str(app(appIndex).completionTime) ' (configuration ' config.name ')']);

    results(appIndex) = app(appIndex).completionTime;
    
    disp([app(appIndex).name ': number of checkpoints : ' num2str(app(appIndex).numCheckpoints) ...
          ', total checkpoint time: ' num2str(app(appIndex).totalCheckpointTime) ...
          ', % of time checkpointing: ' num2str(app(appIndex).totalCheckpointTime * 100 / app(appIndex).completionTime) '%']);
end

if DO_PLOTS
    figure;
    hold on;
    plot(simTime(1:lastValidIndex), numNodesCheckpointing(1:lastValidIndex), '-b');
    plot(simTime(1:lastValidIndex), totalNodes(1:lastValidIndex), '-r');
    hold off;
    title('Number of nodes checkpointing simultaneously');
    
    figure;
    hold on;
    plot(simTime(1:lastValidIndex), unusedPowerPlot(1:lastValidIndex), '-b');
    plot(simTime(1:lastValidIndex), totalPowerPlot(1:lastValidIndex), '-r');
    title('Unused (blue) vs total (red) power');
end

disp('--------------------------------------------------------------------');

if staggerSharing
    staggerAlg.shutdown;
end

end

%------------------------------------------------------------------------%

function app = initialize(name, phasePower, phaseTime, checkpointPower, checkpointInterval, checkpointTime, workToDo, checkpointMethod, scaling, checkpointAtEnd, startTime, nodes, scalingFactor)

if nargin < 11
    startTime = 0.0;
end
if nargin < 12
    nodes = 1;
end
if nargin < 13
    scalingFactor = 1;
end

% Error checking
if length(phasePower) ~= length(phaseTime)
    error(['Number of phase types does not match number of phase times.']);
end

% Specific to the app (also, these do not need to be reset at the start of
% each simulation)
app.name = name;
app.nodes = nodes;
app.singleNodePhasePower = phasePower;
app.phasePower = phasePower * app.nodes;
app.phaseTime = phaseTime;
app.singleNodeCheckpointPower = checkpointPower;
app.checkpointPower = checkpointPower * app.nodes;
app.checkpointTime = checkpointTime;
app.providedCheckpointInterval = checkpointInterval;
app.checkpointMethod = checkpointMethod;
app.scaling = scaling;
app.checkpointAtEnd = checkpointAtEnd;
app.workToDo = workToDo;
app.providedStartTime = startTime;
app.providedScalingFactor = scalingFactor;
app.providedNodes = nodes;

% The optimal checkpoint interval is only supported when checkpointing by
% time
if (app.providedCheckpointInterval == OPTIMAL_CHECKPOINT_INTERVAL && app.checkpointMethod ~= CHECKPOINT_BY_TIME)
    error(['Must checkpoint by time when using optimal checkpoint interval.']);
end

% Data that must be reset every time an application is run
app = reinit(app, 0, startTime, 1, -1, -1);

end

%------------------------------------------------------------------------%

function app = reinit(app, checkpoint_mod, startTime, DEFAULT_ARRAY_SIZE, scalingFactor, numNodes)

global MTTF;
global DO_PLOTS;
global FAILURE_ALLOWED;

if scalingFactor > 0
    app.scalingFactor = scalingFactor;
else
    app.scalingFactor = app.providedScalingFactor;
end

% Based on the paper describing Daly's equation, the interval produced is
% actually the amount of execution time between checkpoints, so the time
% spent checkpointing does not count against this interval.
if app.providedCheckpointInterval == OPTIMAL_CHECKPOINT_INTERVAL
    app.checkpointInterval = optimalCheckpointInterval(MTTF, app.nodes, app.checkpointTime);
elseif checkpoint_mod > 0
    app.checkpointInterval = getCkptInterval(app.phaseTime, app.checkpointTime, app.singleNodePhasePower, app.singleNodeCheckpointPower, app.scaling, app.scalingFactor, checkpoint_mod, app.checkpointMethod);
else
    app.checkpointInterval = app.providedCheckpointInterval;
end

if app.checkpointInterval <= 0
    %error(['Checkpoint interval is too low.']);
    app.checkpointInterval = 1;
end

if numNodes > 0
    app.nodes = numNodes;
else
    app.nodes = app.providedNodes;
end

% Initialize things that are the same for all apps and that need to be
% reset at the start of each simulation.
app.phase = 1;
app.checkpointing = false;
app.totalCheckpointTime = 0;
app.currentPhaseTime = 0;
app.workRemaining = app.workToDo;
app.currentAvailablePower = 0;
app.currentUsage = 0;
app.borrowedPower = 0;
app.powerNeeded = 0;
app.extraPower = 0;
app.finished = false;
app.completionTime = 0;
if DO_PLOTS
    app.powerUsage = zeros(DEFAULT_ARRAY_SIZE, 1);
    app.powerAvailable = zeros(DEFAULT_ARRAY_SIZE, 1);
end
app.restarting = false;
if FAILURE_ALLOWED
    app.lastRestartTime = 0;
    app.nextFailureTime = getTimeToNextFailure(app.nodes);
end
app.numCheckpoints = 0;
app.started = false;
app.startTime = startTime;
app.actualStartTime = 0;
app.iteration = 0;
app.powerLimit = 0; % Apps are given power limits when they start.
app.extraWorkDone = 0;
app.shareablePower = 0;
app.paused = false;
app.endPauseTime = 0;
app.requestCheckpoint = false;
app.checkpointRequestTime = 0;
app.delayedCheckpointTime = 0;
app.checkpointRequestIteration = 0;
app.delayedCheckpointIterations = 0;
app.pendingCheckpointRequest = false;
app.phasePower = app.singleNodePhasePower * app.nodes;
app.checkpointPower = app.singleNodeCheckpointPower * app.nodes;

% The first "checkpoint" is at the start of the application - we can always
% roll back to the start.  This also sets checkpoint related variables.
app = startCheckpoint(app, app.startTime);
app = finishCheckpoint(app, app.startTime);

app.numCheckpoints = 0;

end

%------------------------------------------------------------------------%

function interval = optimalCheckpointInterval(expectedUptime, nodes, checkpointCost)

% MTTF (expected uptime) scales inversely with respect to nodes (twice as
% many nodes results in half the expected uptime).
expectedUptime = expectedUptime / nodes;

% This function is only designed to handle certain ranges of checkpoint
% costs.
if checkpointCost >= (2 * expectedUptime)
    error(['Checkpoint cost is too high relative to expected uptime']);
end
if checkpointCost < (0.25 * expectedUptime)
    error(['Checkpoint cost is too small relative to expected uptime']);
end

% Optimal checkpoint frequency using Daly's equation
term1 = checkpointCost / (2 * expectedUptime);
term2 = sqrt(term1);
term3 = 2 * checkpointCost * expectedUptime;
term3 = sqrt(term3);
interval = (1 + (term2 / 3) + (term1 / 9)) * term3 - checkpointCost;

end

%------------------------------------------------------------------------%

function app = calculatePowerNeeds(app, IDLE_NODE_POWER)

global SHARE_ONLY_DURING_CHECKPOINTS;

app.currentAvailablePower = app.powerLimit;

if ~app.started || app.finished
    % If the application isn't running, it doesn't share power with other
    % applications.
    app.powerNeeded = app.powerLimit;
elseif app.paused
    % Paused applications use some power but not much.
    app.powerNeeded = IDLE_NODE_POWER * app.nodes;
    % Allow paused applications to make progress, but at a low power level
    %app.powerNeeded = app.checkpointPower;
elseif app.checkpointing
    app.powerNeeded = app.checkpointPower;
elseif app.restarting
    % Restarting takes a little more power than checkpointing.
    app.powerNeeded = app.checkpointPower + (10 * app.nodes);
else
    app.powerNeeded = app.phasePower(app.phase);
end

% If extraPower is negative, this app needs more power than it has available.
app.extraPower = app.currentAvailablePower - app.powerNeeded;
if (app.checkpointing || ~SHARE_ONLY_DURING_CHECKPOINTS) && app.extraPower > 0
    app.shareablePower = app.extraPower;
else
    app.shareablePower = 0;
end

end

%------------------------------------------------------------------------%

function apps = sharePower(apps, NUM_APPS, sharingMethod, currentSimTime, idleNodePower)

% Calculate how much unused power is available
powerBank = idleNodePower;
for appIndex = 1:NUM_APPS
    if apps(appIndex).shareablePower > 0
        powerBank = powerBank + apps(appIndex).shareablePower;
    end
end

if fllteq(powerBank, 0.0)
    return;
end

if sharingMethod == NO_SHARING
    % Do nothing
elseif sharingMethod == FIRST_COME_SHARING
    % Allocate extra power to apps that need it, giving priority in number
    % order.
    for appIndex = 1:NUM_APPS
        [apps(appIndex), powerBank] = allocatePower(apps(appIndex), powerBank);
    end
elseif sharingMethod == APP4_SHARING
    % For the paradis4 run, allocate power to the main app first, then give
    % priority to apps that were delayed the longest.
    [apps(1), powerBank] = allocatePower(apps(1), powerBank);
    [apps(4), powerBank] = allocatePower(apps(4), powerBank);
    [apps(3), powerBank] = allocatePower(apps(3), powerBank);
    [apps(2), powerBank] = allocatePower(apps(2), powerBank); %#ok<NASGU>
elseif sharingMethod == APP2_ONLY_SHARING
    % This version is for a run I did on accident - two applications ran
    % but only one of them benefitted from shared power.
    [apps(2), powerBank] = allocatePower(apps(2), powerBank); %#ok<NASGU>
elseif sharingMethod == STACK_SHARING
    % This is the same as the stack algorithm in the controller - it
    % allocates all power to applications in priority order, but the
    % priority changes regularly to make the sharing as fair as possible.
    favoredApp = getFavoredApp(apps, NUM_APPS, currentSimTime, false);
    apps = stackSharing(apps, NUM_APPS, powerBank, favoredApp);
elseif sharingMethod == SPREAD_SHARING || sharingMethod == STAGGER_SHARING || sharingMethod == CONTROL_SHARING
    % This method spreads out unused power across as many applications as
    % possible.
    % Note that several of the more advanced algorithms use spread to share
    % power - the algorithms themselves are only concerned with lining up
    % checkpoints.
    numReceivers = countReceivers(apps, NUM_APPS, true);
    if numReceivers > 0
        powerPerNode = powerBank / numReceivers;
        for i = 1:NUM_APPS
            if apps(i).extraPower < 0
                appExtraPower = powerPerNode * apps(i).nodes;
                [apps(i), leftovers] = allocatePower(apps(i), appExtraPower);
                powerBank = powerBank - appExtraPower + leftovers;
            end
        end
    end
elseif sharingMethod == CPU_STACK_SHARING || sharingMethod == STAGGER_STACK_SHARING || sharingMethod == CONTROL_STACK_SHARING
    % This is the same as the stack method except that it prioritizes
    % applications by their relative CPU intensity - CPU bound applications
    % get the power first.
    receivers = countReceivers(apps, NUM_APPS, false);
    
    % The two columns in this matrix will be used for (1) CPU intensity and
    % (2) index in the apps list.
    cpuIntensities = zeros(receivers, 2);
    receiver = 1;
    for i = 1:NUM_APPS
        if apps(i).extraPower < 0.0
            % Calculate what the progress factor would be if the
            % application just got to use its power limit (and no extra).
            % A smaller progress factor means a higher CPU intensity; in
            % other words, the application would run a lot faster with
            % extra power.
            cpuIntensities(receiver,1) = getProgressFactor(apps(i), apps(i).powerLimit);
            cpuIntensities(receiver,2) = i;
            receiver = receiver + 1;
        end
    end
    
    % Sort the applications by CPU intensity (smaller progress factor = 
    % greater intensity).  Note that among apps with the same CPU
    % intensity, the app that comes first in the list will always be chosen.
    cpuIntensities = sortrows(cpuIntensities);
    
    % Allocate power in order of highest CPU intensity first.  The 1
    % indicates that the first app in the list is favored.
    apps = stackSharing(apps, NUM_APPS, powerBank, 1, cpuIntensities(:,2));
end

end

%------------------------------------------------------------------------%

function currentFavoredApp = getFavoredApp(apps, NUM_APPS, currentSimTime, initialize)
    persistent favoredApp;
    persistent favoredAppTime;

    if initialize
        favoredApp = 1;
        favoredAppTime = 0;
        return;
    end
    
    % Update the favored app every 30 seconds.  Make sure we pick an app
    % that's actually running right now.
    if fllteq(30.0, (currentSimTime - favoredAppTime))
        favoredAppTime = currentSimTime;
        while true
            favoredApp = favoredApp + 1;
            if favoredApp > NUM_APPS
                favoredApp = 1;
            end
            if apps(favoredApp).started && ~apps(favoredApp).finished
                break;
            end
        end
    end
    currentFavoredApp = favoredApp;
end

%------------------------------------------------------------------------%

function receivers = countReceivers(apps, NUM_APPS, countNodes)
    receivers = 0;
    for i = 1:NUM_APPS
        if apps(i).extraPower < 0
            if countNodes
                receivers = receivers + apps(i).nodes;
            else
                receivers = receivers + 1;
            end
        end
    end
end

%------------------------------------------------------------------------%

function apps = stackSharing(apps, NUM_APPS, powerBank, favoredApp, ordering)

if nargin < 5
    ordering = 1:NUM_APPS;
elseif favoredApp ~= 1
    % If we are using ordering, the favored app must be 1.  Otherwise, the
    % code below will not work.
    error(['When using ordering, favored app must be 1']);
end

% Allocate power to the favored app first, then the others in order.
num = length(ordering);
for i = 1:num
    appIndex = favoredApp + ordering(i) - 1;
    if appIndex > NUM_APPS
        appIndex = appIndex - num;
    end
    [apps(appIndex), powerBank] = allocatePower(apps(appIndex), powerBank);
    if fllteq(powerBank, 0.0)
        break;
    end
end
end

%------------------------------------------------------------------------%

function [app, powerBank] = allocatePower(app, powerBank)
    if ~app.checkpointing && ~app.restarting && app.extraPower < 0 % extraPower < 0 indicates that the application needs more power
        sharedPower = min(-app.extraPower, powerBank);
        if sharedPower < 0
          sharedPower = 0;
        end
        app.currentAvailablePower = app.currentAvailablePower + sharedPower;
        app.borrowedPower = app.borrowedPower + sharedPower;
        powerBank = powerBank - sharedPower;
        
        % Some algorithms call allocatePower more than once per app, so we
        % need to update extra power to reflect that we've received some.
        app.extraPower = app.extraPower + sharedPower;
    end
end

%------------------------------------------------------------------------%

function app = performSimulationStep(app, outIndex)

global INCREMENT;
global DO_PLOTS;

app.currentUsage = min(app.currentAvailablePower, app.powerNeeded);
progressFactor = getProgressFactor(app, app.currentUsage);

% When calculating work done, include any extra work done in the previous
% phase.
workDone = (INCREMENT + app.extraWorkDone) * progressFactor;
app.extraWorkDone = 0.0;

% The current phase time is only updated by the amount of work done, not the
% amount of time that has passed.
app.currentPhaseTime = app.currentPhaseTime + workDone;

% If this was the last increment in this phase, some extra work might have
% been done between the end of the phase and the end of the increment.
% This saves off that extra work so it can be applied to the next phase.
% Note that this sometimes takes work done before a checkpoint and applies
% it after the checkpoint, but that is effectively like shifting the
% checkpoint back in time by a small amount, which shouldn't affect the
% results.
if app.currentPhaseTime > app.phaseTime(app.phase)
    % Rescale the work done by the progress factor to take out the effect
    % of the power limit.  It will be scaled back down appropriately when
    % it is applied to the next phase.
    app.extraWorkDone = (app.currentPhaseTime - app.phaseTime(app.phase)) / progressFactor;
    if app.extraWorkDone > INCREMENT
        % If this happens, it's a coding error
        error('Extra work done is greater than increment');
    end
end

% Storing results takes the majority of simulation time (probably due to
% reallocating arrays).  Thus, we allow the option of not updating them to
% increase speed when needed.
if DO_PLOTS
    app.powerUsage(outIndex) = app.currentUsage;
    app.powerAvailable(outIndex) = app.currentAvailablePower;
end

end

%------------------------------------------------------------------------%

function [progressFactor] = getProgressFactor(app, usage)

% Note that the scaling object doesn't have persistent data, so there is no
% need to clear it on every run.
persistent scalingObj;
if isempty(scalingObj)
    scalingObj = PSScaling;
end

if app.checkpointing || app.restarting || ~app.started || app.paused
    % The applicaton does not make progress during checkpoints or restarts.
    progressFactor = 0.0;
elseif app.powerNeeded <= usage %&& ~app.paused
    % If the application has all the power it needs, it does not slow down.
    progressFactor = 1.0;
else    %if usage < app.powerNeeded
    progressFactor = scalingObj.getComputingProgressFactor(app.scaling, app.phasePower(app.phase), app.checkpointPower, app.scalingFactor, usage);
end

end

%------------------------------------------------------------------------%

function app = updatePhase(app, currentSimTime)

if app.restarting
    % Restarting takes 5% longer than checkpointing
    if fllteq(app.checkpointTime * 1.05, currentSimTime - app.lastRestartTime)
        app = finishRestart(app, currentSimTime);
    end
elseif app.checkpointing
    if fllteq(app.checkpointTime, currentSimTime - app.startCheckpointTime)
        % app.phase and app.currentPhaseTime were already set previously
        % when app.checkpointing was set to true.
        app = finishCheckpoint(app, currentSimTime);
        % If this is the checkpoint at the end of the run, mark the app as
        % finished.
        if (app.checkpointAtEnd && app.workRemaining <= 0)
            app = setFinishTime(app, currentSimTime);
        end
    end
elseif fllteq(app.phaseTime(app.phase), app.currentPhaseTime)
  app.phase = app.phase + 1;
  if app.phase > length(app.phasePower)
    app.phase = 1;
    app.iteration = app.iteration + 1;
    app.workRemaining = app.workRemaining - 1;
    
    if ~app.finished && app.workRemaining <= 0
        if (app.checkpointAtEnd)
            % When there's nothing left to do but checkpoint, go ahead and
            % checkpoint anyway (don't wait for your turn).
            app = startCheckpoint(app, currentSimTime);
        else
            app = setFinishTime(app, currentSimTime);
        end
    end

    % We only checkpoint at the end of all the phases (one timestep
    % consists of one execution of all the phases).
    % Note that we have to check if the app is checkpointing here because
    % it might have already started the final checkpoint above.
    if app.started && ~app.restarting && ~app.finished && ~app.paused && ~app.checkpointing && ...
       timeToCheckpoint(app, currentSimTime)
   
        app.requestCheckpoint = true;
        if ~app.pendingCheckpointRequest
            app.checkpointRequestTime = currentSimTime;
            app.checkpointRequestIteration = app.iteration;
            app.pendingCheckpointRequest = true;
        end
    end
  end
  
  % Reset the phase time for the next phase
  app.currentPhaseTime = 0;
end

end

%------------------------------------------------------------------------%

function app = setFinishTime(app, currentSimTime)
    % If the app did extra work due to time discretization, remove that
    % here.  Also, don't count the time before the app was scheduled to
    % run.  Time that the application was delayed due to an algorithm is
    % counted in the completion time.
    disp(['Application ' app.name ' finished at time ' num2str(currentSimTime)]);
    app.completionTime = currentSimTime - app.extraWorkDone - app.actualStartTime;
    app.powerLimit = 0;
    app.finished = true;
end

%------------------------------------------------------------------------%

function do_checkpoint = timeToCheckpoint(app, currentSimTime)

if app.checkpointMethod == CHECKPOINT_BY_TIME
    % Checkpoint based on time
    interval = app.checkpointInterval - app.delayedCheckpointTime;
    do_checkpoint = fllteq(interval, (currentSimTime - app.stopCheckpointTime));
else
    % Checkpoint based on iterations (round to handle round-off problems
    % with floats).
    interval = app.checkpointInterval - app.delayedCheckpointIterations;
    do_checkpoint = round(app.iteration - app.lastCheckpointIteration) >= round(interval);
end

end

%------------------------------------------------------------------------%

function apps = handleCheckpointRequests(apps, NUM_APPS, sharingMethod, currentSimTime, startingShareablePower, totalPower)

if sharingMethod == CONTROL_SHARING || sharingMethod == CONTROL_STACK_SHARING
	shareablePower = startingShareablePower;
    powerNeeded = sum(cat(1, apps.powerNeeded));
    for i = 1:NUM_APPS
        if apps(i).requestCheckpoint
            % TODO: this doesn't handle apps that have extra power before
            % they start checkpointing.
            % If it's been more than half a checkpoint interval since this app
            % was supposed to checkpoint, let it checkpoint even if power
            % is wasted.
            if apps(i).checkpointMethod == CHECKPOINT_BY_TIME
                tooLong = fllteq((apps(i).checkpointInterval / 2), (currentSimTime - apps(i).checkpointRequestTime));
            else
                tooLong = round(apps(i).iteration - apps(i).checkpointRequestIteration) >= round(apps(i).checkpointInterval / 2);
            end
            % If starting this checkpoint won't cause power to be wasted,
            % do it.
            if (fllteq(totalPower, powerNeeded - apps(i).powerNeeded + apps(i).checkpointPower) || ...
                (shareablePower <= 0.0) || ...  % This line ensures that if no one is checkpointing, someone gets to
                tooLong)    % This line ensures that checkpoints aren't delayed to long, which prevents them from being lost (except possibly the last one)
            
                apps(i) = startCheckpoint(apps(i), currentSimTime);
                apps(i).delayedCheckpointTime = currentSimTime - apps(i).checkpointRequestTime;
                apps(i).delayedCheckpointIterations = apps(i).iteration - apps(i).checkpointRequestIteration;
                shareablePower = shareablePower + apps(i).powerLimit - apps(i).checkpointPower;
                powerNeeded = powerNeeded - apps(i).powerNeeded + apps(i).checkpointPower;
            else
                disp(['Denied checkpoint request for app ' apps(i).name ' at time ' num2str(currentSimTime)]);
            end
            % Even if the app didn't get to checkpoint, turn off the
            % checkpoint request (it will request one again when it next
            % gets the chance).
            apps(i).requestCheckpoint = false;
        end
    end
else
    for i = 1:NUM_APPS
        if apps(i).requestCheckpoint
            apps(i) = startCheckpoint(apps(i), currentSimTime);
            apps(i).requestCheckpoint = false;
        end
    end
end

end

%------------------------------------------------------------------------%

function app = startCheckpoint(app, currentSimTime)

disp(['Start checkpointing in ' app.name ' at ' num2str(currentSimTime)]);

% This simulates starting the checkpoint process.  The checkpoint is not
% actually valid until finishCheckpoint is called.
app.checkpointing = true;
app.startCheckpointTime = currentSimTime;
app.numCheckpoints = app.numCheckpoints + 1;
app.lastCheckpointIteration = app.iteration;
app.pendingCheckpointRequest = false;

end

%------------------------------------------------------------------------%

function app = finishCheckpoint(app, currentSimTime)

disp(['Stop checkpointing in ' app.name ' at ' num2str(currentSimTime)]);

% This takes a "checkpoint" by recording everything we need to know in
% order to roll the computation back to an earlier time.  Since we only
% checkpoint at the start of a time step (which is the start of all the
% phases), we mostly just need to know how much work the application has
% left to do.
app.checkpointWorkRemaining = app.workRemaining;
app.checkpointIteration = app.iteration;
app.checkpointing = false;
app.stopCheckpointTime = currentSimTime;
app.totalCheckpointTime = app.totalCheckpointTime + (currentSimTime - app.startCheckpointTime);

end

%------------------------------------------------------------------------%

function app = startRestart(app, currentSimTime)

disp(['Start restart in ' app.name ' at ' num2str(currentSimTime)]);

% This function simulates starting the restart process after a failure.
app.restarting = true;
app.lastRestartTime = currentSimTime;
app.workRemaining = app.checkpointWorkRemaining;
app.iteration = app.checkpointIteration;
app.checkpointing = false;
app.pendingCheckpointRequest = false;
app.requestCheckpoint = false;

% Since we only allow checkpointing at the end of a timestep (i.e. just
% before phase 1), we don't have to remember which phase was executing).
app.phase = 1;
app.currentPhaseTime = 0;
app.extraWorkDone = 0;

% If we happen to start a restart in the middle of the pause, the restart
% doesn't actually happen until the end of the pause.
if app.paused
    app.lastRestartTime = app.lastRestartTime + (app.endPauseTime - currentSimTime);
end

end

%------------------------------------------------------------------------%

function app = finishRestart(app, currentSimTime)

disp(['Finish restart in ' app.name ' at ' num2str(currentSimTime)]);

% This function simulates finishing the restart process after a failure.
app.restarting = false;

% Since we've restarted after a checkpoint, there is no need to take
% another checkpoint until the full checkpoint interval has elapsed.
app.stopCheckpointTime = currentSimTime;
app.lastCheckpointIteration = app.iteration;

end

%------------------------------------------------------------------------%

function app = failAndRestart(app, currentSimTime)

app.nextFailureTime = currentSimTime + getTimeToNextFailure(app.nodes);
if app.started && ~app.finished
    app = startRestart(app, currentSimTime);
end

end

%------------------------------------------------------------------------%

function nextFailureTime = getTimeToNextFailure(nodes)

global MTTF;

% Expected uptime (MTTF) scales inversely with respect to nodes (twice the
% number of nodes results in half the expected uptime.
expectedUptime = MTTF/nodes;
nextFailureTime = expArrivalTime(expectedUptime);

end

%------------------------------------------------------------------------%

% Constants that the user does not need to modify are defined as functions
% here.

% Flag indicating that we should use the optimal checkpoint interval
function val = OPTIMAL_CHECKPOINT_INTERVAL
    val = -1;
end

% Defines the two different ways we can determine when to checkpoint: time
% or iterations.  The value of these constants is not important; they just
% need to be different.
function val = CHECKPOINT_BY_TIME
    val = 1;
end
function val = CHECKPOINT_BY_ITERATIONS
    val = 2;
end

% Defines different types of power sharing algorithms.  The values of these
% constants is not important; they just need to be different.
function val = NO_SHARING
    val = 1;
end
function val = FIRST_COME_SHARING
    val = 2;
end
function val = APP4_SHARING
    val = 3;
end
function val = APP2_ONLY_SHARING
    val = 4;
end
function val = STACK_SHARING
    val = 5;
end
function val = SPREAD_SHARING
    val = 6;
end
function val = CPU_STACK_SHARING
    val = 7;
end
function val = STAGGER_SHARING
    val = 8;
end
function val = BUDDY_SHARING
    val = 9;
end
function val = CONTROL_SHARING
    val = 10;
end
function val = STAGGER_STACK_SHARING
    val =  11;
end
function val = CONTROL_STACK_SHARING
    val = 12;
end

% "Enum" to differentiate between runs where the average improvement is
% needed and runs where the max improvement is needed.
function val = AVERAGE_IMPROVEMENT
    val = 1;
end
function val = MAX_IMPROVEMENT
    val = 2;
end

% Translates the above "enum" into text
function name = nameFromSharingAlg(sharing)
if sharing == NO_SHARING
    name = 'no sharing';
elseif sharing == FIRST_COME_SHARING
    name = 'first come sharing';
elseif sharing == APP4_SHARING
    name = 'app 4 sharing';
elseif sharing == APP2_ONLY_SHARING
    name = 'app 2 only sharing';
elseif sharing == STACK_SHARING
    name = 'stack sharing';
elseif sharing == SPREAD_SHARING
    name = 'spread sharing';
elseif sharing == CPU_STACK_SHARING
    name = 'priority sharing';
elseif sharing == STAGGER_SHARING
    name = 'stagger sharing';
elseif sharing == STAGGER_STACK_SHARING
    name = 'stagger w/ priority sharing';
elseif sharing == BUDDY_SHARING
    name = 'buddy sharing';
elseif sharing == CONTROL_SHARING
    name = 'control sharing';
elseif sharing == CONTROL_STACK_SHARING
    name = 'control w/ priority sharing';
else
    error('Unknown sharing method');
end
end

% Enum to decide which set of checkpoint data to use
function val = NO_CKPT_DATA
    val = 0;
end
function val = EDISON_CKPT_DATA
    val = 1;
end
function val = MIRA_CKPT_DATA
    val = 2;
end
function val = INTREPID_CKPT_DATA
    val = 3;
end

%------------------------------------------------------------------------%
%------------------------------------------------------------------------%
%------------------------------------------------------------------------%

% Everything below this line is test functions.  They are not called during
% normal runs.  The tests are in this file because matlab will only let you
% call the top level function from outside the file.

% Determines whether to catch exceptions when running tests (useful for
% debugging).
function var = CATCH_TEST_ERRORS
    var = false;
end

%------------------------------------------------------------------------%
% Helper functions

% Any test that uses a random number is responsible for initializing the
% random number stream itself.
function initRandForTest(seed)
    s = RandStream('twister', 'Seed', seed);
    RandStream.setGlobalStream(s);
end

function same = testfleq(f1, f2)
    same = fllteq(f1, f2) && fllteq(f2, f1);
end

%------------------------------------------------------------------------%

function passed = nextFailureTimeTest(nodes, seed)
    global MTTF;
    initRandForTest(seed);
    expected = -log(rand()) * (MTTF / nodes);
    initRandForTest(seed);
    actual = getTimeToNextFailure(nodes);
    passed = testfleq(expected, actual);
end

function passed = nextFailureTimeTest1Node
    passed = nextFailureTimeTest(1, 123);
end

function passed = nextFailureTimeTest64Node
    passed = nextFailureTimeTest(64, 5150215);
end

% startRestart is tested thoroughly as part of the failAndRestart* tests,
% so it is not tested independently.
function passed = failAndRestartTest
    initRandForTest(68485);
    workRemaining = 15;
    iteration = 603;
    currentTime = 1354;

    app.name = 'Test App';
    app.nodes = 4;
    app.started = true;
    app.finished = false;
    app.paused = false;
    app.checkpointWorkRemaining = workRemaining;
    app.checkpointIteration = iteration;
    app.extraWorkDone = 150;
    
    updated = failAndRestart(app, currentTime);
    passed = true;
    passed = passed && updated.nextFailureTime > currentTime + 0.000001;
    passed = passed && updated.restarting;
    passed = passed && testfleq(updated.lastRestartTime, currentTime);
    passed = passed && updated.workRemaining == workRemaining;
    passed = passed && updated.iteration == iteration;
    passed = passed && ~updated.checkpointing;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.extraWorkDone == 0;
end

% This is really a copy of the above test since failAndRestart always
% resets the checkpoint, but conceptually it's a different situation so
% I wrote a different test
function passed = failAndRestartTestCheckpointing
    initRandForTest(68);
    workRemaining = 253;
    iteration = 110;
    currentTime = 74;

    app.name = 'Test App';
    app.nodes = 4;
    app.started = true;
    app.finished = false;
    app.paused = false;
    app.checkpointing = true;
    app.checkpointWorkRemaining = workRemaining;
    app.checkpointIteration = iteration;
    app.extraWorkDone = 150;
    
    updated = failAndRestart(app, currentTime);
    passed = true;
    passed = passed && updated.nextFailureTime > currentTime + 0.000001;
    passed = passed && updated.restarting;
    passed = passed && testfleq(updated.lastRestartTime, currentTime);
    passed = passed && updated.workRemaining == workRemaining;
    passed = passed && updated.iteration == iteration;
    passed = passed && ~updated.checkpointing;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.extraWorkDone == 0;
end

function passed = failAndRestartTestPaused
    initRandForTest(3);
    workRemaining = 15;
    iteration = 603;
    currentTime = 1354;
    endPauseTime = 1444;

    app.name = 'Test App';
    app.nodes = 4;
    app.started = true;
    app.finished = false;
    app.paused = true;
    app.checkpointWorkRemaining = workRemaining;
    app.checkpointIteration = iteration;
    app.extraWorkDone = 150;
    app.endPauseTime = endPauseTime;
    app.lastRestartTime = 1234;
    
    updated = failAndRestart(app, currentTime);
    passed = true;
    passed = passed && updated.nextFailureTime > currentTime + 0.000001;
    passed = passed && updated.restarting;
    passed = passed && testfleq(updated.lastRestartTime, endPauseTime);
    passed = passed && updated.workRemaining == workRemaining;
    passed = passed && updated.iteration == iteration;
    passed = passed && ~updated.checkpointing;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.extraWorkDone == 0;
end

function passed = failAndRestartTestNotStarted
    initRandForTest(864351);
    workRemaining = 15;
    iteration = 603;
    currentTime = 1354;

    app.name = 'Test App';
    app.nodes = 4;
    app.started = false;
    app.finished = false;
    app.paused = false;
    app.checkpointWorkRemaining = workRemaining;
    app.checkpointIteration = iteration;
    app.extraWorkDone = 150;
    
    updated = failAndRestart(app, currentTime);
    passed = true;
    passed = passed && updated.nextFailureTime > currentTime + 0.000001;
end

function passed = failAndRestartTestFinished
    initRandForTest(864351);
    workRemaining = 15;
    iteration = 603;
    currentTime = 1354;

    app.name = 'Test App';
    app.nodes = 4;
    app.started = true;
    app.finished = true;
    app.paused = false;
    app.checkpointWorkRemaining = workRemaining;
    app.checkpointIteration = iteration;
    app.extraWorkDone = 150;
    
    updated = failAndRestart(app, currentTime);
    passed = true;
    passed = passed && updated.nextFailureTime > currentTime + 0.000001;
end

function passed = finishRestartTest
    iteration = 53543;
    currentTime = 451;

    app.name = 'Test App';
    app.restarting = true;
    app.stopCheckpointTime = 0;
    app.lastCheckpointIteration = 0;
    app.iteration = iteration;
    
    updated = finishRestart(app, currentTime);
    passed = true;
    passed = passed && ~updated.restarting;
    passed = passed && testfleq(updated.stopCheckpointTime, currentTime);
    passed = passed && updated.lastCheckpointIteration == iteration;
end

function passed = finishCheckpointTest
    workRemaining = 12356;
    iteration = 776;
    currentSimTime = 9823469;
    startCheckpointTime = 9823400;
    totalCheckpointTime = 5000;

    app.name = 'Test App';
    app.checkpointWorkRemaining = 0;
    app.checkpointIteration = 0;
    app.checkpointing = true;
    app.stopCheckpointTime = 0;
    app.totalCheckpointTime = totalCheckpointTime;
    app.workRemaining = workRemaining;
    app.iteration = iteration;
    app.startCheckpointTime = startCheckpointTime;
    
    updated = finishCheckpoint(app, currentSimTime);
    passed = true;
    passed = passed && updated.checkpointWorkRemaining == workRemaining;
    passed = passed && updated.checkpointIteration == iteration;
    passed = passed && ~updated.checkpointing;
    passed = passed && updated.stopCheckpointTime == currentSimTime;
    passed = passed && updated.totalCheckpointTime == (totalCheckpointTime + currentSimTime - startCheckpointTime);
end

function passed = startCheckpointTest
    currentSimTime = 24737;
    numCheckpoints = 234;
    iteration = 7843;
    
    app.name = 'Test App';
    app.checkpointing = false;
    app.startCheckpointTime = 0;
    app.numCheckpoints = numCheckpoints;
    app.lastCheckpointIteration = 0;
    app.pendingCheckpointRequest = true;
    app.iteration = iteration;
    
    updated = startCheckpoint(app, currentSimTime);
    passed = true;
    passed = passed && updated.checkpointing;
    passed = passed && updated.startCheckpointTime == currentSimTime;
    passed = passed && updated.numCheckpoints == numCheckpoints + 1;
    passed = passed && updated.lastCheckpointIteration == iteration;
    passed = passed && ~updated.pendingCheckpointRequest;
end

function apps = prepCheckpointRequestApps(numApps)
    for i = 1:numApps
        apps(i).name = 'Test App';
        apps(i).checkpointing = false;
        apps(i).startCheckpointTime = 0;
        apps(i).numCheckpoints = 10;
        apps(i).lastCheckpointIteration = 0;
        apps(i).pendingCheckpointRequest = true;
        apps(i).iteration = 516;
    end
end

function passed = simpleCheckpointRequestTest
    numApps = 4;

    apps =  prepCheckpointRequestApps(numApps);
    
    apps(1).requestCheckpoint = true;
    apps(2).requestCheckpoint = false;
    apps(3).requestCheckpoint = true;
    apps(4).requestCheckpoint = false;
    apps(4).pendingCheckpointRequest = false;
    
    updated = handleCheckpointRequests(apps, numApps, NO_SHARING, 234203, 5000, 20000);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && ~updated(2).checkpointing;
    passed = passed && updated(3).checkpointing;
    passed = passed && ~updated(4).checkpointing;
    passed = passed && ~updated(1).pendingCheckpointRequest;
    passed = passed && updated(2).pendingCheckpointRequest;
    passed = passed && ~updated(3).pendingCheckpointRequest;
    passed = passed && ~updated(4).pendingCheckpointRequest;
    for i = 1:numApps
        passed = passed && ~updated(i).requestCheckpoint;
    end
end

function apps = prepControlTestApps()
    apps = prepCheckpointRequestApps(2);

    apps(1).requestCheckpoint = true;
    apps(1).checkpointMethod = CHECKPOINT_BY_TIME;
    apps(1).checkpointInterval = 100;
    apps(1).checkpointRequestTime = 500;
    apps(1).checkpointPower = 30;
    apps(1).powerNeeded = apps(1).checkpointPower;
    apps(1).delayedCheckpointTime = 0;
    apps(1).delayedCheckpointIterations = 0;
    apps(1).iteration = 123;
    apps(1).checkpointRequestIteration = 120;
    apps(1).powerLimit = 70;
    
    apps(2).pendingCheckpointRequest = false;
    apps(2).requestCheckpoint = false;
    apps(2).powerNeeded = 110;
end

% This test is designed to let app(1) checkpoint, but just barely.  The
% total power is 140, and the power needed by the two apps is 140.
function passed = simpleControlCheckpointRequestTest(sharingMethod)
    apps = prepControlTestApps();
    updated = handleCheckpointRequests(apps, 2, sharingMethod, 502, 20, 140);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && updated(1).delayedCheckpointTime == 2;
    passed = passed && updated(1).delayedCheckpointIterations == 3;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

function passed = simpleControlCheckpointRequestTestControl
    passed = simpleControlCheckpointRequestTest(CONTROL_SHARING);
end

function passed = simpleControlCheckpointRequestTestControlStack
    passed = simpleControlCheckpointRequestTest(CONTROL_STACK_SHARING);
end

% This test is designed to deny app(1)'s checkpoint request, but just
% barely.  The two apps need 140 watts, but there are 141 watts available,
% so power would be wasted.
function passed = simpleControlCheckpointRequestDeniedTest()
    apps = prepControlTestApps();
    updated = handleCheckpointRequests(apps, 2, CONTROL_SHARING, 502, 20, 141);
    passed = true;
    passed = passed && ~updated(1).checkpointing;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

% This test lets app 1 checkpoint but only because it's been too long since
% it's last checkpoint (just barely) by time.
function passed = checkpointRequestTooLongByTimeTest()
    apps = prepControlTestApps();
    updated = handleCheckpointRequests(apps, 2, CONTROL_SHARING, 550, 20, 141);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && updated(1).delayedCheckpointTime == 50;
    passed = passed && updated(1).delayedCheckpointIterations == 3;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

% This test lets app 1 checkpoint but only because it's been too long since
% it's last checkpoint (just barely) by iterations.
function passed = checkpointRequestTooLongByIterationsTest()
    apps = prepControlTestApps();
    apps(1).checkpointRequestIteration = 73;
    apps(1).checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    updated = handleCheckpointRequests(apps, 2, CONTROL_SHARING, 502, 20, 141);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && updated(1).delayedCheckpointTime == 2;
    passed = passed && updated(1).delayedCheckpointIterations == 50;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

% A repeat of the same two tests, but the checkpoints are denied just
% barely.
function passed = checkpointRequestNotTooLongByTimeTest()
    apps = prepControlTestApps();
    updated = handleCheckpointRequests(apps, 2, CONTROL_SHARING, 549.999, 20, 141);
    passed = true;
    passed = passed && ~updated(1).checkpointing;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

function passed = checkpointRequestNotTooLongByIterationsTest()
    apps = prepControlTestApps();
    apps(1).checkpointRequestIteration = 74;
    apps(1).checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    updated = handleCheckpointRequests(apps, 2, CONTROL_SHARING, 502, 20, 141);
    passed = true;
    passed = passed && ~updated(1).checkpointing;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

% This test verifies that if no applications are checkpointing, an
% application that requests a checkpoint will be able to, even if power is
% wasted.  Note that this decision is made solely by looking at
% shareablePower (the second to last argument to handleCheckpointRequests),
% which will always be zero if no applications are checkpointing.
function passed = checkpointRequestNoOneCheckpointingTest()
    apps = prepControlTestApps();
    updated = handleCheckpointRequests(apps, 2, CONTROL_SHARING, 502, 0, 141);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && updated(1).delayedCheckpointTime == 2;
    passed = passed && updated(1).delayedCheckpointIterations == 3;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
end

function apps = prepTwoCheckpointRequestApps()
    apps = prepCheckpointRequestApps(2);

    apps(1).requestCheckpoint = true;
    apps(1).checkpointMethod = CHECKPOINT_BY_TIME;
    apps(1).checkpointInterval = 100;
    apps(1).checkpointRequestTime = 500;
    apps(1).checkpointPower = 30;
    apps(1).powerNeeded = 80;
    apps(1).delayedCheckpointTime = 0;
    apps(1).delayedCheckpointIterations = 0;
    apps(1).iteration = 123;
    apps(1).checkpointRequestIteration = 120;
    apps(1).powerLimit = 70;
    
    apps(2).requestCheckpoint = true;
    apps(2).checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    apps(2).checkpointInterval = 100;
    apps(2).checkpointRequestTime = 500;
    apps(2).checkpointPower = 50;
    apps(2).powerNeeded = 100;
    apps(2).delayedCheckpointTime = 0;
    apps(2).delayedCheckpointIterations = 0;
    apps(2).iteration = 123;
    apps(2).checkpointRequestIteration = 120;
    apps(2).powerLimit = 90;
end

% This tests the situation in which 2 apps want to checkpoint, but either
% checkpointing will cause power to be wasted.  Only the first should be
% allowed to checkpoint.
function passed = twoCheckpointRequestNoOneCheckpointingTest()
    apps = prepTwoCheckpointRequestApps;
    updated = handleCheckpointRequests(apps, 2, CONTROL_STACK_SHARING, 502, 0, 160);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
    passed = passed && ~updated(2).requestCheckpoint;
end

% Test when 2 apps want to checkpoint but only one can without power being
% wasted.
function passed = twoCheckpointRequestOneCanTest()
    apps = prepTwoCheckpointRequestApps;
    updated = handleCheckpointRequests(apps, 2, CONTROL_STACK_SHARING, 502, 10, 120);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && ~updated(2).checkpointing;
    passed = passed && ~updated(2).requestCheckpoint;
end

% 2 apps want to checkpoint and both can without wasting power
function passed = twoCheckpointRequestBothCanTest()
    apps = prepTwoCheckpointRequestApps;
    updated = handleCheckpointRequests(apps, 2, CONTROL_STACK_SHARING, 502, 10, 80);
    passed = true;
    passed = passed && updated(1).checkpointing;
    passed = passed && ~updated(1).requestCheckpoint;
    passed = passed && updated(2).checkpointing;
    passed = passed && ~updated(2).requestCheckpoint;
end

function passed = doCheckpointByTimeTest()
    app.checkpointMethod = CHECKPOINT_BY_TIME;
    app.checkpointInterval = 400;
    app.delayedCheckpointTime = 20;
    app.stopCheckpointTime = 1020;
    passed = timeToCheckpoint(app, 1400);
end

function passed = dontCheckpointByTimeTest()
    app.checkpointMethod = CHECKPOINT_BY_TIME;
    app.checkpointInterval = 400;
    app.delayedCheckpointTime = 20;
    app.stopCheckpointTime = 1020;
    passed = ~timeToCheckpoint(app, 1399.99);
end

function passed = doCheckpointByIterationsTest()
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    passed = timeToCheckpoint(app, 0);
end

function passed = dontCheckpointByIterationsTest()
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 175;
    app.lastCheckpointIteration = 123;
    passed = ~timeToCheckpoint(app, 0);
end

function passed = finishTest1()
    app.name = 'Test App';
    app.completionTime = 0;
    app.extraWorkDone = 0;
    app.actualStartTime = 0;
    app.powerLimit = 80;
    app.finished = false;
    updated = setFinishTime(app, 1000);
    passed = true;
    passed = passed && updated.completionTime == 1000;
    passed = passed && updated.powerLimit == 0;
    passed = passed && updated.finished;
end

function passed = finishTest2()
    app.name = 'Test App';
    app.completionTime = 0;
    app.extraWorkDone = 0.05;
    app.actualStartTime = 0;
    app.powerLimit = 80;
    app.finished = false;
    updated = setFinishTime(app, 1000);
    passed = true;
    passed = passed && testfleq(updated.completionTime, 999.95);
    passed = passed && updated.powerLimit == 0;
    passed = passed && updated.finished;
end

function passed = finishTest3()
    app.name = 'Test App';
    app.completionTime = 0;
    app.extraWorkDone = 0;
    app.actualStartTime = 400;
    app.powerLimit = 80;
    app.finished = false;
    updated = setFinishTime(app, 1000);
    passed = true;
    passed = passed && updated.completionTime == 600;
    passed = passed && updated.powerLimit == 0;
    passed = passed && updated.finished;
end

function passed = finishTest4()
    app.name = 'Test App';
    app.completionTime = 0;
    app.extraWorkDone = 0.02;
    app.actualStartTime = 817;
    app.powerLimit = 80;
    app.finished = false;
    updated = setFinishTime(app, 1000);
    passed = true;
    passed = passed && testfleq(updated.completionTime, 182.98);
    passed = passed && updated.powerLimit == 0;
    passed = passed && updated.finished;
end

function passed = updatePhaseFinishRestartTest()
    app.name = 'Test App';
    app.restarting = true;
    app.stopCheckpointTime = 0;
    app.lastCheckpointIteration = 0;
    app.iteration = 10;
    app.checkpointTime = 100;
    app.lastRestartTime = 1000;
    updated = updatePhase(app, 1105);
    passed = ~updated.restarting;
end

function passed = updatePhaseDontFinishRestartTest()
    app.name = 'Test App';
    app.restarting = true;
    app.stopCheckpointTime = 0;
    app.lastCheckpointIteration = 0;
    app.iteration = 10;
    app.checkpointTime = 100;
    app.lastRestartTime = 1000;
    updated = updatePhase(app, 1104.999);
    passed = updated.restarting;
end

function passed = updatePhaseDontFinishCheckpointTest()
    app.name = 'Test App';
    app.restarting = false;
    app.checkpointWorkRemaining = 0;
    app.checkpointIteration = 0;
    app.checkpointing = true;
    app.stopCheckpointTime = 0;
    app.totalCheckpointTime = 1000;
    app.workRemaining = 100;
    app.iteration = 10;
    app.startCheckpointTime = 1200;
    app.checkpointTime = 80;
    updated = updatePhase(app, 1279.999);
    passed = updated.checkpointing;
end

% This test verifies that the final checkpoint (after the computation is
% finished) ends at the proper time.
function passed = updatePhaseFinishCheckpointTest()
    app.name = 'Test App';
    app.restarting = false;
    app.checkpointWorkRemaining = 0;
    app.checkpointIteration = 0;
    app.checkpointing = true;
    app.stopCheckpointTime = 0;
    app.totalCheckpointTime = 1000;
    app.workRemaining = 100;
    app.iteration = 10;
    app.startCheckpointTime = 1200;
    app.checkpointTime = 80;
    app.checkpointAtEnd = false;
    updated = updatePhase(app, 1280);
    passed = ~updated.checkpointing;
end

function passed = updatePhaseFinishCheckpointNotAtEndTest()
    app.name = 'Test App';
    app.restarting = false;
    app.finished = false;
    app.checkpointWorkRemaining = 0;
    app.checkpointIteration = 0;
    app.checkpointing = true;
    app.stopCheckpointTime = 0;
    app.totalCheckpointTime = 1000;
    app.workRemaining = 100;
    app.iteration = 10;
    app.startCheckpointTime = 1200;
    app.checkpointTime = 80;
    app.checkpointAtEnd = true;
    updated = updatePhase(app, 1280);
    passed = true;
    passed = passed && ~updated.checkpointing;
    passed = passed && ~updated.finished;
end

function passed = updatePhaseFinishCheckpointAtEndTest()
    app.name = 'Test App';
    app.restarting = false;
    app.finished = false;
    app.checkpointWorkRemaining = 0;
    app.checkpointIteration = 0;
    app.checkpointing = true;
    app.stopCheckpointTime = 0;
    app.totalCheckpointTime = 1000;
    app.workRemaining = 0;
    app.iteration = 10;
    app.startCheckpointTime = 1200;
    app.checkpointTime = 80;
    app.checkpointAtEnd = true;
    app.extraWorkDone = 0;
    app.actualStartTime = 0;
    updated = updatePhase(app, 1280);
    passed = true;
    passed = passed && ~updated.checkpointing;
    passed = passed && updated.finished;
end

function passed = dontUpdatePhaseTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phase = 3;
    app.currentPhaseTime = 39.999;
    updated = updatePhase(app, 100);
    passed = true;
    passed = passed && updated.phase == 3;
    passed = passed && testfleq(updated.currentPhaseTime, 39.999);
end

function passed = updateIntermediatePhaseTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 2;
    app.currentPhaseTime = 30;
    app.iteration = 10;
    updated = updatePhase(app, 100);
    passed = true;
    passed = passed && updated.phase == 3;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 10;
end

function passed = updatePhaseNoFinishNoCheckpointTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.iteration = 10;
    app.workRemaining = 764;
    app.finished = false;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_TIME;
    app.checkpointInterval = 400;
    app.delayedCheckpointTime = 20;
    app.stopCheckpointTime = 1020;
    app.requestCheckpoint = false;

    updated = updatePhase(app, 1399.999);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 11;
    passed = passed && updated.workRemaining == 763;
    passed = passed && ~updated.finished;
    passed = passed && ~updated.checkpointing;
    passed = passed && ~updated.requestCheckpoint;
end

% This test is designed so that it's time to finish and time to checkpoint.
% The app should just finish without taking the checkpoint.
function passed = updatePhaseFinishTest()
    app.name = 'Test App';
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 1;
    app.finished = false;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 175;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.checkpointAtEnd = false;
    app.extraWorkDone = 0;
    app.actualStartTime = 200;

    updated = updatePhase(app, 1234);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 176;
    passed = passed && updated.workRemaining == 0;
    passed = passed && updated.finished;
    passed = passed && ~updated.checkpointing;
    passed = passed && ~updated.requestCheckpoint;
end

% In this test, the app is finished but there's still a final checkpoint to
% take.  This test verifies that the app starts the final checkpoint
% (unlike updatePhaseFinishCheckpointTest above which verifies that the app
% finishes the final checkpoint).
function passed = updatePhaseFinishAppCheckpointTest()
    app.name = 'Test App';
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 1;
    app.finished = false;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 175;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.checkpointAtEnd = true;
    app.extraWorkDone = 0;
    app.actualStartTime = 200;
    app.pendingCheckpointRequest = false;
    app.numCheckpoints = 154;

    updated = updatePhase(app, 1234);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 176;
    passed = passed && updated.workRemaining == 0;
    passed = passed && ~updated.finished;
    passed = passed && updated.checkpointing;   % The app goes straight into the final checkpoint; it doesn't request a checkpoint and wait to start it.
    passed = passed && ~updated.requestCheckpoint;
    passed = passed && ~updated.pendingCheckpointRequest;
end

function passed = updatePhaseStartCheckpointNoPendingTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 764;
    app.finished = false;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.pendingCheckpointRequest = false;
    app.checkpointRequestTime = 0;
    app.checkpointRequestIteration = 0;

    updated = updatePhase(app, 1399.999);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 177;
    passed = passed && updated.workRemaining == 763;
    passed = passed && ~updated.finished;
    passed = passed && ~updated.checkpointing;  % The app doesn't actually start checkpointing, it just requests a checkpoint.
    passed = passed && updated.requestCheckpoint;
    passed = passed && testfleq(updated.checkpointRequestTime, 1399.999);
    passed = passed && updated.checkpointRequestIteration == 177;
    passed = passed && updated.pendingCheckpointRequest;
end

function passed = updatePhaseStartCheckpointPendingTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 764;
    app.finished = false;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.pendingCheckpointRequest = true;
    app.checkpointRequestTime = 100;
    app.checkpointRequestIteration = 200;

    updated = updatePhase(app, 1399.999);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 177;
    passed = passed && updated.workRemaining == 763;
    passed = passed && ~updated.finished;
    passed = passed && ~updated.checkpointing;  % The app doesn't actually start checkpointing, it just requests a checkpoint.
    passed = passed && updated.requestCheckpoint;
    passed = passed && testfleq(updated.checkpointRequestTime, 100);
    passed = passed && updated.checkpointRequestIteration == 200;
    passed = passed && updated.pendingCheckpointRequest;
end

% This, I think, is an impossible situation, but the code handles it.  It
% verifies that if it's time to take a checkpoint but the app is not
% started yet, it doesn't take the checkpoint.
function passed = updatePhaseStartCheckpointNotStartedTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 764;
    app.finished = false;
    app.started = false;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.pendingCheckpointRequest = true;
    app.checkpointRequestTime = 100;
    app.checkpointRequestIteration = 200;

    updated = updatePhase(app, 1399.999);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 177;
    passed = passed && updated.workRemaining == 763;
    passed = passed && ~updated.finished;
    passed = passed && ~updated.checkpointing;
    passed = passed && ~updated.requestCheckpoint;
    passed = passed && testfleq(updated.checkpointRequestTime, 100);
    passed = passed && updated.checkpointRequestIteration == 200;
    passed = passed && updated.pendingCheckpointRequest;
end

% Verify that if it's time to take a checkpoint but the app is paused, it
% doesn't take it.
function passed = updatePhaseStartCheckpointPausedTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 764;
    app.finished = false;
    app.started = true;
    app.paused = true;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.pendingCheckpointRequest = true;
    app.checkpointRequestTime = 100;
    app.checkpointRequestIteration = 200;

    updated = updatePhase(app, 1399.999);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 177;
    passed = passed && updated.workRemaining == 763;
    passed = passed && ~updated.finished;
    passed = passed && ~updated.checkpointing;
    passed = passed && ~updated.requestCheckpoint;
    passed = passed && testfleq(updated.checkpointRequestTime, 100);
    passed = passed && updated.checkpointRequestIteration == 200;
    passed = passed && updated.pendingCheckpointRequest;
end

% This is another impossible situation, I think.  If it's time to
% checkpoint but the app is finished, don't checkpoint.
function passed = updatePhaseStartCheckpointFinishedTest()
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 764;
    app.finished = true;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.pendingCheckpointRequest = true;
    app.checkpointRequestTime = 100;
    app.checkpointRequestIteration = 200;

    updated = updatePhase(app, 1399.999);
    passed = true;
    passed = passed && updated.phase == 1;
    passed = passed && updated.currentPhaseTime == 0;
    passed = passed && updated.iteration == 177;
    passed = passed && updated.workRemaining == 763;
    passed = passed && updated.finished;
    passed = passed && ~updated.checkpointing;
    passed = passed && ~updated.requestCheckpoint;
    passed = passed && testfleq(updated.checkpointRequestTime, 100);
    passed = passed && updated.checkpointRequestIteration == 200;
    passed = passed && updated.pendingCheckpointRequest;
end

% Get the progress factor when the app is checkpointing
function passed = progressFactorCheckpointingTest()
    app.checkpointing = true;
    passed = getProgressFactor(app, 0) == 0;
end

% Get the progress factor when the app is restarting
function passed = progressFactorRestartingTest()
    app.checkpointing = false;
    app.restarting = true;
    passed = getProgressFactor(app, 0) == 0;
end

% Get the progress factor when the app is not started
function passed = progressFactorNotStartedTest()
    app.checkpointing = false;
    app.restarting = false;
    app.started = false;
    passed = getProgressFactor(app, 0) == 0;
end

% Get the progress factor when the app is paused
function passed = progressFactorPausedTest()
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = true;
    passed = getProgressFactor(app, 0) == 0;
end

% Get the progress factor when the app has enough power
function passed = progressFactorFullPowerTest()
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = false;
    app.powerNeeded = 50;
    passed = getProgressFactor(app, 50) == 1;
end

% Get the progress factor when the app doesn't have enough power
function passed = progressFactorPartialPowerTest()
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = false;
    app.powerNeeded = 90;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [30, 90];
    app.phase = 2;
    app.checkpointPower = 30;
    app.scalingFactor = 1;
    factor = getProgressFactor(app, 89.999);
    % We don't check the exact progress factor here - that is tested more
    % thoroughly when PSScaling is tested
    passed = factor < 1.0 && factor > 0.0;
end

% Verify scaling behavior when min and max power match tested power and the
% usage falls exactly on a tested amount.
function passed = simpleScalingTest()
    obj = PSScaling();
    factor = obj.getComputingProgressFactor(PSScaling.LAMMPS_SCALING, 88, 40, 1.2, 60);
    expected = PSScaling.LAMMPS_SPEED(3);
    passed = testfleq(factor, expected);
end

% Verify scaling behavior when min, max, and used power don't match a
% tested amount.
function passed = interpolationScalingTest()
    obj = PSScaling();
    min = 50;
    max = 120;
    used = 100;
    factor = obj.getComputingProgressFactor(PSScaling.PARADIS_SCALING, max, min, 0.3, used);
    expUsed = interp1([min, max], [PSScaling.PARADIS_POWER(1), PSScaling.PARADIS_POWER(end)], used);
    expFactor = interp1(PSScaling.PARADIS_POWER, PSScaling.PARADIS_SPEED, expUsed);
    passed = testfleq(factor, expFactor);
end

% Similar to previous test but with Cactus
function passed = cactusScalingTest()
    obj = PSScaling();
    min = 60;
    max = 80;
    used = 67;
    factor = obj.getComputingProgressFactor(PSScaling.CACTUS_SCALING, max, min, 0.89, used);
    expUsed = interp1([min, max], [PSScaling.CACTUS_POWER(1), PSScaling.CACTUS_POWER(end)], used);
    expFactor = interp1(PSScaling.CACTUS_POWER, PSScaling.CACTUS_SPEED, expUsed);
    passed = testfleq(factor, expFactor);
end

function passed = noScalingTest()
    obj = PSScaling();
    passed = obj.getComputingProgressFactor(PSScaling.NO_SCALING, 500, 3, 0.12, 434) == 1;
end

function passed = customScalingTest()
    obj = PSScaling();
    min = 20;
    max = 80;
    used = 67;
    scale = 0.35;
    factor = obj.getComputingProgressFactor(PSScaling.CUSTOM_SCALING, max, min, scale, used);
    power = PSScaling.CUSTOM_POWER;
    speed = PSScaling.CUSTOM_SPEED;
    
    % I basically copied this code from the PSScaling object because I
    % couldn't think of a better way to calculate it
    m = (max - min) / (power(end) - power(1));
    b = max - m * power(end);
    power = m * power + b;
    speed = speed * scale;
    speed = speed + (1 - speed(end));
    
    expUsed = interp1([min, max], [power(1), power(end)], used);
    expFactor = interp1(power, speed, expUsed);
    passed = testfleq(factor, expFactor);
end

function passed = simulationStepTestNoExtraWork()
    % This test assumes INCREMENT is 0.1
    app.currentAvailablePower = 70;
    app.powerNeeded = 91;
    app.currentUsage = 0;
    app.extraWorkDone = 0;
    app.currentPhaseTime = 9.9;
    app.phaseTime = [10 50];
    app.phase = 1;
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = false;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [91, 80];
    app.checkpointPower = 40;
    app.scalingFactor = 0.1;
    updated = performSimulationStep(app, -1);
    passed = true;
    passed = passed && testfleq(updated.currentUsage, 70);
    passed = passed && updated.extraWorkDone == 0;
    passed = passed && updated.currentPhaseTime > 9.9;
    passed = passed && updated.currentPhaseTime < 10;
end
    
function passed = simulationStepTestExtraWork()
    % This test assumes INCREMENT is 0.1
    app.currentAvailablePower = 90;
    app.powerNeeded = 91;
    app.currentUsage = 0;
    app.extraWorkDone = 2;
    app.currentPhaseTime = 1;
    app.phaseTime = [10 50];
    app.phase = 1;
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = false;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [91, 80];
    app.checkpointPower = 40;
    app.scalingFactor = 0.1;
    updated = performSimulationStep(app, -1);
    passed = true;
    passed = passed && testfleq(updated.currentUsage, 90);
    passed = passed && updated.extraWorkDone == 0;
    passed = passed && updated.currentPhaseTime > 3;
    passed = passed && updated.currentPhaseTime < 3.1;
end

function passed = simulationStepTestExtraPower()
    % This test assumes INCREMENT is 0.1
    app.currentAvailablePower = 200;
    app.powerNeeded = 91;
    app.currentUsage = 0;
    app.extraWorkDone = 0;
    app.currentPhaseTime = 1;
    app.phaseTime = [10 50];
    app.phase = 1;
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = false;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [91, 80];
    app.checkpointPower = 40;
    app.scalingFactor = 0.1;
    updated = performSimulationStep(app, -1);
    passed = true;
    passed = passed && testfleq(updated.currentUsage, 91);
    passed = passed && updated.extraWorkDone == 0;
    passed = passed && testfleq(updated.currentPhaseTime, 1.1);
end

function passed = simulationStepTestEndPhase()
    % This test assumes INCREMENT is 0.1
    app.currentAvailablePower = 200;
    app.powerNeeded = 91;
    app.currentUsage = 0;
    app.extraWorkDone = 0;
    app.currentPhaseTime = 9.92;
    app.phaseTime = [10 50];
    app.phase = 1;
    app.checkpointing = false;
    app.restarting = false;
    app.started = true;
    app.paused = false;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [91, 80];
    app.checkpointPower = 40;
    app.scalingFactor = 0.1;
    updated = performSimulationStep(app, -1);
    passed = true;
    passed = passed && testfleq(updated.currentUsage, 91);
    passed = passed && testfleq(updated.extraWorkDone, 0.02);
    passed = passed && testfleq(updated.currentPhaseTime, 10.02);
end

function passed = simulationStepTestCheckpointingPaused()
    % This test assumes INCREMENT is 0.1
    app.currentAvailablePower = 200;
    app.powerNeeded = 91;
    app.currentUsage = 0;
    app.extraWorkDone = 0;
    app.currentPhaseTime = 9.92;
    app.phaseTime = [10 50];
    app.phase = 1;
    app.checkpointing = true;
    app.restarting = false;
    app.started = true;
    app.paused = true;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [91, 80];
    app.checkpointPower = 40;
    app.scalingFactor = 0.1;
    app.checkpointTimeRemaining = 18.3;
    app.totalCheckpointTime = 17.2;
    updated = performSimulationStep(app, -1);
    passed = true;
    passed = passed && updated.checkpointTimeRemaining == 18.3;
    passed = passed && updated.totalCheckpointTime == 17.2;
end

function passed = simulationStepTestRestartingPaused()
    % This test assumes INCREMENT is 0.1
    app.currentAvailablePower = 200;
    app.powerNeeded = 91;
    app.currentUsage = 0;
    app.extraWorkDone = 0;
    app.currentPhaseTime = 9.92;
    app.phaseTime = [10 50];
    app.phase = 1;
    app.checkpointing = false;
    app.restarting = true;
    app.started = true;
    app.paused = true;
    app.scaling = PSScaling.PARADIS_SCALING;
    app.phasePower = [91, 80];
    app.checkpointPower = 40;
    app.scalingFactor = 0.1;
    app.restartTimeRemaining = 18.3;
    updated = performSimulationStep(app, -1);
    passed = true;
    passed = passed && updated.restartTimeRemaining == 18.3;
end

function passed = allocatePowerTest()
    app.checkpointing = false;
    app.restarting = false;
    app.extraPower = -20;
    app.currentAvailablePower = 50;
    app.borrowedPower = 10;
    [updated, remainingPower] = allocatePower(app, 100);
    passed = true;
    passed = passed && updated.currentAvailablePower == 70;
    passed = passed && updated.borrowedPower == 30;
    passed = passed && updated.extraPower == 0;
    passed = passed && remainingPower == 80;
end

function passed = allocateInsufficientPowerTest()
    app.checkpointing = false;
    app.restarting = false;
    app.extraPower = -20;
    app.currentAvailablePower = 50;
    app.borrowedPower = 10;
    [updated, remainingPower] = allocatePower(app, 10);
    passed = true;
    passed = passed && updated.currentAvailablePower == 60;
    passed = passed && updated.borrowedPower == 20;
    passed = passed && updated.extraPower == -10;
    passed = passed && remainingPower == 0;
end

function passed = dontAllocatePowerTest()
    app.checkpointing = false;
    app.restarting = false;
    app.extraPower = 0;
    app.currentAvailablePower = 50;
    app.borrowedPower = 10;
    [updated, remainingPower] = allocatePower(app, 1000);
    passed = true;
    passed = passed && updated.currentAvailablePower == 50;
    passed = passed && updated.borrowedPower == 10;
    passed = passed && updated.extraPower == 0;
    passed = passed && remainingPower == 1000;
end

function passed = allocatePowerCheckpointingTest()
    app.checkpointing = true;
    app.restarting = false;
    app.extraPower = -5;
    app.currentAvailablePower = 50;
    app.borrowedPower = 10;
    [updated, remainingPower] = allocatePower(app, 1000);
    passed = true;
    passed = passed && updated.currentAvailablePower == 50;
    passed = passed && updated.borrowedPower == 10;
    passed = passed && updated.extraPower == -5;
    passed = passed && remainingPower == 1000;
end

function passed = allocatePowerRestartingTest()
    app.checkpointing = false;
    app.restarting = true;
    app.extraPower = -5;
    app.currentAvailablePower = 50;
    app.borrowedPower = 10;
    [updated, remainingPower] = allocatePower(app, 1000);
    passed = true;
    passed = passed && updated.currentAvailablePower == 50;
    passed = passed && updated.borrowedPower == 10;
    passed = passed && updated.extraPower == -5;
    passed = passed && remainingPower == 1000;
end

function apps = prepStackApps()
    for i = 1:4
        apps(i).checkpointing = false;
        apps(i).restarting = false;
        apps(i).extraPower = -20;
        apps(i).currentAvailablePower = 50;
        apps(i).borrowedPower = 10;
    end
end

function passed = simpleStackTest()
    apps = prepStackApps;
    updated = stackSharing(apps, 4, 45, 3);
    passed = true;
    passed = passed && updated(1).currentAvailablePower == 55;
    passed = passed && updated(2).currentAvailablePower == 50;
    passed = passed && updated(3).currentAvailablePower == 70;
    passed = passed && updated(4).currentAvailablePower == 70;
end

% Ordering is only supported if the favored app is 1
function passed = orderingStackTest()
    apps = prepStackApps;
    ordering = [1, 2, 4, 3];
    updated = stackSharing(apps, 4, 45, 1, ordering);
    passed = true;
    passed = passed && updated(1).currentAvailablePower == 70;
    passed = passed && updated(2).currentAvailablePower == 70;
    passed = passed && updated(3).currentAvailablePower == 50;
    passed = passed && updated(4).currentAvailablePower == 55;
end

function passed = stackTestExtraPower()
    apps = prepStackApps;
    updated = stackSharing(apps, 4, 80, 3);
    passed = true;
    passed = passed && updated(1).currentAvailablePower == 70;
    passed = passed && updated(2).currentAvailablePower == 70;
    passed = passed && updated(3).currentAvailablePower == 70;
    passed = passed && updated(4).currentAvailablePower == 70;
end

function apps = prepReceiverApps()
    apps(1).extraPower = -50;
    apps(1).nodes = 16;
    apps(2).extraPower = 0;
    apps(2).nodes = 512;
    apps(3).extraPower = 40;
    apps(3).nodes = 64;
    apps(4).extraPower = -1;
    apps(4).nodes = 52;
end

function passed = countReceiverAppsTest()
    apps = prepReceiverApps;
    passed = countReceivers(apps, 4, false) == 2;
end

function passed = countReceiverNodesTest()
    apps = prepReceiverApps;
    passed = countReceivers(apps, 4, true) == 68;
end

% The favored app tests must be run in order because the function has
% persistent data.  Each test depends on the output from the previous test.
function apps = prepFavoredApps()
    apps(1).started = true;
    apps(1).finished = false;
    apps(2).started = true;
    apps(2).finished = false;
    apps(3).started = true;
    apps(3).finished = false;
    apps(4).started = false;
    apps(4).finished = false;
end

function passed = initFavoredAppTest()
    apps = prepFavoredApps;
    getFavoredApp([], 0, 0, true);
    passed = getFavoredApp(apps, 4, 29.999, false) == 1;
end

function passed = advanceFavoredAppTest()
    apps = prepFavoredApps;
    passed = true;
    passed = passed && getFavoredApp(apps, 4, 30, false) == 2;
    passed = passed && getFavoredApp(apps, 4, 60, false) == 3;
end

% This test verifies that we skip apps that haven't started or have
% finished.  In this test, we skip apps 4 and 1.
function passed = skipFavoredAppTest()
    apps = prepFavoredApps;
    apps(1).finished = true;
    passed = getFavoredApp(apps, 4, 90, false) == 2;
end

function passed = notStartedPowerNeedsTest()
    app.started = false;
    app.finished = false;
    app.checkpointing = false;
    app.powerLimit = 60;
    app.currentAvailablePower = 0;
    app.powerNeeded = 0;
    app.extraPower = 0;
    app.shareablePower = 0;
    updated = calculatePowerNeeds(app, 20);
    passed = true;
    passed = passed && updated.currentAvailablePower == 60;
    passed = passed && updated.powerNeeded == 60;
    passed = passed && updated.extraPower == 0;
    passed = passed && updated.shareablePower == 0;
end

function passed = finishedPowerNeedsTest()
    app.started = true;
    app.finished = true;
    app.checkpointing = false;
    app.powerLimit = 50;
    app.currentAvailablePower = 0;
    app.powerNeeded = 0;
    app.extraPower = 0;
    app.shareablePower = 0;
    updated = calculatePowerNeeds(app, 200);
    passed = true;
    passed = passed && updated.currentAvailablePower == 50;
    passed = passed && updated.powerNeeded == 50;
    passed = passed && updated.extraPower == 0;
    passed = passed && updated.shareablePower == 0;
end

function passed = pausedPowerNeedsTest()
    app.started = true;
    app.finished = false;
    app.checkpointing = false;
    app.paused = true;
    app.powerLimit = 750;
    app.nodes = 10;
    app.currentAvailablePower = 0;
    app.powerNeeded = 0;
    app.extraPower = 0;
    app.shareablePower = 0;
    updated = calculatePowerNeeds(app, 23);
    passed = true;
    passed = passed && updated.currentAvailablePower == 750;
    passed = passed && updated.powerNeeded == 230;
    passed = passed && updated.extraPower == 520;
    passed = passed && updated.shareablePower == 520;
end

function passed = checkpointPowerNeedsTest()
    app.started = true;
    app.finished = false;
    app.checkpointing = true;
    app.paused = false;
    app.powerLimit = 75;
    app.checkpointPower = 30;
    app.currentAvailablePower = 0;
    app.powerNeeded = 0;
    app.extraPower = 0;
    app.shareablePower = 0;
    updated = calculatePowerNeeds(app, 23);
    passed = true;
    passed = passed && updated.currentAvailablePower == 75;
    passed = passed && updated.powerNeeded == 30;
    passed = passed && updated.extraPower == 45;
    passed = passed && updated.shareablePower == 45;
end

function passed = restartPowerNeedsTest()
    app.started = true;
    app.finished = false;
    app.checkpointing = false;
    app.restarting = true;
    app.paused = false;
    app.powerLimit = 75;
    app.checkpointPower = 30;
    app.nodes = 1;
    app.currentAvailablePower = 0;
    app.powerNeeded = 0;
    app.extraPower = 0;
    app.shareablePower = 0;
    updated = calculatePowerNeeds(app, 23);
    passed = true;
    passed = passed && updated.currentAvailablePower == 75;
    passed = passed && updated.powerNeeded == 40;
    passed = passed && updated.extraPower == 35;
    passed = passed && updated.shareablePower == 35;
end

function passed = simplePowerNeedsTest()
    app.started = true;
    app.finished = false;
    app.checkpointing = false;
    app.restarting = false;
    app.paused = false;
    app.powerLimit = 75;
    app.checkpointPower = 30;
    app.nodes = 1;
    app.phasePower = [20 120 75 90 86];
    app.phase = 4;
    app.currentAvailablePower = 0;
    app.powerNeeded = 0;
    app.extraPower = 0;
    app.shareablePower = 0;
    updated = calculatePowerNeeds(app, 23);
    passed = true;
    passed = passed && updated.currentAvailablePower == 75;
    passed = passed && updated.powerNeeded == 90;
    passed = passed && updated.extraPower == -15;
    passed = passed && updated.shareablePower == 0;
end

function passed = pauseTestNominal
    lastRestartTime = 15;
    startCheckpointTime = 20;
    currentSimTime = 1501;
    duration = 20;
    
    app.name = 'Test App';
    app.paused = false;
    app.endPauseTime = 0;
    app.restarting = false;
    app.checkpointing = false;
    app.lastRestartTime = lastRestartTime;
    app.startCheckpointTime = startCheckpointTime;
    
    updated = PSPauseApp(app, currentSimTime, duration, 'Test: ');
    
    passed = true;
    passed = passed && updated.paused;
    passed = passed && updated.endPauseTime == currentSimTime + duration;
    passed = passed && updated.lastRestartTime == lastRestartTime;
    passed = passed && updated.startCheckpointTime == startCheckpointTime;
end

function passed = pauseTestPaused
    lastRestartTime = 27;
    startCheckpointTime = 83;
    currentSimTime = 1501;
    duration = 55;
    endPauseTime = 1700;
    
    app.name = 'Test App';
    app.paused = false;
    app.endPauseTime = endPauseTime;
    app.restarting = false;
    app.checkpointing = false;
    app.lastRestartTime = lastRestartTime;
    app.startCheckpointTime = startCheckpointTime;
    
    updated = PSPauseApp(app, currentSimTime, duration, 'Test: ');
    
    passed = true;
    passed = passed && updated.paused;
    passed = passed && updated.endPauseTime == currentSimTime + duration;
    passed = passed && updated.lastRestartTime == lastRestartTime;
    passed = passed && updated.startCheckpointTime == startCheckpointTime;
end

function passed = pauseTestRestarting
    lastRestartTime = 27;
    startCheckpointTime = 83;
    currentSimTime = 1501;
    duration = 55;
    endPauseTime = 1700;
    
    app.name = 'Test App';
    app.paused = false;
    app.endPauseTime = endPauseTime;
    app.restarting = true;
    app.checkpointing = false;
    app.lastRestartTime = lastRestartTime;
    app.startCheckpointTime = startCheckpointTime;
    
    updated = PSPauseApp(app, currentSimTime, duration, 'Test: ');
    
    passed = true;
    passed = passed && updated.paused;
    passed = passed && updated.endPauseTime == currentSimTime + duration;
    passed = passed && testfleq(updated.lastRestartTime, lastRestartTime + duration);
    passed = passed && updated.startCheckpointTime == startCheckpointTime;
end

function passed = pauseTestCheckpointing
    lastRestartTime = 27;
    startCheckpointTime = 83;
    currentSimTime = 1501;
    duration = 55;
    endPauseTime = 1700;
    
    app.name = 'Test App';
    app.paused = false;
    app.endPauseTime = endPauseTime;
    app.restarting = false;
    app.checkpointing = true;
    app.lastRestartTime = lastRestartTime;
    app.startCheckpointTime = startCheckpointTime;
    
    updated = PSPauseApp(app, currentSimTime, duration, 'Test: ');
    
    passed = true;
    passed = passed && updated.paused;
    passed = passed && updated.endPauseTime == currentSimTime + duration;
    passed = passed && updated.lastRestartTime == lastRestartTime;
    passed = passed && testfleq(updated.startCheckpointTime, startCheckpointTime + duration);
end

function passed = pauseTestRestartingPaused
    lastRestartTime = 27;
    startCheckpointTime = 83;
    currentSimTime = 1501;
    duration = 55;
    endPauseTime = 1700;
    
    app.name = 'Test App';
    app.paused = true;
    app.endPauseTime = endPauseTime;
    app.restarting = true;
    app.checkpointing = false;
    app.lastRestartTime = lastRestartTime;
    app.startCheckpointTime = startCheckpointTime;
    
    remainingOldPause = endPauseTime - currentSimTime;
    
    updated = PSPauseApp(app, currentSimTime, duration, 'Test: ');
    
    passed = true;
    passed = passed && updated.paused;
    passed = passed && updated.endPauseTime == currentSimTime + duration;
    passed = passed && testfleq(updated.lastRestartTime, lastRestartTime + duration - remainingOldPause);
    passed = passed && updated.startCheckpointTime == startCheckpointTime;
end

function passed = pauseTestCheckpointingPaused
    lastRestartTime = 27;
    startCheckpointTime = 83;
    currentSimTime = 1501;
    duration = 55;
    endPauseTime = 1700;
    
    app.name = 'Test App';
    app.paused = true;
    app.endPauseTime = endPauseTime;
    app.restarting = false;
    app.checkpointing = true;
    app.lastRestartTime = lastRestartTime;
    app.startCheckpointTime = startCheckpointTime;
    
    remainingOldPause = endPauseTime - currentSimTime;
    
    updated = PSPauseApp(app, currentSimTime, duration, 'Test: ');
    
    passed = true;
    passed = passed && updated.paused;
    passed = passed && updated.endPauseTime == currentSimTime + duration;
    passed = passed && updated.lastRestartTime == lastRestartTime;
    passed = passed && testfleq(updated.startCheckpointTime, startCheckpointTime + duration - remainingOldPause);
end

% This checks a bug from an earlier version.  If a job requested a
% checkpoint, then restarted on the same iteration, the job thought it
% was checkpointing and restarting at the same time.
function passed = checkpointRestartTest
    currentSimTime = 1399.999;

    app.name = 'App 0001';
    app.restarting = false;
    app.checkpointing = false;
    app.phaseTime = [20, 30, 40];
    app.phasePower = [50, 90, 80];
    app.phase = 3;
    app.currentPhaseTime = 40;
    app.workRemaining = 764;
    app.finished = false;
    app.started = true;
    app.paused = false;
    app.checkpointMethod = CHECKPOINT_BY_ITERATIONS;
    app.checkpointInterval = 60;
    app.delayedCheckpointIterations = 7;
    app.iteration = 176;
    app.lastCheckpointIteration = 123;
    app.requestCheckpoint = false;
    app.pendingCheckpointRequest = false;
    app.checkpointRequestTime = 0;
    app.checkpointRequestIteration = 0;
    app.nodes = 1;
    app.checkpointWorkRemaining = 0;
    app.checkpointIteration = 170;
    app.numCheckpoints = 3;
    app.startCheckpointTime = 0;
    
    % Request a checkpoint
    updated = updatePhase(app, currentSimTime);
    % Fail
    updated = failAndRestart(updated, currentSimTime);
    % Try to start the checkpoint
    updated = handleCheckpointRequests(updated, 1, NO_SHARING, currentSimTime, 100, 200);
    
    % Verify that we're restarting but not checkpointing
    passed = true;
    passed = passed && ~updated.checkpointing;
    passed = passed && updated.restarting;
    passed = passed && ~updated.pendingCheckpointRequest;
    passed = passed && ~updated.requestCheckpoint;
end

%------------------------------------------------------------------------%
% The test runner.  This is at the end to make it easy to find.

function runTests
    tests = { ...
        @nextFailureTimeTest1Node ...
        @nextFailureTimeTest64Node ...
        @failAndRestartTest ...
        @failAndRestartTestCheckpointing ...
        @failAndRestartTestPaused ...
        @failAndRestartTestNotStarted ...
        @failAndRestartTestFinished ...
        @finishRestartTest ...
        @finishCheckpointTest ...
        @startCheckpointTest ...
        @simpleCheckpointRequestTest ...
        @simpleControlCheckpointRequestTestControl ...
        @simpleControlCheckpointRequestTestControlStack ...
        @simpleControlCheckpointRequestDeniedTest ...
        @checkpointRequestTooLongByTimeTest ...
        @checkpointRequestTooLongByIterationsTest ...
        @checkpointRequestNotTooLongByTimeTest ...
        @checkpointRequestNotTooLongByIterationsTest ...
        @checkpointRequestNoOneCheckpointingTest ...
        @twoCheckpointRequestNoOneCheckpointingTest ...
        @twoCheckpointRequestOneCanTest ...
        @twoCheckpointRequestBothCanTest ...
        @doCheckpointByTimeTest ...
        @dontCheckpointByTimeTest ...
        @doCheckpointByIterationsTest ...
        @dontCheckpointByIterationsTest ...
        @finishTest1 ...
        @finishTest2 ...
        @finishTest3 ...
        @finishTest4 ...
        @updatePhaseFinishRestartTest ...
        @updatePhaseDontFinishRestartTest ...
        @updatePhaseDontFinishCheckpointTest ...
        @updatePhaseFinishCheckpointTest ...
        @updatePhaseFinishCheckpointNotAtEndTest ...
        @updatePhaseFinishCheckpointAtEndTest ...
        @dontUpdatePhaseTest ...
        @updateIntermediatePhaseTest ...
        @updatePhaseNoFinishNoCheckpointTest ...
        @updatePhaseFinishTest ...
        @updatePhaseFinishAppCheckpointTest ...
        @updatePhaseStartCheckpointNoPendingTest ...
        @updatePhaseStartCheckpointPendingTest ...
        @updatePhaseStartCheckpointNotStartedTest ...
        @updatePhaseStartCheckpointPausedTest ...
        @updatePhaseStartCheckpointFinishedTest ...
        @progressFactorCheckpointingTest ...
        @progressFactorRestartingTest ...
        @progressFactorNotStartedTest ...
        @progressFactorPausedTest ...
        @progressFactorFullPowerTest ...
        @progressFactorPartialPowerTest ...
        @simpleScalingTest ...
        @interpolationScalingTest ...
        @cactusScalingTest ...
        @noScalingTest ...
        @customScalingTest ...
        @simulationStepTestNoExtraWork ...
        @simulationStepTestExtraWork ...
        @simulationStepTestExtraPower ...
        @simulationStepTestEndPhase ...
        @simulationStepTestCheckpointingPaused ...
        @simulationStepTestRestartingPaused ...
        @allocatePowerTest ...
        @allocateInsufficientPowerTest ...
        @dontAllocatePowerTest ...
        @allocatePowerCheckpointingTest ...
        @allocatePowerRestartingTest ...
        @simpleStackTest ...
        @orderingStackTest ...
        @stackTestExtraPower ...
        @countReceiverAppsTest ...
        @countReceiverNodesTest ...
        @initFavoredAppTest ...
        @advanceFavoredAppTest ...
        @skipFavoredAppTest ...
        @notStartedPowerNeedsTest ...
        @finishedPowerNeedsTest ...
        @pausedPowerNeedsTest ...
        @checkpointPowerNeedsTest ...
        @restartPowerNeedsTest ...
        @simplePowerNeedsTest ...
        @pauseTestNominal ...
        @pauseTestPaused ...
        @pauseTestRestarting ...
        @pauseTestCheckpointing ...
        @pauseTestRestartingPaused ...
        @pauseTestCheckpointingPaused ...
        @checkpointRestartTest ...
    };
    
    header = '----> ';
    disp('--------------------------------------------------------------------');
    numTests = length(tests);
    failures = 0;
    for i = 1:numTests
        testFunc = tests{i};
        if CATCH_TEST_ERRORS
            try
                passed = feval(testFunc);
            catch
                passed = false;
            end
        else
            passed = feval(testFunc);
        end
        if (~passed)
            failures = failures + 1;
            disp([header func2str(testFunc) ' failed.']);
        end
    end
    
    disp('--------------------------------------------------------------------');
    disp([header num2str(numTests) ' tests run, ' num2str(failures) ' failures.']);
    disp('--------------------------------------------------------------------');
end
