% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.

% This file "chops" the output from a set of tests so that each test is in
% its own file.
function chopoutput

fid = fopen('output.txt');

run = 1;
config = 1;

fout = -1;

while true
    line = fgetl(fid);
    if feof(fid)
        break;
    end
    
    if (fout == -1)
        fout = fopen(['output-' num2str(run) '-' num2str(config) '.txt'], 'w');
    end

    sanitized = strrep(line, '%', '%%');
    fprintf(fout, [sanitized '\n']);
    
    if contains(line, 'Total run time after run ')
        fclose(fout);
        fout = -1;
        config = config + 1;
    elseif streq(line, 'Totals for all apps (execution time, average speedup, min speedup, max speedup, avg of percent speedups)')
        run = run + 1;
        
        % Have to reset this to zero because there is an extra line containing
        % "Total run time after run" at the very end of the run.
        config = 0;
    end
end

fclose(fid);
if fout ~= -1
    fclose(fout);
end

end

function result = contains(line, string)
    result = ~isempty(strfind(line, string));
end

function result = streq(str1, str2)
    result = strcmp(str1, str2);
end

