% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.

% A class to track the state of applications during a run.  Used by the
% appStates script.
classdef StateManager < handle
    properties (Access = private)
        states;
        currentState;
        lastTransition;
        lastCheckpoint;
        pauseState;
    end
    methods
        % Constructor
        function obj = StateManager(NUM_APPS)
            obj.states = zeros(NUM_APPS, PSState.NUM_STATES);
            obj.currentState = ones(NUM_APPS) * PSState.NOT_STARTED;
            obj.lastTransition = zeros(NUM_APPS);
            obj.lastCheckpoint = zeros(NUM_APPS);
            obj.pauseState = ones(NUM_APPS) * PSState.NOT_STARTED;
        end
        
        % Transition functions
        function start(obj, app, time)
            obj.updateState(app, time, PSState.COMPUTING, PSState.NOT_STARTED);
        end
        
        function startCheckpoint(obj, app, time)
            % All apps do a "fake checkpoint" before they start, which is
            % ignored.
            if obj.currentState(app) == PSState.NOT_STARTED
                return;
            end
            
            obj.updateState(app, time, PSState.IO, [PSState.COMPUTING, PSState.PAUSED]);
        end
        
        function stopCheckpoint(obj, app, time)
            obj.lastCheckpoint(app) = time;
            % All apps do a "fake checkpoint" before they start, which is
            % ignored.
            if obj.currentState(app) == PSState.NOT_STARTED
                return;
            end
            obj.updateState(app, time, PSState.COMPUTING, PSState.IO);
        end
        
        function pause(obj, app, time)
            % Record the state we were in before pausing so we can go back
            % to that state.
            state = obj.currentState(app);
            if (state ~= PSState.PAUSED)
                obj.pauseState(app) = state;
            end
            obj.updateState(app, time, PSState.PAUSED, [PSState.COMPUTING, PSState.IO, PSState.RESTARTING, PSState.PAUSED]);
        end
        
        function unpause(obj, app, time)
            % Figure out which state to go back to
            newState = obj.pauseState(app);
            if newState == PSState.NOT_STARTED
                error('Un-pausing but previous state is not available');
            end
            obj.pauseState(app) = PSState.NOT_STARTED;
            obj.updateState(app, time, newState, PSState.PAUSED);
        end
        
        function startRestart(obj, app, time)
            % If we restart in the middle of a pause, the pause is no
            % longer valid.
            obj.pauseState(app) = PSState.NOT_STARTED;
            obj.updateState(app, time, PSState.RESTARTING, [PSState.COMPUTING, PSState.IO, PSState.RESTARTING, PSState.PAUSED]);
            % Calculate how much time was lost since the last checkpoint
            obj.states(app, PSState.LOST_TIME) = obj.states(app, PSState.LOST_TIME) + (time - obj.lastCheckpoint(app));
        end
        
        function stopRestart(obj, app, time)
            obj.updateState(app, time, PSState.COMPUTING, PSState.RESTARTING);
        end
        
        function finish(obj, app, time)
            obj.updateState(app, time, PSState.FINISHED, PSState.COMPUTING);
        end
        
        % Getters
        function states = getStates(obj)
            states = obj.states;
        end
    end
    methods (Access = private)
        function invalidTransition(obj, app, newState)
            %error(['Invalid transition for App ' num2str(app) ': ' PSState.toString(obj.currentState(app)) ' to ' PSState.toString(newState)]);
            disp(['---->Invalid transition for App ' num2str(app) ': ' PSState.toString(obj.currentState(app)) ' to ' PSState.toString(newState)]);
        end
        function updateState(obj, app, time, newState, validStates)
            state = obj.currentState(app);

            % Check for invalid transitions
            if ~any(validStates == state)
                obj.invalidTransition(app, newState);
            end
            
            % Update the time spent in the previous state
            obj.states(app, state) = obj.states(app, state) + (time - obj.lastTransition(app));
            
            % Move to the new state
            obj.currentState(app) = newState;
            obj.lastTransition(app) = time;
        end
    end
end
