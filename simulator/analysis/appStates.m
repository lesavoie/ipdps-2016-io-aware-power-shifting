% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.

% Returns data describing the times that each application changed states
% (running, checkpointing, etc) during the course of a run.  This is useful
% as an input to more detailed analyses.
function states = appStates

load results.mat;

fid = fopen('output.txt');

run = 1;
config = 1;

states = zeros(MONTE_CARLO_RUNS, NUM_CONFIGS, NUM_APPS, PSState.NUM_STATES);
sm = StateManager(NUM_APPS);

while true
    line = fgetl(fid);
    if feof(fid)
        break;
    end
    
    app = getApp(line);
    time = getTime(line);
    
    if contains(line, 'Starting application')
        sm.start(app, time);
        
    elseif contains(line, 'Start checkpointing')
        sm.startCheckpoint(app, time);
    elseif contains(line, 'Stop checkpointing')
        sm.stopCheckpoint(app, time);
        
    elseif contains(line, 'Pausing app')
        sm.pause(app, time);
    elseif contains(line, 'Un-pausing app')
        sm.unpause(app, time);
        
    elseif contains(line, 'Start restart')
        sm.startRestart(app, time);
    elseif contains(line, 'Finish restart')
        sm.stopRestart(app, time);
        
    elseif contains(line, 'finished at time')
        sm.finish(app, time);
        
    elseif contains(line, 'Total run time after run ')
        if config ~= 0
            states(run, config, :, :) = sm.getStates;
            sm = StateManager(NUM_APPS);
        end
        config = config + 1;
    elseif streq(line, 'Totals for all apps (execution time, average speedup, min speedup, max speedup, avg of percent speedups)')
        run = run + 1;
        
        % Have to reset this to zero because there is an extra line containing
        % "Total run time after run" at the very end of the run.
        config = 0;
    end
end

fclose(fid);

end

function app = getApp(line)
i = strfind(line, 'App ') + 4;
if isempty(i)
    app = -1;
else
    appStr = '';
    while isstrprop(line(i), 'digit')
        appStr = [appStr line(i)]; %#ok<AGROW>
        i = i + 1;
    end
    app = int32(str2double(appStr));
end
end

function time = getTime(line)
    % This works on the assumption that the time is the last token on the
    % line.
    i = length(line);
    str = '';
    while isstrprop(line(i), 'digit') || line(i) == '.'
        str = [line(i), str]; %#ok<AGROW>
        i = i - 1;
    end
    time = str2double(str);
end

function result = contains(line, string)
    result = ~isempty(strfind(line, string));
end

function result = streq(str1, str2)
    result = strcmp(str1, str2);
end
