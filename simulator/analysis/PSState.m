% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
classdef PSState
    properties (Constant)
        % This class defines the states an app can be in.  It is used by
        % analysis functions.
        NOT_STARTED = 1;
        COMPUTING = 2;
        IO = 3;
        RESTARTING = 4;
        PAUSED = 5;
        FINISHED = 6;
        
        % For the purposes of analysis, FINISHED is ignored and instead we
        % track time lost due to restarts.
        LOST_TIME = 6;
        
        NUM_STATES = 6;
    end
    methods(Static)
        function str = toString(state)
            if state == PSState.NOT_STARTED
                str = 'not started';
            elseif state == PSState.COMPUTING
                str = 'computing';
            elseif state == PSState.IO
                str = 'I/O';
            elseif state == PSState.RESTARTING
                str = 'restarting';
            elseif state == PSState.PAUSED
                str = 'paused';
            elseif state == PSState.FINISHED
                str = 'finished';
            else
                str = 'unexpected state';
            end
        end
    end
end
