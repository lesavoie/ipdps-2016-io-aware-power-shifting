% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.

% This script prints the lines pertaning to a single application from a
% specific run.  You must run "chopoutput" before running this script.
function getApp(run, config, app)

fid = fopen(['output-' num2str(run) '-' num2str(config) '.txt']);
appName = ['App ' num2str(app, '%04d')];
repeatDenied = false;
started = false;

while true
    line = fgetl(fid);
    if feof(fid)
        break;
    end
    
    if contains(line, appName)
        if contains(line, 'Starting')
            started = true;
        end
        if ~contains(line, 'Denied')
            repeatDenied = false;
        end
        if ~repeatDenied && started
            disp(line);
        end
        % Don't display repeated messages stating that a checkpoint was
        % denied.
        if contains(line, 'Denied')
            repeatDenied = true;
        else
            repeatDenied = false;
        end
    end
end

fclose(fid);

end

function result = contains(line, string)
    result = ~isempty(strfind(line, string));
end

