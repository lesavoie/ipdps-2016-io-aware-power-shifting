% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.
classdef StaggerAlgorithm < handle
    properties (Access = private)
        MAX_GROUP_SIZE = 8;
        MIN_GROUP_SIZE = 3;
        MAX_DELAY_FACTOR = 0.03;
        SMALL_GROUPS_ALLOWED = 1;
        numApps;
        CHECKPOINT_BY_ITERATIONS;
        task;
        runningApps = [];
        arrivedApps = [];
        departedApps = [];
        groups = {};
        appDelayRemaining;
        freeGroups = [];
        groupsWithDepartures;
        appsToPause;
        overallDelay = 0;
    end
    methods
        function obj = StaggerAlgorithm(numApps, CHECKPOINT_BY_ITERATIONS)
            obj.numApps = numApps;
            obj.CHECKPOINT_BY_ITERATIONS = CHECKPOINT_BY_ITERATIONS;
            obj.task = 'Stagger: ';
            obj.appDelayRemaining = zeros(numApps, 1);
        end
        
        function apps = doScheduling(obj, apps, currentSimTime)
            if obj.runningAppSetChanged(apps)
                obj.appsToPause = containers.Map('KeyType', 'int32', 'ValueType', 'double');
                
                % Clean out any departed apps from their groups.  This
                % includes disbanding groups that have become too small.
                apps = obj.removeDepartedApps(apps, currentSimTime);
                
                % Add any new apps to a group
                for a = 1:length(obj.arrivedApps)
                    apps = obj.findGroup(apps, obj.arrivedApps(a), currentSimTime);
                end
                
                % Disband any groups that have become too small by reason
                % of a departure.
                apps = obj.disbandGroups(apps, currentSimTime);
                
                % Pause any apps that should be paused
                apps = obj.applyPauses(apps, currentSimTime);
                
                %Clean up
                obj.arrivedApps = [];
                obj.departedApps = [];
            end
        end
        
        function shutdown(obj)
            disp([obj.task 'Overall delay: ' num2str(obj.overallDelay) ' seconds']);
        end
    end
    methods (Access = private)
        function changed = runningAppSetChanged(obj, apps)
            % Loop through all the apps and see if any have started or
            % finished since we last ran.
            changed = false;
            for i = 1:obj.numApps
                % See if a new application started.
                if apps(i).started && ~apps(i).restarting && ~apps(i).finished && ~any(obj.runningApps == i)
                    % Add it to the list of running apps
                    obj.runningApps(end + 1) = i;
                    obj.arrivedApps(end + 1) = i;
                    changed = true;
                    
                    % Set the maximum delay for this app to a percentage of
                    % its runtime (not counting checkpoints) at full power.
                    % Note that this formula is correct regardless of
                    % whether the app is starting or restarting.
                    obj.appDelayRemaining(i) = (sum(apps(i).phaseTime) * apps(i).workRemaining) * obj.MAX_DELAY_FACTOR;
                end
                % See if an application finished
                if (apps(i).finished || apps(i).restarting) && any(obj.runningApps == i)
                    % Remove it from the list of running apps.  An app that
                    % has failed and is restarting is treated as a job
                    % departure.  When it finishes restarting, it will be
                    % added again in the if statement above.
                    obj.runningApps(obj.runningApps == i) = [];
                    obj.departedApps(end + 1) = i;
                    changed = true;
                end
            end
        end
        
        function apps = removeDepartedApps(obj, apps, currentSimTime)
            obj.groupsWithDepartures = [];
            for g = 1:length(obj.groups)
                departedFromGroup = intersect(obj.departedApps, obj.groups{g});
                if ~isempty(departedFromGroup)
                    disp([obj.task 'Removing apps ' num2str(departedFromGroup) ' from group ' num2str(g)]);
                    obj.groups{g} = setdiff(obj.groups{g}, departedFromGroup);
                    % If the group has become empty, disband it now.
                    % Otherwise, add it to the list of groups to be
                    % considered for disbanding after new apps have been
                    % added.
                    if isempty(obj.groups{g})
                        apps = obj.disbandGroup(g, apps, currentSimTime);
                    else
                        obj.groupsWithDepartures(end + 1) = g;
                    end
                end
            end
        end
        
        function smallGroups = countSmallGroups(obj)
            smallGroups = 0;
            for g = 1:length(obj.groups)
                groupSize = length(obj.groups{g});
                if groupSize > 0 && groupSize < obj.MIN_GROUP_SIZE
                    smallGroups = smallGroups + 1;
                end
            end
        end
        
        function apps = findGroup(obj, apps, newApp, currentSimTime)
            success = false;
            for g = 1:length(obj.groups)
                groupSize = length(obj.groups{g});
                % At this stage we ignore empty groups because they are
                % disbanded.  If we need to create a new group, that
                % happens in the if statement after this loop.
                if groupSize > 0 && groupSize < obj.MAX_GROUP_SIZE
                    [apps, success] = obj.addToGroup(apps, newApp, g, currentSimTime);
                    if success
                        break;
                    end
                end
            end
            % If we couldn't find a group for the app, create a new one.
            % The app is guaranteed to fit in the new, empty group.
            if ~success
                % If there is a left-over disbanded group, use that.
                % Otherwise, create a new one.
                if ~isempty(obj.freeGroups)
                    newGroup = obj.freeGroups(1);
                    obj.freeGroups(1) = [];
                else
                    newGroup = length(obj.groups) + 1;
                end
                obj.groups{newGroup} = [];
                disp([obj.task 'Creating new group ' num2str(newGroup) ' for app ' num2str(newApp)]);
                [apps, success] = obj.addToGroup(apps, newApp, newGroup, currentSimTime);
                if ~success
                    error('Stagger: Failure adding app to empty group.  This suggests an error in the stack algorithm');
                end
            end
        end
        
        function [apps, success] = addToGroup(obj, apps, newApp, groupNum, currentSimTime)
            groupSize = length(obj.groups{groupNum});
            if groupSize > 0
                % Calculate the time of the next checkpoint for each
                % application in the group.  This is done from last to first so that
                % Matlab allocates the entire array at once (for speed).
                for i = groupSize:-1:1
                    a = obj.groups{groupNum}(i);
                    checkpoint(i).start = obj.getCheckpointStart(apps(a), a, currentSimTime);
                    checkpoint(i).end = checkpoint(i).start + apps(a).checkpointTime;
                end
                newAppCheckpoint.start = obj.getCheckpointStart(apps(newApp), newApp, currentSimTime);
                newAppCheckpoint.end = newAppCheckpoint.start + apps(newApp).checkpointTime;

                % Add the new app to the group but delay it so that its checkpoint
                % doesn't conflict with other checkpoints in the group.
                totalDelay = 0.0;
                aligned = false;
                % This loop allows us to repeatedly search through the apps in the group for checkpoint conflicts - when
                % we move the checkpoint, we may create a conflict that didn't exist the first time we checked.
                checks = 0;
                while ~aligned && checks < groupSize * 3 % This loop should run at most once per app that's already in the group, so this will avoid infinite loops due to round off errors.  The *3 is just to be extra sure we don't break something.
                    aligned = true;
                    checks = checks + 1;
                    % Scan through all apps in the group and find checkpoint conflicts.
                    for i = 1:groupSize
                        if newAppCheckpoint.end > checkpoint(i).start && newAppCheckpoint.start < checkpoint(i).end
                            diff = checkpoint(i).end - newAppCheckpoint.start;
                            totalDelay = totalDelay + diff;
                            % If the delay gets too big, give up on this
                            % group and move on to the next one.
                            if totalDelay > obj.appDelayRemaining(newApp);
                                success = false;
                                return;
                            end

                            newAppCheckpoint.start = newAppCheckpoint.start + diff;
                            newAppCheckpoint.end = newAppCheckpoint.end + diff;
                            
                            % When we find a checkpoint conflict, we have to go back and make sure the new
                            % checkpoint doesn't conflict with apps we've already checked.
                            aligned = false;
                            break;
                        end
                    end
                end
                % Once we've looped through all the apps without an
                % alignment conflict, we set the actual delay.
                if totalDelay > 0.0
                    obj.appsToPause(newApp) = totalDelay;
                else
                    % If the app was going to be paused because it was
                    % added to another group, remove the pause.
                    if obj.appsToPause.isKey(newApp)
                        obj.appsToPause.remove(newApp);
                    end
                end
            end
            % Add the new app to the group
            disp([obj.task 'Adding app ' num2str(newApp) ' to group ' num2str(groupNum)]);
            obj.groups{groupNum}(end + 1) = newApp;
            success = true;
        end
        
        function start = getCheckpointStart(obj, app, a, currentSimTime)
            % Calculate iteration time
            if app.checkpointMethod == obj.CHECKPOINT_BY_ITERATIONS
                iterationTime = sum(app.phaseTime) * app.providedCheckpointInterval;
            else
                iterationTime = app.providedCheckpointInterval;
            end
            start = iterationTime + app.stopCheckpointTime;
            % If the app is currently paused (or will be when this
            % algorithm finishes), take that into account when calculating
            % the next checkpoint time.
            if obj.appsToPause.isKey(a)
                start = start + obj.appsToPause(a);
            elseif app.paused
                start = start + (app.endPauseTime - currentSimTime);
            end
        end
        
        function apps = disbandGroups(obj, apps, currentSimTime)
            for i = 1:length(obj.groupsWithDepartures)
                g = obj.groupsWithDepartures(i);
                % If the group has become too small, disband it, but not if
                % the number of small groups is below the max.
                tooSmall = length(obj.groups{g}) < obj.MIN_GROUP_SIZE;
                smallGroups = obj.countSmallGroups;
                if (tooSmall && smallGroups > obj.SMALL_GROUPS_ALLOWED)
                    apps = obj.disbandGroup(g, apps, currentSimTime);
                end
            end
        end
        
        function apps = disbandGroup(obj, g, apps, currentSimTime)
            disp([obj.task 'Disbanding group ' num2str(g)]);
            % Delete the group.  This leaves an empty array in the
            % group list, which keeps the numbering the same for
            % other groups.
            oldGroup = obj.groups{g};
            obj.groups{g} = [];
            obj.freeGroups(end + 1) = g;
            % Move all the apps to other groups.
            for i = 1:length(oldGroup)
                apps = obj.findGroup(apps, oldGroup(i), currentSimTime);
            end
        end
        
        function apps = applyPauses(obj, apps, currentSimTime)
            toPause = obj.appsToPause.keys;
            for i = 1:length(toPause)
                a = toPause{i};
                totalDelay = obj.appsToPause(a);
                apps(a) = PSPauseApp(apps(a), currentSimTime, totalDelay, obj.task);
                obj.appDelayRemaining(a) = obj.appDelayRemaining(a) - totalDelay;
                obj.overallDelay = obj.overallDelay + totalDelay;
            end
        end
    end
end
