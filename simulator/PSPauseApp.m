% Copyright 2017 Lee Savoie
%
% This file is part of PowerShifter.
%
% PowerShifter is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% PowerShifter is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with PowerShifter.  If not, see <http://www.gnu.org/licenses/>.

% This is used by the power sharing simulation to pause apps.  It is
% separate because it can be called from several sharing algorithms, which
% are sometimes in separate files.
function app = PSPauseApp(app, currentSimTime, duration, alg)
    disp([alg 'Pausing app ' app.name ' at ' num2str(currentSimTime)]);
    % Handle cases where the app is already paused
    remainingOldPause = 0;
    if app.paused
        remainingOldPause = app.endPauseTime - currentSimTime;
    end
    app.paused = true;
    app.endPauseTime = currentSimTime + duration;
    % If we are checkpointing or restarting, update the start time to account
    % for the pause.  This will make it seem as if the app did nothing
    % while it was paused.
    if app.restarting
        app.lastRestartTime = app.lastRestartTime + duration - remainingOldPause;
    elseif app.checkpointing
        app.startCheckpointTime = app.startCheckpointTime + duration - remainingOldPause;
    end
end
